<?php

// No direct access
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Main class
 *
 * @since 0.1
 */
class WPO_Themecustomizer {


	public function __construct() {
		add_action('admin_menu', array( $this, 'adminLoadMenu') );
		require_once( dirname(__FILE__).'/livetheme.php' );

		WPO_LiveTheme::getInstance();
		 
	}	


	public function adminLoadMenu(){
		add_theme_page( 'Live Theme Editor', __("Live Theme Editor", TEXTDOMAIN), 'switch_themes', 'wpo_livethemeedit', array($this,'liveThemePage') );
	}

	public function liveThemePage(){
	}

	/*public function l($text){
		return $text;
	}*/

}

// Instantiate the main class
new WPO_Themecustomizer();
