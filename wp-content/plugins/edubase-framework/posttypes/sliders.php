<?php

if(!function_exists('wpo_create_type_sliders')   ){
    function wpo_create_type_sliders(){
        $labels = array(
            'name' => __( 'Sliders', "wpoframework" ),
            'singular_name' => __( 'Slider', "wpoframework"),
            'add_new' => __( 'Add New Slider', "wpoframework" ),
            'add_new_item' => __( 'Add New Slider', "wpoframework" ),
            'edit_item' => __( 'Edit Slider', "wpoframework" ),
            'new_item' => __( 'New Slider', "wpoframework" ),
            'view_item' => __( 'View Slider', "wpoframework" ),
            'search_items' => __( 'Search Slider', "wpoframework" ),
            'not_found' => __( 'No Slider found', "wpoframework" ),
            'not_found_in_trash' => __( 'No Slider found in Trash', "wpoframework" ),
            'parent_item_colon' => __( 'Parent Slider:', "wpoframework" ),
            'menu_name' => __( 'Opal Sliders', "wpoframework" )
        );

        $args = array(
            'labels' => $labels,
            'hierarchical' => true,
            'description' => 'List Slider',
            'supports' => array( 'title', 'editor', 'thumbnail' ),
            'taxonomies' => array('slider_group' ),
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 5,
            'show_in_nav_menus' => false,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'has_archive' => true,
            'query_var' => true,
            'can_export' => true,
            'rewrite' => true,
            'capability_type' => 'post'
        );
        register_post_type( 'sliders', $args );


        $labels = array(
            'name' => __( 'Slider groups', "wpoframework" ),
            'singular_name' => __( 'Slider group', "wpoframework" ),
            'search_items' =>  __( 'Search Slider groups',"wpoframework" ),
            'all_items' => __( 'All Slider groups',"wpoframework" ),
            'parent_item' => __( 'Parent Slider group',"wpoframework" ),
            'parent_item_colon' => __( 'Parent Slider group:',"wpoframework" ),
            'edit_item' => __( 'Edit Slider group',"wpoframework" ),
            'update_item' => __( 'Update Slider group',"wpoframework" ),
            'add_new_item' => __( 'Add New Slider group',"wpoframework" ),
            'new_item_name' => __( 'New Slider group',"wpoframework" ),
            'menu_name' => __( 'Slider groups',"wpoframework" ),
        );

        register_taxonomy('slider_group',array('sliders'), array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'slider_group' ),
            'show_in_nav_menus'=>false
        ));
    }
    add_action( 'init','wpo_create_type_sliders' );
}