<?php
 /**
  * $Desc
  *
  * @version    $Id$
  * @package    wpbase
  * @author     Opal  Team <opalwordpressl@gmail.com >
  * @copyright  Copyright (C) 2015 www.wpopal.com. All Rights Reserved.
  * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
  *
  * @website  http://www.wpopal.com
  * @support  http://www.wpopal.com/support/forum.html
  */

if(!function_exists('wpo_create_type_brand')  ){
  function wpo_create_type_brand(){
    $labels = array(
      'name' => __( 'Brand', "wpoframework" ),
      'singular_name' => __( 'Brand', "wpoframework" ),
      'add_new' => __( 'Add New Brand', "wpoframework" ),
      'add_new_item' => __( 'Add New Brand', "wpoframework" ),
      'edit_item' => __( 'Edit Brand', "wpoframework" ),
      'new_item' => __( 'New Brand', "wpoframework" ),
      'view_item' => __( 'View Brand', "wpoframework" ),
      'search_items' => __( 'Search Brands', "wpoframework" ),
      'not_found' => __( 'No Brands found', "wpoframework" ),
      'not_found_in_trash' => __( 'No Brands found in Trash', "wpoframework" ),
      'parent_item_colon' => __( 'Parent Brand:', "wpoframework" ),
      'menu_name' => __( 'Opal Brands', "wpoframework" ),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'description' => 'List Brand',
        'supports' => array( 'title', 'thumbnail'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_nav_menus' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
    register_post_type( 'brands', $args );
    
      if( class_exists('WPO_MetaBox') && !file_exists(WPO_THEME_INC_DIR   . 'metabox_templates/brands.php')  ){
       //Post setting
        new WPO_MetaBox(array(
          'id'       => 'wpo_brandconfig',
          'title'    => __('Brands Configuration', "wpoframework"),
          'types'    => array('brands'),
          'priority' => 'high',
          'template' => WPO_PLUGIN_FRAMEWORK_TEMPLATE_DIR . 'brands.php'
        ));
      }

  }

  add_action('init','wpo_create_type_brand');
}