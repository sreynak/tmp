<?php
 /**
  * $Desc
  *
  * @version    $Id$
  * @package    wpbase
  * @author     Opal  Team <opalwordpressl@gmail.com >
  * @copyright  Copyright (C) 2015 www.wpopal.com. All Rights Reserved.
  * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
  *
  * @website  http://www.wpopal.com
  * @support  http://www.wpopal.com/support/forum.html
  */
if(!function_exists('wpo_create_type_gallery')   ){
    function wpo_create_type_gallery(){
      $labels = array(
          'name'               => __( 'Gallerys', "wpoframework" ),
          'singular_name'      => __( 'Gallery', "wpoframework" ),
          'add_new'            => __( 'Add New Gallery', "wpoframework" ),
          'add_new_item'       => __( 'Add New Gallery', "wpoframework" ),
          'edit_item'          => __( 'Edit Gallery', "wpoframework" ),
          'new_item'           => __( 'New Gallery', "wpoframework" ),
          'view_item'          => __( 'View Gallery', "wpoframework" ),
          'search_items'       => __( 'Search Gallerys', "wpoframework" ),
          'not_found'          => __( 'No Gallerys found', "wpoframework" ),
          'not_found_in_trash' => __( 'No Gallerys found in Trash', "wpoframework" ),
          'parent_item_colon'  => __( 'Parent Gallery:', "wpoframework" ),
          'menu_name'          => __( 'Opal Gallerys', "wpoframework" ),
      );

      $args = array(
          'labels'              => $labels,
          'hierarchical'        => false,
          'description'         => 'List Gallery',
          'supports'            => array( 'title', 'editor', 'author', 'thumbnail','excerpt','custom-fields' ), //page-attributes, post-formats
          'taxonomies'          => array( 'gallery_category','skills','post_tag' ),
          'public'              => true,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'menu_position'       => 5,
          'menu_icon'           => WPO_FRAMEWORK_ADMIN_IMAGE_URI.'icon/admin_ico_gallery.png',
          'show_in_nav_menus'   => false,
          'publicly_queryable'  => true,
          'exclude_from_search' => false,
          'has_archive'         => true,
          'query_var'           => true,
          'can_export'          => true,
          'rewrite'             => array('slug'=>'gallery'),
          'capability_type'     => 'post',
      );
      register_post_type( 'gallery', $args );

      //Add Port folio Skill
      // Add new taxonomy, make it hierarchical like categories
      //first do the translations part for GUI
      $labels = array(
        'name'              => __( 'Categories', "wpoframework" ),
        'singular_name'     => __( 'Category', "wpoframework" ),
        'search_items'      => __( 'Search Category',"wpoframework" ),
        'all_items'         => __( 'All Categories',"wpoframework" ),
        'parent_item'       => __( 'Parent Category',"wpoframework" ),
        'parent_item_colon' => __( 'Parent Category:',"wpoframework" ),
        'edit_item'         => __( 'Edit Category',"wpoframework" ),
        'update_item'       => __( 'Update Category',"wpoframework" ),
        'add_new_item'      => __( 'Add New Category',"wpoframework" ),
        'new_item_name'     => __( 'New Category Name',"wpoframework" ),
        'menu_name'         => __( 'Categories',"wpoframework" ),
      );
      // Now register the taxonomy
      register_taxonomy('gallery_category',array('gallery'),
          array(
              'hierarchical'      => true,
              'labels'            => $labels,
              'show_ui'           => true,
              'show_admin_column' => true,
              'query_var'         => true,
              'show_in_nav_menus' => false,
              'rewrite'           => array( 'slug' => 'gallery-category'
          ),
      ));



      if( class_exists('WPO_MetaBox') && !file_exists(WPO_THEME_INC_DIR   . 'metabox_templates/gallery.php')  ){   //Gallery Setting.
        $aa = new WPO_MetaBox(array(
          'id'       => 'wpo_pageconfig',
          'title'    => __('Gallery Configuration', 'wpoframework'),
          'types'    => array('gallery'),
          'priority' => 'high',
          'template' => WPO_PLUGIN_FRAMEWORK_TEMPLATE_DIR . 'gallery.php',
        ));

      } 

  }
  add_action( 'init','wpo_create_type_gallery' );
}