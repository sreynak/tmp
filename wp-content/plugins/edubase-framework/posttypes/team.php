<?php
 /**
  * $Desc
  *
  * @version    $Id$
  * @package    wpbase
  * @author     Opal  Team <opalwordpressl@gmail.com >
  * @copyright  Copyright (C) 2015 www.wpopal.com. All Rights Reserved.
  * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
  *
  * @website  http://www.wpopal.com
  * @support  http://www.wpopal.com/support/forum.html
  */

if(!function_exists('wpo_create_type_team')   ){
    function wpo_create_type_team(){
      $labels = array(
        'name' => __( 'Opal Team', "wpoframework" ),
        'singular_name' => __( 'Team', "wpoframework" ),
        'add_new' => __( 'Add New Team', "wpoframework" ),
        'add_new_item' => __( 'Add New Team', "wpoframework" ),
        'edit_item' => __( 'Edit Team', "wpoframework" ),
        'new_item' => __( 'New Team', "wpoframework" ),
        'view_item' => __( 'View Team', "wpoframework" ),
        'search_items' => __( 'Search Teams', "wpoframework" ),
        'not_found' => __( 'No Teams found', "wpoframework" ),
        'not_found_in_trash' => __( 'No Teams found in Trash', "wpoframework" ),
        'parent_item_colon' => __( 'Parent Team:', "wpoframework" ),
        'menu_name' => __( 'Opal Teams', "wpoframework" ),
      );

      $args = array(
          'labels' => $labels,
          'hierarchical' => false,
          'description' => 'List Team',
          'supports' => array( 'title', 'editor', 'thumbnail','excerpt'),
          'public' => true,
          'show_ui' => true,
          'show_in_menu' => true,
          'menu_position' => 5,
          'show_in_nav_menus' => false,
          'publicly_queryable' => true,
          'exclude_from_search' => true,
          'has_archive' => true,
          'query_var' => true,
          'can_export' => true,
          'rewrite' => false,
          'capability_type' => 'post'
      );
      register_post_type( 'team', $args );

        if( class_exists('WPO_MetaBox') && !file_exists(WPO_THEME_INC_DIR   . 'metabox_templates/team.php')  ){
          new WPO_MetaBox(array(
            'id'       => 'wpo_team',
            'title'    => __('Team Options', "wpoframework"),
            'types'    => array('team'),
            'priority' => 'high',
            'template' => WPO_PLUGIN_FRAMEWORK_TEMPLATE_DIR . 'team.php',
          ));
        }  
    }

   add_action( 'init','wpo_create_type_team' );
}


