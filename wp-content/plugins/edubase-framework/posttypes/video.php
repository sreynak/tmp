<?php
 /**
  * $Desc
  *
  * @version    $Id$
  * @package    wpbase
  * @author     Opal  Team <opalwordpressl@gmail.com >
  * @copyright  Copyright (C) 2015  www.wpopal.com. All Rights Reserved.
  * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
  *
  * @website  http://www.wpopal.com
  * @support  http://www.wpopal.com/support/forum.html
  */

if(!function_exists('wpo_create_type_video')   ){
  function wpo_create_type_video(){
    $labels = array(
      'name' => __( 'Video', "wpoframework" ),
      'singular_name' => __( 'Video', "wpoframework" ),
      'add_new' => __( 'Add New Video', "wpoframework" ),
      'add_new_item' => __( 'Add New Video', "wpoframework" ),
      'edit_item' => __( 'Edit Video', "wpoframework" ),
      'new_item' => __( 'New Video', "wpoframework" ),
      'view_item' => __( 'View Video', "wpoframework" ),
      'search_items' => __( 'Search Videos', "wpoframework" ),
      'not_found' => __( 'No Videos found', "wpoframework" ),
      'not_found_in_trash' => __( 'No Videos found in Trash', "wpoframework" ),
      'parent_item_colon' => __( 'Parent Video:', "wpoframework" ),
      'menu_name' => __( 'Opal Videos', "wpoframework" ),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'description' => 'List Video',
        'supports' => array( 'title', 'editor', 'thumbnail','comments', 'excerpt' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
    register_post_type( 'video', $args );

    $labels = array(
        'name'              => __( 'Categories Video', "wpoframework" ),
        'singular_name'     => __( 'Category', "wpoframework" ),
        'search_items'      => __( 'Search Category',"wpoframework" ),
        'all_items'         => __( 'All Categories',"wpoframework" ),
        'parent_item'       => __( 'Parent Category',"wpoframework" ),
        'parent_item_colon' => __( 'Parent Category:',"wpoframework" ),
        'edit_item'         => __( 'Edit Category',"wpoframework" ),
        'update_item'       => __( 'Update Category',"wpoframework" ),
        'add_new_item'      => __( 'Add New Category',"wpoframework" ),
        'new_item_name'     => __( 'New Category Name',"wpoframework" ),
        'menu_name'         => __( 'Categories Video',"wpoframework" ),
      );
      // Now register the taxonomy
      register_taxonomy('category_video',array('video'),
          array(
              'hierarchical'      => true,
              'labels'            => $labels,
              'show_ui'           => true,
              'show_admin_column' => true,
              'query_var'         => true,
              'rewrite'           => array( 'slug' => 'video'
          ),
      ));

      if( class_exists('WPO_MetaBox') && !file_exists(WPO_THEME_INC_DIR   . 'metabox_templates/video.php') ){
          new WPO_MetaBox(array(
            'id'       => 'wpo_formatvideo',
            'title'    => __('Embed Options', 'wpoframework' ),
            'types'    => array('video'),
            'priority' => 'high',
            'template' => WPO_PLUGIN_FRAMEWORK_TEMPLATE_DIR . 'video.php',
          ));

      }  
  }
  add_action( 'init', 'wpo_create_type_video' );
}


