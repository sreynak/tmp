<?php 
/*
  Plugin Name: Edubase Framework
  Plugin URI: http://www.wpopal.com/
  Description: implement rick functions for themes base on wpo framework and load widgets for theme used, this is required.
  Version: 1.2
  Author: WP_Opal
  Author URI: http://www.wpopal.com
  License: GPLv2 or later
  Update: 21/Dec/2015
 */

 /**
  * $Desc
  *
  * @version    $Id$
  * @package    wpbase
  * @author     Opal Team  Team <opalwordpressl@gmail.com >
  * @copyright  Copyright (C) 2015 www.wpopal.com. All Rights Reserved.
  * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
  *
  * @website  http://www.wpopal.com
  * @support  http://www.wpopal.com/support/forum.html
  */

  define( 'WPO_PLUGIN_FRAMEWORK_URL', plugin_dir_url( __FILE__ ) );
  define( 'WPO_PLUGIN_FRAMEWORK_DIR', plugin_dir_path( __FILE__ ));
  define( 'WPO_PLUGIN_FRAMEWORK_TEMPLATE_DIR', WPO_PLUGIN_FRAMEWORK_DIR.'metabox_templates/' );
  define( 'WPO_PLUGIN_FRAMEWORK_CSS_EDITOR_DIR', WPO_PLUGIN_FRAMEWORK_DIR.'customize/assets/' );
  define( 'WPO_PLUGIN_FRAMEWORK_CSS_EDITOR_URL', WPO_PLUGIN_FRAMEWORK_URL.'customize/assets/' );
  
  include_once( dirname( __FILE__ ) . '/import/import.php' );
  include_once( dirname( __FILE__ ) . '/export/export.php' );

  if( !defined('WPO_THEME_NAME') ){

    $themename = get_option( 'stylesheet' ); 
    $themename = preg_replace("/\W/", "_", strtolower($themename) );
    define( 'WPO_THEME_NAME', $themename );
  }


  /**
   * Loading Widgets
   */
  function wpo_fw_widgets_init(){
      if( !defined('WPO_THEME_DIR') ){
        return ;
      }  


      require( WPO_PLUGIN_FRAMEWORK_DIR.'setting.class.php' );
      require( WPO_PLUGIN_FRAMEWORK_DIR.'widget.class.php' );
      require( WPO_PLUGIN_FRAMEWORK_DIR.'livetheme.class.php' );
      
      define( "WPO_PLUGIN_FRAMEWORK", true );
      define( 'WPO_PLUGIN_FRAMEWORK_WIDGET_TEMPLATES', WPO_THEME_TEMPLATE_DIR  );

      $widgets = apply_filters( 'wpo_fw_load_widgets', array( 'twitter','posts','featured_post','top_rate','sliders','recent_comment','recent_post','tabs','flickr', 'video', 'socials', 'menu_vertical', 'socials_siderbar') );


      if( !empty($widgets) ){
          foreach( $widgets as $opt => $key ){

              $file = str_replace( 'enable_', '', $key );
              $filepath = WPO_PLUGIN_FRAMEWORK_DIR.'widgets/'.$file.'.php'; 
              if( file_exists($filepath) ){ 
                  require_once( $filepath );
              }
          }  
      }
  }
  add_action( 'widgets_init', 'wpo_fw_widgets_init' );

    
  /**
   * Loading Post Types
   */
  function wpo_fw_load_posttypes_setup(){
      
      if( !defined('WPO_THEME_DIR') ){
        return ;
      }  

      $opts = apply_filters( 'wpo_fw_load_posttypes', get_option( 'wpo_themer_posttype' ) );
      if( !empty($opts) ){
          foreach( $opts as $opt => $key ){

              $file = str_replace( 'enable_', '', $opt );
              $filepath = WPO_PLUGIN_FRAMEWORK_DIR.'posttypes/'.$file.'.php'; 
              if( file_exists($filepath) ){
                  require_once( $filepath );
              }
          }  
      }
  }   
  add_action( 'init', 'wpo_fw_load_posttypes_setup', 1 );   


  function wpo_theme_options($name, $default = false) {
      
        // get the meta from the database
        $options = ( get_option( 'wpo_theme_options' ) ) ? get_option( 'wpo_theme_options' ) : null;

        // d( $options );
       
        // return the option if it exists
        if ( isset( $options[$name] ) ) {
            return apply_filters( 'wpo_theme_options_$name', $options[ $name ] );
        }
        if( get_option( $name ) ){
            return get_option( $name );
        }
        // return default if nothing else
        return apply_filters( 'wpo_theme_options_$name', $default );
    }

    function wpo_enqueue_style(){
    if( wpo_theme_options('customize-theme','') && wpo_theme_options('customize-theme','') != 'nouse' ){
      wp_enqueue_style('customize-style',WPO_PLUGIN_FRAMEWORK_CSS_EDITOR_URL.wpo_theme_options('customize-theme').'.css');
    }
  }
  add_action( 'wp_enqueue_scripts', 'wpo_enqueue_style' );