
<!--Select gallery post-->
<p class="wpo_section postformat wpo-postformat-gallery">

   <label><b><?php _e( 'Gallery image', 'wpothemer')?></b></label>
   <?php
       wp_enqueue_media();
       wp_enqueue_style( 'WPO_admin_metabox_css', WPO_FRAMEWORK_ADMIN_STYLE_URI.'css/metabox.css');
       wp_enqueue_script('WPO_gallery_upload', WPO_FRAMEWORK_ADMIN_STYLE_URI.'js/gallery_upload.js');
   ?>
   <span class="add_product_images hide-if-no-js">
       <a href="#" class="button" data-choose="<?php _e( 'Add Images to Gallery', 'wpothemer' ); ?>" data-update="<?php _e( 'Add to gallery', 'wpothemer' ); ?>" data-delete="<?php _e( 'Delete image', 'wpothemer'); ?>" data-text="<?php _e( 'Delete', 'wpothemer'); ?>"><i class="fa fa-upload"></i><?php _e(' Add gallery', 'wpothemer'); ?></a>
   </span> 
   <div id="product_images_container" class="postformat wpo-postformat-gallery">

       <ul class="product_images">
       <?php
           $mb->the_field( 'post_gallery');
           $galleries = $mb->get_the_value();
           $attachments = array_filter( explode( ',', $galleries ) );

           if ( $attachments ) {
               foreach ( $attachments as $attachment_id ) {
                   echo '<li class="image" data-attachment_id="' . esc_attr( $attachment_id ) . '">
                       ' . wp_get_attachment_image( $attachment_id, 'lookbook-slide' ) . '
                       <ul class="actions">
                           <li><a href="#" class="delete tips" data-tip="' . __( 'Delete image', 'wpothemer' ) . '"><i class="fa fa-times"></i></a></li>
                       </ul>
                   </li>';
               }
           }
       ?>
       </ul>

       <input type="hidden" id="product_image_gallery" name="<?php $mb->the_name(); ?>" value="<?php echo esc_attr( $mb->get_the_value() ); ?>" />

   </div>
</p>