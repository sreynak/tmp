<?php

// No direct access
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Main class
 *
 * @since 0.1
 */
class WPO_Themecustomizer {


	public function __construct() {
		add_action('admin_menu', array( $this, 'adminLoadMenu') );
		add_action( 'customize_register', array( $this, 'wpo_cst_customizer'));
		require_once( WPO_PLUGIN_FRAMEWORK_DIR.'livetheme/livetheme.php' );

		WPO_LiveTheme::getInstance();
		 
	}	


	public function adminLoadMenu(){
		add_options_page( 
			'Live Theme Editor', 
			'Live Theme Editor',
			'manage_options', 
			'wpo_livethemeedit', array($this,'liveThemePage') );
	}

	public function liveThemePage(){
	
	}

	public function wpo_cst_customizer($wp_customize){
         $wp_customize->add_setting( 'wpo_theme_options[customize-theme]', array(
            'capability'        => 'manage_options',
            'type'       => 'option',
            'default'           => '',
            'theme_supports'    => 'custom-background',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control(  new WPO_Framework_CustomizeProfile( $wp_customize, 'wpo_theme_options[customize-theme]', array(
            'label'      => __( 'Custom Theme Profile', 'wpoframework' ),
            'section'    => 'ts_general_settings',
        ) ) );
    }


}

// Instantiate the main class
new WPO_Themecustomizer();


if(  class_exists("WP_Customize_Control") ){


    class WPO_Framework_CustomizeProfile extends WP_Customize_Control{
         
        public function render_content(){ 
                    
            $profiles = $this->wpo_cst_css_profiles();

         
            $output = array();
            
            foreach( $profiles as $profile ){ 
                $selected = ($this->value() == $profile)?' selected="selected" ': '';
                $output[] = '<option value="'.$profile.'" '.$selected.'>'.$profile.'</option>';
            }

            $dropdown = '<select>'.implode( " ", $output ).'</select>';
            $dropdown = str_replace('<select', '<select ' . $this->get_link(), $dropdown );

            $sub = '<div class="alert alert-info">
                   '.esc_html__('Click Live Customizing Theme to create custom-theme-profiles and they will be listed in above dropdown box. You select one profile theme to apply for your site
!important: All theme profiles are stored in folder YOURTHEME/css/customze, it need permission 0755 to put files inside', 'wpoframework').'
            </div>' .'<div><a href="'.admin_url('options-general.php?page=wpo_livethemeedit').'">'.esc_html__('Create Profile Now', 'wpoframework').'</a></div>';
            printf( 
                 '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>'.$sub,
                $this->label,
                $dropdown
            );
     
        }

        public function wpo_cst_css_profiles(){
		    $path = WPO_PLUGIN_FRAMEWORK_DIR.'/customize/assets/*.css';
		    $files = glob($path  );
		    $skins = array( 'nouse' => esc_html__('No Use', 'wpoframework') );

		    if(count($files)>0){
		        foreach ($files as $key => $file) {
		            $skin = str_replace( '.css', '', basename($file) );
		            $skins[$skin]=$skin;
		        }
		    }

		    return $skins;
		}
    }
}
