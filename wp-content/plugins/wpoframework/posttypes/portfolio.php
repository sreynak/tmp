<?php
 /**
  * $Desc
  *
  * @version    $Id$
  * @package    wpbase
  * @author     Opal  Team <opalwordpressl@gmail.com >
  * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
  * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
  *
  * @website  http://www.wpopal.com
  * @support  http://www.wpopal.com/support/forum.html
  */
if(!function_exists('wpo_create_type_portfolio')  ){
    function wpo_create_type_portfolio(){
      $labels = array(
          'name'               => __( 'Portfolios', "wpothemer" ),
          'singular_name'      => __( 'Portfolio', "wpothemer" ),
          'add_new'            => __( 'Add New Portfolio', "wpothemer" ),
          'add_new_item'       => __( 'Add New Portfolio', "wpothemer" ),
          'edit_item'          => __( 'Edit Portfolio', "wpothemer" ),
          'new_item'           => __( 'New Portfolio', "wpothemer" ),
          'view_item'          => __( 'View Portfolio', "wpothemer" ),
          'search_items'       => __( 'Search Portfolios', "wpothemer" ),
          'not_found'          => __( 'No Portfolios found', "wpothemer" ),
          'not_found_in_trash' => __( 'No Portfolios found in Trash', "wpothemer" ),
          'parent_item_colon'  => __( 'Parent Portfolio:', "wpothemer" ),
          'menu_name'          => __( 'Opal Portfolios', "wpothemer" ),
      );

      $args = array(
          'labels'              => $labels,
          'hierarchical'        => true,
          'description'         => 'List Portfolio',
          'supports'            => array( 'title', 'editor', 'author', 'thumbnail','excerpt','custom-fields' ), //page-attributes, post-formats
          'taxonomies'          => array( 'portfolio_category','gallery_category','post_tag' ),
          'public'              => true,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'menu_position'       => 5,
          'menu_icon'           => WPO_FRAMEWORK_ADMIN_IMAGE_URI.'icon/admin_ico_portfolio.png',
          'show_in_nav_menus'   => false,
          'publicly_queryable'  => true,
          'exclude_from_search' => false,
          'has_archive'         => true,
          'query_var'           => true,
          'can_export'          => true,
          'rewrite'             => true,
          'capability_type'     => 'post'
      );
      register_post_type( 'portfolio', $args );

      //Add Port folio Skill
      // Add new taxonomy, make it hierarchical like categories
      //first do the translations part for GUI
      $labels = array(
        'name'              => __( 'Categories', "wpothemer" ),
        'singular_name'     => __( 'Category', "wpothemer" ),
        'search_items'      => __( 'Search Category',"wpothemer" ),
        'all_items'         => __( 'All Categories',"wpothemer" ),
        'parent_item'       => __( 'Parent Category',"wpothemer" ),
        'parent_item_colon' => __( 'Parent Category:',"wpothemer" ),
        'edit_item'         => __( 'Edit Category',"wpothemer" ),
        'update_item'       => __( 'Update Category',"wpothemer" ),
        'add_new_item'      => __( 'Add New Category',"wpothemer" ),
        'new_item_name'     => __( 'New Category Name',"wpothemer" ),
        'menu_name'         => __( 'Categories',"wpothemer" ),
      );
      // Now register the taxonomy
      register_taxonomy('category_portfolio',array('portfolio'),
          array(
              'hierarchical'      => true,
              'labels'            => $labels,
              'show_ui'           => true,
              'show_admin_column' => true,
              'query_var'         => true,
              'show_in_nav_menus' =>false,
              'rewrite'           => array( 'slug' => 'category-portfolio'
          ),
      ));



      if( class_exists('WPO_MetaBox') && !file_exists(WPO_THEME_INC_DIR   . 'metabox_templates/portfolio.php')  ){
        new WPO_MetaBox(array(
          'id'       => 'wpo_portfolio',
          'title'    => __('Portfolio Options', "wpothemer"),
          'types'    => array('portfolio'),
          'priority' => 'high',
          'template' => WPO_PLUGIN_FRAMEWORK_TEMPLATE_DIR . 'portfolio.php',
        ));
      }   
  }
  add_action( 'init','wpo_create_type_portfolio' );
}