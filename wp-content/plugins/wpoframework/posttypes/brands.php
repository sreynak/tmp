<?php
 /**
  * $Desc
  *
  * @version    $Id$
  * @package    wpbase
  * @author     Opal  Team <opalwordpressl@gmail.com >
  * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
  * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
  *
  * @website  http://www.wpopal.com
  * @support  http://www.wpopal.com/support/forum.html
  */

if(!function_exists('wpo_create_type_brand')  ){
  function wpo_create_type_brand(){
    $labels = array(
      'name' => __( 'Brand', "wpothemer" ),
      'singular_name' => __( 'Brand', "wpothemer" ),
      'add_new' => __( 'Add New Brand', "wpothemer" ),
      'add_new_item' => __( 'Add New Brand', "wpothemer" ),
      'edit_item' => __( 'Edit Brand', "wpothemer" ),
      'new_item' => __( 'New Brand', "wpothemer" ),
      'view_item' => __( 'View Brand', "wpothemer" ),
      'search_items' => __( 'Search Brands', "wpothemer" ),
      'not_found' => __( 'No Brands found', "wpothemer" ),
      'not_found_in_trash' => __( 'No Brands found in Trash', "wpothemer" ),
      'parent_item_colon' => __( 'Parent Brand:', "wpothemer" ),
      'menu_name' => __( 'Opal Brands', "wpothemer" ),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'description' => 'List Brand',
        'supports' => array( 'title', 'thumbnail'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_nav_menus' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
    register_post_type( 'brands', $args );
    
      if( class_exists('WPO_MetaBox') && !file_exists(WPO_THEME_INC_DIR   . 'metabox_templates/brands.php')  ){
       //Post setting
        new WPO_MetaBox(array(
          'id'       => 'wpo_brandconfig',
          'title'    => __('Brands Configuration', "wpothemer"),
          'types'    => array('brands'),
          'priority' => 'high',
          'template' => WPO_PLUGIN_FRAMEWORK_TEMPLATE_DIR . 'brands.php'
        ));
      }

  }

  add_action('init','wpo_create_type_brand');
}