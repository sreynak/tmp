<?php

if(!function_exists('wpo_create_type_sliders')   ){
    function wpo_create_type_sliders(){
        $labels = array(
            'name' => __( 'Sliders', "wpothemer" ),
            'singular_name' => __( 'Slider', "wpothemer"),
            'add_new' => __( 'Add New Slider', "wpothemer" ),
            'add_new_item' => __( 'Add New Slider', "wpothemer" ),
            'edit_item' => __( 'Edit Slider', "wpothemer" ),
            'new_item' => __( 'New Slider', "wpothemer" ),
            'view_item' => __( 'View Slider', "wpothemer" ),
            'search_items' => __( 'Search Slider', "wpothemer" ),
            'not_found' => __( 'No Slider found', "wpothemer" ),
            'not_found_in_trash' => __( 'No Slider found in Trash', "wpothemer" ),
            'parent_item_colon' => __( 'Parent Slider:', "wpothemer" ),
            'menu_name' => __( 'Opal Sliders', "wpothemer" )
        );

        $args = array(
            'labels' => $labels,
            'hierarchical' => true,
            'description' => 'List Slider',
            'supports' => array( 'title', 'editor', 'thumbnail' ),
            'taxonomies' => array('slider_group' ),
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 5,
            'show_in_nav_menus' => false,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'has_archive' => true,
            'query_var' => true,
            'can_export' => true,
            'rewrite' => true,
            'capability_type' => 'post'
        );
        register_post_type( 'sliders', $args );


        $labels = array(
            'name' => __( 'Slider groups', "wpothemer" ),
            'singular_name' => __( 'Slider group', "wpothemer" ),
            'search_items' =>  __( 'Search Slider groups',"wpothemer" ),
            'all_items' => __( 'All Slider groups',"wpothemer" ),
            'parent_item' => __( 'Parent Slider group',"wpothemer" ),
            'parent_item_colon' => __( 'Parent Slider group:',"wpothemer" ),
            'edit_item' => __( 'Edit Slider group',"wpothemer" ),
            'update_item' => __( 'Update Slider group',"wpothemer" ),
            'add_new_item' => __( 'Add New Slider group',"wpothemer" ),
            'new_item_name' => __( 'New Slider group',"wpothemer" ),
            'menu_name' => __( 'Slider groups',"wpothemer" ),
        );

        register_taxonomy('slider_group',array('sliders'), array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'slider_group' ),
            'show_in_nav_menus'=>false
        ));
    }
    add_action( 'init','wpo_create_type_sliders' );
}