<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     Opal  Team <opalwordpressl@gmail.com >
 * @copyright  Copyright (C) 2014 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
class WPO_Breaking_Post_Widget extends WPO_Widget {
    
    public function __construct() {
        parent::__construct(
            // Base ID of your widget
            'wpo_breaking_post_widget',
            // Widget name will appear in UI
            __('WPO Breaking Post Widget', 'wpothemer')
        );
        $this->widgetName = 'breaking_post';
    }

    public function widget( $args, $instance ) {
        global $post;
        extract( $args );
        extract( $instance );

        echo ($before_widget);
        require($this->renderLayout($layout));
        echo ($after_widget);
    }
        
    // Widget Backend
    public function form( $instance ) {
        $defaults = array(  'title'         => 'Breaking News',
                            'layout'        => 'default' ,
                            'number'        => '5',
                            'post_per_page' => 1,
                            'filter'        =>  'recent',
                            'class'         => '');
        $instance = wp_parse_args((array) $instance, $defaults);
        $filters = array(
            'recent' => __('Recent post', 'wpothemer'),
            'mostview' => __('Most views', 'wpothemer'),
            'mostcomment' => __('Popular Posts', 'wpothemer'),

         );
        $colums = array(
            '1' => __('1 post', 'wpothemer'),
            '2' => __('2 posts', 'wpothemer'),
            '3' => __('3 posts', 'wpothemer'),
            '4' => __('4 posts', 'wpothemer'),
        );
        
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php echo __('Title: ', 'wpothemer'); ?></label>
            <br>
            <input id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($instance['title']); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id( 'layout' )); ?>"><?php echo __('Template Style:', 'wpothemer'); ?></label>
            <br>
            <select name="<?php echo esc_attr($this->get_field_name( 'layout' )); ?>" id="<?php echo esc_attr($this->get_field_id( 'layout' )); ?>">
                <?php foreach ($this->selectLayout() as $key => $value): ?>
                    <option value="<?php echo esc_attr( $value ); ?>" <?php selected( $instance['layout'], $value ); ?>><?php echo esc_html( $value ); ?></option>
                <?php endforeach; ?>
            </select>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php echo __('Number post:', 'wpothemer'); ?></label>
            <br>
            <input id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr( $instance['number'] ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id( 'post_per_page' )); ?>"><?php echo __('Post per page:', 'wpothemer'); ?></label>
            <br>
            <select name="<?php echo esc_attr($this->get_field_name( 'post_per_page' )); ?>" id="<?php echo esc_attr($this->get_field_id( 'post_per_page' )); ?>">
                <?php foreach ($colums as $k => $vl): ?>
                    <option value="<?php echo esc_attr( $k ); ?>" <?php selected( $instance['post_per_page'], $k ); ?>><?php echo esc_html( $vl ); ?></option>
                <?php endforeach; ?>
            </select>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id( 'filter' )); ?>"><?php echo __('Filter by:', 'wpothemer'); ?></label>
            <br>
            <select name="<?php echo esc_attr($this->get_field_name( 'filter' )); ?>" id="<?php echo esc_attr($this->get_field_id( 'filter' )); ?>">
                <?php foreach ($filters as $key => $value): ?>
                    <option value="<?php echo esc_attr( $key ); ?>" <?php selected( $instance['filter'], $key ); ?>><?php echo esc_html( $value ); ?></option>
                <?php endforeach; ?>
            </select>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('class')); ?>"><?php echo __('Class:', 'wpothemer'); ?></label>
            <br>
            <input id="<?php echo esc_attr($this->get_field_id('class')); ?>" name="<?php echo esc_attr($this->get_field_name('class')); ?>" type="text" value="<?php echo esc_attr( $instance['class'] ); ?>" />
        </p>

<?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;

        $instance['title']          = $new_instance['title'];
        $instance['number']         = $new_instance['number'];
        $instance['filter']         = $new_instance['filter'];
        $instance['class']          = $new_instance['class'];
        $instance['post_per_page']  = $new_instance['post_per_page'];
        $instance['layout']         = ( ! empty( $new_instance['layout'] ) ) ? $new_instance['layout'] : 'default';
        return $instance;

    }
}

register_widget( 'WPO_Breaking_Post_Widget' );