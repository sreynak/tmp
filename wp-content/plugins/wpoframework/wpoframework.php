<?php 
/*
  Plugin Name: WPO Framework
  Plugin URI: http://www.wpopal.com/
  Description: implement rick functions for themes base on wpo framework and load widgets for theme used, this is required.
  Version: 1.2
  Author: WP_Opal
  Author URI: http://www.wpopal.com
  License: GPLv2 or later
 */

 /**
  * $Desc
  *
  * @version    $Id$
  * @package    wpbase
  * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
  * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
  * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
  *
  * @website  http://www.wpopal.com
  * @support  http://www.wpopal.com/support/forum.html
  */

  define( 'WPO_PLUGIN_FRAMEWORK_URL', plugin_dir_url( __FILE__ ) );
  define( 'WPO_PLUGIN_FRAMEWORK_DIR', plugin_dir_path( __FILE__ ).'/' );
  define( 'WPO_PLUGIN_FRAMEWORK_TEMPLATE_DIR', WPO_PLUGIN_FRAMEWORK_DIR.'metabox_templates/' );

  /**
   * Loading Widgets
   */
  function wpo_fw_widgets_init(){
    if( !defined('WPO_THEME_DIR') ){
        return ;
      }  

      define( "WPO_PLUGIN_FRAMEWORK", true );
      define( 'WPO_PLUGIN_FRAMEWORK_WIDGET_TEMPLATES', WPO_THEME_TEMPLATE_DIR  );


      require( WPO_PLUGIN_FRAMEWORK_DIR.'setting.class.php' );
      require( WPO_PLUGIN_FRAMEWORK_DIR.'widget.class.php' );
     
      $widgets = array( 'twitter','posts','featured_post','top_rate','sliders','recent_comment','recent_post','tabs','flickr', 'video', 'socials', 'menu_vertical', 'socials_siderbar', 'breaking_post') ;
      $widgets = apply_filters( 'wpo_fw_load_widgets', $widgets );
      if( !empty($widgets) ){
          foreach( $widgets as $opt => $key ){

              $file = str_replace( 'enable_', '', $key );
              $filepath = WPO_PLUGIN_FRAMEWORK_DIR.'widgets/'.$file.'.php'; 
              if( file_exists($filepath) ){
                  require_once( $filepath );
              }
          }  
      }
  }
  add_action( 'widgets_init', 'wpo_fw_widgets_init' );

  /**
   * Loading Post Types
   */
  function wpo_fw_themer_setup(){
      
      if( !defined('WPO_THEME_DIR') ){
          return ;
      }  

      $opts = get_option( 'wpo_themer_posttype' );
      $opts = apply_filters( 'wpo_fw_load_posttypes', $opts );
      if( !empty($opts) ){
          foreach( $opts as $opt => $key ){

              $file = str_replace( 'enable_', '', $opt );
              $filepath = WPO_PLUGIN_FRAMEWORK_DIR.'posttypes/'.$file.'.php';
              if( file_exists($filepath) ){
                  require_once( $filepath );
              }
          }  
      }
  }   
  add_action( 'init', 'wpo_fw_themer_setup', 1 );   