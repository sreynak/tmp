<?php 
/**
 * Theme function
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <opalwordpress@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */	
	if ( ! isset( $content_width ) ) $content_width = 900;
	/**
	 * Functioning OF Template
	 */
	function edubase_wpo_header_style(){
	    $text_color = get_header_textcolor();
	    return ;
	}

	// This theme allows users to set a custom background.
	add_theme_support( 'custom-background', apply_filters( 'wpo_custom_background_args', array(
	    'default-color' => 'f5f5f5',
	  ) ) );

	function edubase_wpo_custom_header_setup() {
		  add_theme_support( 'custom-header', apply_filters( 'wpo_custom_header_args', array(
		    'default-text-color'     => 'fff',
		    'width'                  => 1920,
		    'height'                 => 90,
		    'flex-height'            => true,
		    'wp-head-callback'       => 'edubase_wpo_header_style',
		    'admin-head-callback'    => 'wpo_admin_header_style',
		    'admin-preview-callback' => 'wpo_admin_header_image',
		  ) ) );
	}

	add_action( 'after_setup_theme', 'edubase_wpo_custom_header_setup' );

	function edubase_theme_slug_setup() {
	   add_theme_support( 'title-tag' );
	}
	add_action( 'after_setup_theme', 'edubase_theme_slug_setup' );


	/**
	 * Add Custom Class To Body Tag
	 */
	add_filter('body_class', 'edubase_wpo_body_class');
	
	function edubase_wpo_body_class( $classes ){
		foreach ( $classes as $key => $value ) {
		
		if ( $value == 'boxed' || $value == 'default' ) 
			unset( $classes[$key] );
		}
		
		$classes[] = edubase_wpo_theme_options('configlayout');
		$classes[] = 'wpo-animate-scroll';

		return $classes;
	}

	/**
	 *
	 */
	function edubase_edubase_wpo_render_breadcrumbs_heading_tag( $tag ){
		if( ! is_single() ){
			return 'h1';
		}
		return $tag;
	}

	add_filter( 'edubase_edubase_wpo_render_breadcrumbs_heading_tag', 'edubase_edubase_wpo_render_breadcrumbs_heading_tag' );
	/**
	 * Customize Breadcrumd with Background Color Or Background Images
	 */
	function edubase_wpo_layout_breadcrumbs_bg(){
		$customize_image = get_header_image();
		if( isset($edubase_wpopconfig['background_breadcrumb']) && $edubase_wpopconfig['background_breadcrumb'] ){
			switch ( $edubase_wpopconfig['background_breadcrumb']) {
				case 'bg_image':
					$style = 'background-image: url('.esc_url_raw( $edubase_wpopconfig['image_breadcrumb'] ).');';
					break;
				
				case 'bg_color':
					$style = 'background-color:'.esc_attr( $edubase_wpopconfig['bg_color'] );
					break;

				case 'global':
					if( $customize_image  ){
						$style = "background: url('".esc_url_raw( $customize_image )."') no-repeat center center #f9f9f9";
						break;
					}
				default:
					$style="background: url('".get_template_directory_uri()."/images/breadcrumb.jpg') no-repeat center center #f9f9f9";
					break;
			}
		}elseif( isset( $customize_image) && !empty( $customize_image)) {
					$style="background: url('".esc_url_raw( $customize_image )."') no-repeat center center #f9f9f9";
		}else{
			$style="background: url('".get_template_directory_uri()."/images/breadcrumb.jpg') no-repeat center center #f9f9f9";
		}
		return $style;
	}

	/**
	 * Hooks to render BreadCrumb
	 */
	function edubase_wpo_layout_breadcrumbs_render(){
		global $edubase_wpopconfig;
		if( is_front_page() ){
			return ;
		}
		$style = edubase_wpo_layout_breadcrumbs_bg();
?>
	    <div class="wpo-breadcrumbs" style="<?php echo ($style);?>">
	        <?php edubase_wpo_breadcrumb(); ?>
	    </div>
 	<?php 
	}

	add_action( 'edubase_wpo_layout_breadcrumbs_render', 'edubase_wpo_layout_breadcrumbs_render' );

	/**
	 * Hook To Renderr With Layout Having Configed Layout as left-right, or fullwidth 
	 * With Open Tags
	 */
	function edubase_wpo_layout_template_before(){
		global $edubase_wpopconfig; 
	?>
		
		<section id="wpo-mainbody" class="wpo-mainbody default-template clearfix">
	  		<div class="container<?php if( isset($edubase_wpopconfig['layout'])&&$edubase_wpopconfig['layout']=='fullwidth') { ?>-fuild<?php } ?>">
	      	<div class="container-inner">
	        		<div class="row">
	          		<?php get_sidebar( 'left' );  ?>
			        <!-- MAIN CONTENT -->
			        <div id="wpo-content" class="<?php echo esc_attr( $edubase_wpopconfig['main']['class'] ); ?>">
			            <div class="wpo-content">

	<?php }  
	add_action( 'edubase_wpo_layout_template_before', 'edubase_wpo_layout_template_before' );

	/**
	 * Hook To Renderr With Layout Having Configed Layout as left-right, or fullwidth 
	 * With Close Tags
	 */
	function edubase_wpo_layout_template_after(){
		global $edubase_wpopconfig; 
	?>	
				         	</div>
			            </div>
			          <!-- //MAIN CONTENT -->
			          <?php get_sidebar( 'right' );  ?>
			         </div>
			   </div>
		   </div>
		</section>

	<?php }
	add_action( 'edubase_wpo_layout_template_after', 'edubase_wpo_layout_template_after' );
?>