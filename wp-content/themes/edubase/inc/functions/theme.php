<?php
/**
 * Defined function to render contents or process logic related with rendering.
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */


if ( version_compare( $GLOBALS['wp_version'], '4.1', '<' ) ) :
  /**
  * Filters wp_title to print a neat <title> tag based on what is being viewed.
  *
  * @param string $title Default title text for current view.
  * @param string $sep Optional separator.
  * @return string The filtered title.
  */
  function edubase_wpo_wp_title( $title, $sep ) {
    if ( is_feed() ) {
      return $title;
    }
    
    global $page, $paged;
    
    // Add the blog name
    $title .= get_bloginfo( 'name', 'display' );
    
    // Add the blog description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    
    if ( $site_description && ( is_home() || is_front_page() ) ) {
      $title .= " $sep $site_description";
    }
    
    // Add a page number if necessary:
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
      $title .= " $sep " . sprintf( __( 'Page %s', '_s' ), max( $paged, $page ) );
    }
    
    return $title;
    
  }
    
  add_filter( 'wp_title', 'edubase_wpo_wp_title', 10, 2 );
  
  /**
  * Title shim for sites older than WordPress 4.1.
  *
  * @link https://make.wordpress.org/core/2014/10/29/title-tags-in-4-1/
  * @todo Remove this function when WordPress 4.3 is released.
  */
  function edubase_wpo_render_title() {
  ?>
    <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php
  }
  add_action( 'wp_head', 'edubase_wpo_render_title' );
endif;


/** 
 * Configure columns with width via bootstrap classes for each layou type
 */
function edubase_wpo_config_layout($layout,$config=array()){
    switch ($layout) {
    // Two Sidebar
    case 'leftmainright':
        $config['left-sidebar']['show']   = true;
        $config['left-sidebar']['class']  ='col-md-3';
        $config['right-sidebar']['class'] ='col-md-3';
        $config['right-sidebar']['show']  = true;
        $config['main']['class']    ='col-xs-12 col-md-6';
    break;
    //One Sidebar Right
    case 'mainright':
        $config['left-sidebar']['show']   = false;
        $config['right-sidebar']['show']  = true;
        $config['main']['class']      ='col-xs-12 col-md-9 no-sidebar-left';
        $config['right-sidebar']['class']   ='col-xs-12 col-md-3';
    break;
    // One Sidebar Left
    case 'leftmain':
        $config['left-sidebar']['show']   = true;
        $config['right-sidebar']['show']  = false;
        $config['left-sidebar']['class']  ='col-xs-12 col-md-3';
        $config['main']['class']    ='col-xs-12 col-md-9 no-sidebar-right';
    break;

    // Fullwidth
    default:
        $config['left-sidebar']['show']   = false;
        $config['right-sidebar']['show']  = false;
        $config['main']['class']      ='col-xs-12 no-sidebar';
        break;
    }

    return $config;
}

/**
 * include breadcrumb layout
 */
function edubase_wpo_breadcrumb(){
    if(is_file(EDUBASE_WPO_THEME_TEMPLATE_DIR.'elements/breadcrumb.php')){
        require (EDUBASE_WPO_THEME_TEMPLATE_DIR.'elements/breadcrumb.php');
    }
}
 
/**
 * get list of menu group
 */
function edubase_wpo_get_menugroups(){
    $menus = wp_get_nav_menus( );
    $option_menu = array(''=>'---Select Menu---');
    foreach ($menus as $menu) {
        $option_menu[$menu->term_id]=$menu->name;
    }

    return $option_menu;
}

/**
 * Search With Category
 */
if(!function_exists('edubase_categories_searchform')){
    function edubase_categories_searchform(){
        if(class_exists('WooCommerce')){
        	global $wpdb;
			$dropdown_args = array(
                'show_counts'        => false,
                'hierarchical'       => true,
                'show_uncategorized' => 0
            );
        ?>
		<form role="search" method="get" class="input-group search-category" action="<?php echo esc_url( home_url('/') ); ?>">
            <div class="input-group-addon search-category-container">
            	<label class="select">
            		<?php wc_product_dropdown_categories( $dropdown_args ); ?>
            	</label>
            </div>
            <input name="s" id="s" maxlength="60" class="form-control search-category-input" type="text" size="20" placeholder="<?php esc_html_e('Enter search...', 'edubase'); ?>">
            <div class="input-group-btn">
                <label class="btn btn-link btn-search">
                  <span id="wpo-title-search" class="title-search hidden"><?php _e('Search', 'edubase') ?></span>
                  <input type="submit" id="searchsubmit" class="fa searchsubmit" value="&#xf002;"/>
                </label>
                <input type="hidden" name="post_type" value="product"/>
            </div>
        </form>
        <?php
        }else{
        	get_search_form();
        }
    }
}

/**
 * Pagination Navigation
 */
if(!function_exists('edubase_wpo_pagination_nav')){
    function edubase_wpo_pagination_nav($per_page,$total,$max_num_pages=''){
        ?>
        <section class="wpo-pagination">
            <?php global  $wp_query; ?>
            <?php edubase_wpo_pagination($prev = __('Previous','edubase'), $next = __('Next','edubase'), $pages=$max_num_pages ,array('class'=>'pull-left')); ?>
            <div class="result-count pull-right">
                <?php
                $paged    = max( 1, $wp_query->get( 'paged' ) );
                $first    = ( $per_page * $paged ) - $per_page + 1;
                $last     = min( $total, $wp_query->get( 'posts_per_page' ) * $paged );

                if ( 1 == $total ) {
                    _e( 'Showing the single result', 'woocommerce' );
                } elseif ( $total <= $per_page || -1 == $per_page ) {
                    printf( __( 'Showing all %d results', 'woocommerce' ), $total );
                } else {
                    printf( _x( 'Showing %1$d to %2$d of %3$d results', '%1$d = first, %2$d = last, %3$d = total', 'woocommerce' ), $first, $last, $total );
                }
                ?>
            </div>
        </section>
    <?php
    }
}

/**
 * Gener paginations
 */
if(!function_exists('edubase_wpo_pagination')){
    //page navegation
    function edubase_wpo_pagination($prev = 'Prev', $next = 'Next', $pages='' ,$args=array('class'=>'')) {
        global $wp_query, $wp_rewrite;
        $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
        if($pages==''){
            global $wp_query;
             $pages = $wp_query->max_num_pages;
             if(!$pages)
             {
                 $pages = 1;
             }
        }
        $pagination = array(
            'base' => @add_query_arg('paged','%#%'),
            'format' => '',
            'total' => $pages,
            'current' => $current,
            'prev_text' => $prev,
            'next_text' => $next,
            'type' => 'array'
        );

        if( $wp_rewrite->using_permalinks() )
            $pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );

        
        if(isset( $_GET['s'])){
            $cq = $_GET['s'];
            $sq = str_replace(" ", "+", $cq);
        }
        
        if( !empty($wp_query->query_vars['s']) ){
            $pagination['add_args'] = array( 's' => $sq);
        }
        if(paginate_links( $pagination )!=''){
            $paginations = paginate_links( $pagination );
            echo '<ul class="pagination '.esc_attr( $args["class"] ).'">';
                foreach ($paginations as $key => $pg) {
                    echo '<li>'. $pg .'</li>';
                }
            echo '</ul>';
        }
    }
}

if(!function_exists('edubase_wpo_getcontent')){
    function edubase_wpo_getcontent( $config ){
        $postconfig = get_post_meta(get_the_ID(),$config['type'],true);
        if( isset($postconfig[$config['format']]) && !empty( $postconfig[$config['format']] ) ) 
            return $postconfig[ $config['format'] ];
        return false;
    }
}

if(!function_exists('edubase_wpo_share_box')){
    function edubase_wpo_share_box( $layout='',$args=array() ){
    $default = array(
      'position' => 'top',
      'animation' => 'true'
      );
    $args = wp_parse_args( (array) $args, $default );
    
    $path = EDUBASE_WPO_THEME_TEMPLATE_DIR.'elements/sharebox';
    if( $layout!='' ){
      $path = $path.'-'.$layout;
    }
    $path .= '.php';

    if( is_file($path) ){
      require($path);
    }
 
    }
}
 
if(!function_exists('edubase_wpo_theme_comment')){
    function edubase_wpo_theme_comment($comment, $args, $depth){
      if(is_file(EDUBASE_WPO_THEME_TEMPLATE_DIR.'list_comments.php')){
        require (EDUBASE_WPO_THEME_TEMPLATE_DIR.'list_comments.php');
      }
    }
}
 
if(!function_exists('edubase_wpo_render_togglebutton')){
    function edubase_wpo_render_togglebutton($class='btn-inverse-danger', $toggle='offcanvas'){
    ?>
        <button data-toggle="<?php echo esc_attr($toggle); ?>" class="btn btn-offcanvas btn-toggle-canvas <?php echo esc_attr( $class ); ?>" type="button">
           <i class="fa fa-bars"></i>
        </button>
    <?php
    }
}

if(  !function_exists("edubase_wpo_render_breadcrumbs") ){
  function edubase_wpo_render_breadcrumbs( $showheading=false, $tag='h2' ){
      
      global $pagenow; 

      $tag = apply_filters( 'edubase_edubase_wpo_render_breadcrumbs_heading_tag', $tag );
      
      $delimiter = '&nbsp/&nbsp';
      $home = __('Home', 'edubase');
      $before = '<span class="active">';
      $after = '</span>';

      $html = '';
      $heading = get_the_title();
      if (!is_home() && !is_front_page() || is_paged()) {

       $html .= '<ol class="list-unstyled breadcrumb-links">';

        global $post;
        $homeLink = home_url();
         $html .=  '<li><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . '</li> ';

        if (is_category()) {
          global $wp_query;
          $cat_obj = $wp_query->get_queried_object();
          $thisCat = $cat_obj->term_id;
          $thisCat = get_category($thisCat);
          $parentCat = get_category($thisCat->parent);
          if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
          
           $heading = $before . single_cat_title('', false) . $after;
        } elseif (is_day()) {
           $html .=  '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';
           $html .=  '<li><a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a></li> ' . $delimiter . ' ';
        
           $heading = $before . get_the_time('d') . $after;
        } elseif (is_month()) {
           $html .=  '<a href="' . get_year_link(get_the_time('Y')) . '" >' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';
        
           $heading = $before . get_the_time('F') . $after;
        } elseif (is_year()) {
          
           $heading = $before . get_the_time('Y') . $after;
        } elseif (is_single() && !is_attachment()) { 
          if ( get_post_type() != 'post' ) {
            $post_type = get_post_type_object(get_post_type());
            $slug = $post_type->rewrite;
             $html .=  '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a></li> ' . $delimiter . ' ';
           
             $heading =  $before . get_the_title() . $after;
          } else {
            $cat = get_the_category(); $cat = $cat[0];
            $html .=  get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
          
            $heading =  $before . get_the_title() . $after;
          }

        } elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404() && !is_tag()) {
          $post_type = get_post_type_object(get_post_type());
          if(  $post_type  )
         
           $heading =  $before . $post_type->labels->singular_name . $after;
        } elseif (is_attachment()) {
          $parent = get_post($post->post_parent);
          $cat = get_the_category($parent->ID); 
          if( isset( $cat[0] ) ){
            $cat = $cat[0]; 
            $html .=  get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
          }
           $html .=  '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a></li> ' . $delimiter . ' ';
           

           $heading =  $before . get_the_title() . $after;

        } elseif ( is_page() && !$post->post_parent ) {
        
           $heading = $before . get_the_title() . $after;
        } elseif ( is_page() && $post->post_parent ) { 
          $parent_id  = $post->post_parent;
          $breadcrumbs = array();
          while ($parent_id) {
            $page = get_page($parent_id);
            $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a></li>';
            $parent_id  = $page->post_parent;
          }
          $breadcrumbs = array_reverse($breadcrumbs);
          foreach ($breadcrumbs as $crumb) $html .= $crumb . ' ' . $delimiter . ' ';
       
          $heading = $before . get_the_title() . $after;

        } elseif ( is_search() ) {
      
           $heading =  $before . 'Search results for "' . get_search_query() . '"' . $after;
        } elseif ( is_tag() ) {
         
          $heading = $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
        } elseif ( is_author() ) {
          global $author;
          $userdata = get_userdata($author);
      
           $heading = $before . 'Articles posted by ' . $userdata->display_name . $after;
        } elseif ( is_404() ) {
 
          $heading = $before . 'Error 404' . $after;
        }

        if ( get_query_var('paged') ) {
          if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
         
          if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
        }

        $html .= '</ol>';
    
          echo '<'.$tag.' class="breadcrumb-heading">'.$heading.'</'.$tag.'>';
       
        echo trim($html); 
      }
  }
}

if ( !function_exists( 'edubase_wpo_print_style_footer' ) ) {
  function edubase_wpo_print_style_footer(){
    $footer = edubase_wpo_theme_options('footer-style','default');
    global $edubase_wpopconfig;
    if('page' == get_post_type()){
      if($edubase_wpopconfig['footer_skin'] && $edubase_wpopconfig['footer_skin']!='global'){
        $footer = $edubase_wpopconfig['footer_skin'];
      }
    }
    if($footer!='default'){
    $shortcodes_custom_css = get_post_meta( $footer, '_wpb_shortcodes_custom_css', true );
      if ( ! empty( $shortcodes_custom_css ) ) {
        echo '<style>
              '.$shortcodes_custom_css.'
            </style>
          ';
      }
    }
  }
  add_action('wp_head','edubase_wpo_print_style_footer', 18);
}

