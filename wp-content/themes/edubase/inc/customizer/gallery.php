<?php
add_action( 'customize_register', 'edubase_wp_ct_gallery_setting' );
function edubase_wp_ct_gallery_setting( $wp_customize ){
    
    $wp_customize->add_panel( 'panel_gallery', array(
        'priority' => 82,
        'capability' => 'edit_theme_options',
        'theme_supports' => '',
        'title' => __( 'Gallery', 'edubase' ),
        'description' =>__( 'Make default setting for page, general', 'edubase' ),
    ) );

    /**
     * Layout Setting
     */
    $wp_customize->add_section( 'gallery_layout_settings', array(
        'priority' => 1,
        'capability' => 'edit_theme_options',
        'theme_supports' => '',
        'title' => __( 'Layout Setting', 'edubase' ),
        'description' => '',
        'panel' => 'panel_gallery',
    ) );

     ///  Archive layout setting
    $wp_customize->add_setting( 'wpo_theme_options[gallery-archive-layout]', array(
        'capability' => 'edit_theme_options',
        'type'       => 'option',
        'default'   => 'mainright',
        'sanitize_callback' => 'sanitize_text_field'
    ) );

    $wp_customize->add_control( new Edubase_Wpo_Layout_DropDown( $wp_customize, 'wpo_theme_options[gallery-archive-layout]', array(
        'settings'  => 'wpo_theme_options[gallery-archive-layout]',
        'label'     => __('Archive Layout', 'edubase'),
        'section'   => 'gallery_layout_settings',
        'priority' => 1

    ) ) );

   
    $wp_customize->add_setting( 'wpo_theme_options[gallery-archive-left-sidebar]', array(
        'capability' => 'edit_theme_options',
        'type'       => 'option',
        'default'   => 'gallery-sidebar-left',
        'sanitize_callback' => 'sanitize_text_field'
    ) );

    
    $wp_customize->add_control( new Edubase_Wpo_Sidebar_DropDown( $wp_customize, 'wpo_theme_options[gallery-archive-left-sidebar]', array(
        'settings'  => 'wpo_theme_options[gallery-archive-left-sidebar]',
        'label'     => __('Archive Left Sidebar', 'edubase'),
        'section'   => 'gallery_layout_settings' ,
         'priority' => 2
    ) ) );

     /// 
    $wp_customize->add_setting( 'wpo_theme_options[gallery-archive-right-sidebar]', array(
        'capability' => 'edit_theme_options',
        'type'       => 'option',
        'default'   => 'gallery-sidebar-right',
        'sanitize_callback' => 'sanitize_text_field'
    ) );

    $wp_customize->add_control( new Edubase_Wpo_Sidebar_DropDown( $wp_customize, 'wpo_theme_options[gallery-archive-right-sidebar]', array(
        'settings'  => 'wpo_theme_options[gallery-archive-right-sidebar]',
        'label'     => __('Archive Right Sidebar', 'edubase'),
        'section'   => 'gallery_layout_settings',
         'priority' => 2 
    ) ) );

     ///  single layout setting
    $wp_customize->add_setting( 'wpo_theme_options[gallery-single-layout]', array(
        'capability' => 'edit_theme_options',
        'type'       => 'option',
        'default'   => 'mainright',
        'sanitize_callback' => 'sanitize_text_field'
    ) );

    $wp_customize->add_control( new Edubase_Wpo_Layout_DropDown( $wp_customize,  'wpo_theme_options[gallery-single-layout]', array(
        'settings'  => 'wpo_theme_options[gallery-single-layout]',
        'label'     => __('Single Blog Layout', 'edubase'),
        'section'   => 'gallery_layout_settings' 
    ) ) );

   
    $wp_customize->add_setting( 'wpo_theme_options[gallery-single-left-sidebar]', array(
        'capability' => 'edit_theme_options',
        'type'       => 'option',
        'default'   => 'gallery-sidebar-left',
        'sanitize_callback' => 'sanitize_text_field'
    ) );

    $wp_customize->add_control( new Edubase_Wpo_Sidebar_DropDown( $wp_customize,  'wpo_theme_options[gallery-single-left-sidebar]', array(
        'settings'  => 'wpo_theme_options[gallery-single-left-sidebar]',
        'label'     => __('Single gallery Left Sidebar', 'edubase'),
        'section'   => 'gallery_layout_settings' 
    ) ) );

     $wp_customize->add_setting( 'wpo_theme_options[gallery-single-right-sidebar]', array(
        'capability' => 'edit_theme_options',
        'type'       => 'option',
        'default'   => 'gallery-sidebar-right',
        'sanitize_callback' => 'sanitize_text_field'
    ) );

    $wp_customize->add_control( new Edubase_Wpo_Sidebar_DropDown( $wp_customize,  'wpo_theme_options[gallery-single-right-sidebar]', array(
        'settings'  => 'wpo_theme_options[gallery-single-right-sidebar]',
        'label'     => __('Single gallery Right Sidebar', 'edubase'),
        'section'   => 'gallery_layout_settings' 
    ) ) );

    /**
     * Archive Setting
     */
    $wp_customize->add_section( 'gallery_archive_general_settings', array(
        'priority' => 11,
        'capability' => 'edit_theme_options',
        'theme_supports' => '',
        'title' => __( 'Archive Setting', 'edubase' ),
        'description' => '',
        'panel' => 'panel_gallery',
    ) );

     $wp_customize->add_setting('wpo_theme_options[gallery-archive-column]', array(
        'capability' => 'edit_theme_options',
        'type'       => 'option',
        'default'   => '4',
        'sanitize_callback' => 'sanitize_text_field'
    ) );

    $wp_customize->add_control( 'wpo_theme_options[gallery-archive-column]', array(
        'label'      => __( 'Select column', 'edubase' ),
        'section'    => 'gallery_archive_general_settings',
        'type'       => 'select',
        'choices'     => array(
            '2' => __('2 column', 'edubase' ),
            '3' => __('3 column', 'edubase' ),
            '4' => __('4 column', 'edubase' ),
            '6' => __('6 column', 'edubase' ),
        )
    ) ); 
}
