<?php
if(EDUBASE_WPO_EVENT_ACTIVED){
    add_action( 'customize_register', 'edubase_wp_ct_event_setting' );
    function edubase_wp_ct_event_setting( $wp_customize ){
        
        $wp_customize->add_panel( 'panel_event', array(
            'priority' => 81,
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'title' => __( 'Event', 'edubase' ),
            'description' =>__( 'Make default setting for page, general', 'edubase' ),
        ) );

        /**
         * Layout Setting
         */
        $wp_customize->add_section( 'event_layout_settings', array(
            'priority' => 1,
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'title' => __( 'Layout Setting', 'edubase' ),
            'description' => '',
            'panel' => 'panel_event',
        ) );

        $wp_customize->add_setting( 'wpo_theme_options[event-archive-layout]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'mainright',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Edubase_Wpo_Layout_DropDown( $wp_customize, 'wpo_theme_options[event-archive-layout]', array(
            'settings'  => 'wpo_theme_options[event-archive-layout]',
            'label'     => __('Archive Layout', 'edubase'),
            'section'   => 'event_layout_settings',
            'priority' => 1

        ) ) );
       
        $wp_customize->add_setting( 'wpo_theme_options[event-archive-left-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'event-sidebar-left',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        
        $wp_customize->add_control( new Edubase_Wpo_Sidebar_DropDown( $wp_customize, 'wpo_theme_options[event-archive-left-sidebar]', array(
            'settings'  => 'wpo_theme_options[event-archive-left-sidebar]',
            'label'     => __('Archive Left Sidebar', 'edubase'),
            'section'   => 'event_layout_settings' ,
             'priority' => 2
        ) ) );

         /// 
        $wp_customize->add_setting( 'wpo_theme_options[event-archive-right-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'event-sidebar-right',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Edubase_Wpo_Sidebar_DropDown( $wp_customize, 'wpo_theme_options[event-archive-right-sidebar]', array(
            'settings'  => 'wpo_theme_options[event-archive-right-sidebar]',
            'label'     => __('Archive Right Sidebar', 'edubase'),
            'section'   => 'event_layout_settings',
             'priority' => 2 
        ) ) );

         ///  single layout setting
        $wp_customize->add_setting( 'wpo_theme_options[event-single-layout]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'mainright',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Edubase_Wpo_Layout_DropDown( $wp_customize,  'wpo_theme_options[event-single-layout]', array(
            'settings'  => 'wpo_theme_options[event-single-layout]',
            'label'     => __('Single Event Layout', 'edubase'),
            'section'   => 'event_layout_settings' 
        ) ) );

       
        $wp_customize->add_setting( 'wpo_theme_options[event-single-left-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'event-sidebar-left',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Edubase_Wpo_Sidebar_DropDown( $wp_customize,  'wpo_theme_options[event-single-left-sidebar]', array(
            'settings'  => 'wpo_theme_options[event-single-left-sidebar]',
            'label'     => __('Single Event Left Sidebar', 'edubase'),
            'section'   => 'event_layout_settings' 
        ) ) );

         $wp_customize->add_setting( 'wpo_theme_options[event-single-right-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'event-sidebar-right',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Edubase_Wpo_Sidebar_DropDown( $wp_customize,  'wpo_theme_options[event-single-right-sidebar]', array(
            'settings'  => 'wpo_theme_options[event-single-right-sidebar]',
            'label'     => __('Single event Right Sidebar', 'edubase'),
            'section'   => 'event_layout_settings' 
        ) ) );

    }
}