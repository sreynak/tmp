<?php
if(EDUBASE_WPO_IBEDUCATOR_ACTIVED){
    add_action( 'customize_register', 'edubase_wp_ct_courses_setting' );
    function edubase_wp_ct_courses_setting( $wp_customize ){
        
        $wp_customize->add_panel( 'panel_courses', array(
            'priority' => 81,
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'title' => __( 'Courses Setting', 'edubase' ),
            'description' =>__( 'Make default setting for page, general', 'edubase' ),
        ) );

        /**
         * Layout Setting
         */
        $wp_customize->add_section( 'courses_layout_settings', array(
            'priority' => 1,
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'title' => __( 'Layout Setting', 'edubase' ),
            'description' => '',
            'panel' => 'panel_courses',
        ));

        $wp_customize->add_setting( 'wpo_theme_options[courses-archive-layout]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'mainright',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Edubase_Wpo_Layout_DropDown( $wp_customize, 'wpo_theme_options[courses-archive-layout]', array(
            'settings'  => 'wpo_theme_options[courses-archive-layout]',
            'label'     => __('Archive Layout', 'edubase'),
            'section'   => 'courses_layout_settings',
            'priority' => 1

        ) ) );
       
        $wp_customize->add_setting( 'wpo_theme_options[courses-archive-left-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'courses-sidebar-left',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Edubase_Wpo_Sidebar_DropDown( $wp_customize, 'wpo_theme_options[courses-archive-left-sidebar]', array(
            'settings'  => 'wpo_theme_options[courses-archive-left-sidebar]',
            'label'     => __('Archive Left Sidebar', 'edubase'),
            'section'   => 'courses_layout_settings' ,
             'priority' => 2
        ) ) );

         /// 
        $wp_customize->add_setting( 'wpo_theme_options[courses-archive-right-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'courses-sidebar-right',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Edubase_Wpo_Sidebar_DropDown( $wp_customize, 'wpo_theme_options[courses-archive-right-sidebar]', array(
            'settings'  => 'wpo_theme_options[courses-archive-right-sidebar]',
            'label'     => __('Archive Right Sidebar', 'edubase'),
            'section'   => 'courses_layout_settings',
             'priority' => 2 
        ) ) );

         ///  single layout setting
        $wp_customize->add_setting( 'wpo_theme_options[courses-single-layout]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'mainright',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Edubase_Wpo_Layout_DropDown( $wp_customize,  'wpo_theme_options[courses-single-layout]', array(
            'settings'  => 'wpo_theme_options[courses-single-layout]',
            'label'     => __('Single Course Layout', 'edubase'),
            'section'   => 'courses_layout_settings' 
        ) ) );

       
        $wp_customize->add_setting( 'wpo_theme_options[courses-single-left-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'courses-sidebar-left',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Edubase_Wpo_Sidebar_DropDown( $wp_customize,  'wpo_theme_options[courses-single-left-sidebar]', array(
            'settings'  => 'wpo_theme_options[courses-single-left-sidebar]',
            'label'     => __('Single Courses Left Sidebar', 'edubase'),
            'section'   => 'courses_layout_settings' 
        ) ) );

         $wp_customize->add_setting( 'wpo_theme_options[courses-single-right-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'courses-sidebar-right',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Edubase_Wpo_Sidebar_DropDown( $wp_customize,  'wpo_theme_options[courses-single-right-sidebar]', array(
            'settings'  => 'wpo_theme_options[courses-single-right-sidebar]',
            'label'     => __('Single courses Right Sidebar', 'edubase'),
            'section'   => 'courses_layout_settings' 
        ) ) );

        $wp_customize->add_setting('wpo_theme_options[courses-number-related-single]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => '3',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( 'wpo_theme_options[courses-number-related-single]', array(
            'label'      => __( 'Number Courses related single', 'edubase' ),
            'section'    => 'courses_layout_settings',
            'type'       => 'number',
            'default'     => '3'
        ) ); 

        /**
         * Archive Setting
         */
        $wp_customize->add_section( 'courses_archive_general_settings', array(
            'priority' => 11,
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'title' => __( 'Archive Setting', 'edubase' ),
            'description' => '',
            'panel' => 'panel_courses',
        ) );

         $wp_customize->add_setting('wpo_theme_options[courses-archive-column]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => '4',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( 'wpo_theme_options[courses-archive-column]', array(
            'label'      => __( 'Select column', 'edubase' ),
            'section'    => 'courses_archive_general_settings',
            'type'       => 'select',
            'choices'     => array(
                '2' => __('2 column', 'edubase' ),
                '3' => __('3 column', 'edubase' ),
                '4' => __('4 column', 'edubase' )
            )
        ) ); 
    }
}