<?php

 
	/*********************************************************************************************************************
	 *  Vertical menu
	 *********************************************************************************************************************/
	
    $option_menu = array( __('---Select Menu---','edubase')=>'');

    if( is_admin() ){
	    $menus = wp_get_nav_menus( array( 'orderby' => 'name' ) );
	    foreach ($menus as $menu) {
	    	$option_menu[$menu->name]=$menu->term_id;
	    }
	}    
	vc_map( array(
	    "name" => __("WPO Vertical Menu",'edubase'),
	    "base" => "wpo_verticalmenu",
	    "class" => "",
	    "category" => __('Opal Elements', 'edubase'),
	    "params" => array(
	    	array(
				"type" => "textfield",
				"heading" => __("Title", 'edubase'),
				"param_name" => "title",
				"value" => 'Vertical Menu'
			),
	    	array(
				"type" => "dropdown",
				"heading" => __("Menu", 'edubase'),
				"param_name" => "menu",
				"value" => $option_menu,
				"admin_label" => true,
				"description" => __("Select menu.", 'edubase')
			),
			array(
				"type" => "dropdown",
				"heading" => __("Position", 'edubase'),
				"param_name" => "postion",
				"value" => array(
						'left'=>'left',
						'right'=>'right'
					),
				"admin_label" => true,
				"description" => __("Postion Menu Vertical.", 'edubase')
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", 'edubase'),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
			)
	   	)
	));

	/*********************************************************************************************************************
	 * Pricing Table
	 *********************************************************************************************************************/
	vc_map( array(
	    "name" => __("WPO Pricing",'edubase'),
	    "base" => "wpo_pricing",
	    "description" => __('Make Plan for membership', 'edubase' ),
	    "class" => "",
	    "category" => __('Opal Elements', 'edubase'),
	    "params" => array(
	    	array(
				"type" => "textfield",
				"heading" => __("Title", 'edubase'),
				"param_name" => "title",
				"value" => '',
					"admin_label" => true
			),
			array(
				"type" => "textfield",
				"heading" => __("Price", 'edubase'),
				"param_name" => "price",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "textfield",
				"heading" => __("Currency", 'edubase'),
				"param_name" => "currency",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "textfield",
				"heading" => __("Period", 'edubase'),
				"param_name" => "period",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "textfield",
				"heading" => __("Subtitle", 'edubase'),
				"param_name" => "subtitle",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "dropdown",
				"heading" => __("Is Featured", 'edubase'),
				"param_name" => "featured",
				'value' 	=> array(  __('No', 'edubase') => 0,  __('Yes', 'edubase') => 1 ),
			),
			array(
				"type" => "dropdown",
				"heading" => __("Skin", 'edubase'),
				"param_name" => "skin",
				'value' 	=> array(  __('Skin 1', 'edubase') => 'v1',  __('Skin 2', 'edubase') => 'v2', __('Skin 3', 'edubase') => 'v3' ),
			),
			array(
				"type" => "dropdown",
				"heading" => __("Box Style", 'edubase'),
				"param_name" => "style",
				'value' 	=> array( 'boxed' => __('Boxed', 'edubase')),
			),

			array(
				"type" => "textarea_html",
				"heading" => __("Content", 'edubase'),
				"param_name" => "content",
				"value" => '',
				'description'	=> __('Allow  put html tags', 'edubase')
			),

			array(
				"type" => "textfield",
				"heading" => __("Link Title", 'edubase'),
				"param_name" => "linktitle",
				"value" => '',
				'description'	=> ''
			),

			array(
				"type" => "textfield",
				"heading" => __("Link", 'edubase'),
				"param_name" => "link",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", 'edubase'),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
			)
	   	)
	));

		

	/******************************
	 * Our Team
	 ******************************/
	vc_map( array(
	    "name" => __("(Team) Grid Style",'edubase'),
	    "base" => "wpo_team",
	    "class" => "",
	    "description" => 'Show Personal Profile Info',
	    "category" => __('Opal Elements', 'edubase'),
	    "params" => array(
	    	array(
				"type" => "textfield",
				"heading" => __("Title", 'edubase'),
				"param_name" => "title",
				"value" => '',
					"admin_label" => true
			),
			array(
				"type" => "attach_image",
				"heading" => __("Photo", 'edubase'),
				"param_name" => "photo",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "textfield",
				"heading" => __("Job", 'edubase'),
				"param_name" => "job",
				"value" => 'CEO',
				'description'	=>  ''
			),

			array(
				"type" => "textarea",
				"heading" => __("Information", 'edubase'),
				"param_name" => "information",
				"value" => '',
				'description'	=> __('Allow  put html tags', 'edubase')
			),
			array(
				"type" => "textfield",
				"heading" => __("Phone", 'edubase'),
				"param_name" => "phone",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "textfield",
				"heading" => __("Google Plus", 'edubase'),
				"param_name" => "google",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "textfield",
				"heading" => __("Facebook", 'edubase'),
				"param_name" => "facebook",
				"value" => '',
				'description'	=> ''
			),

			array(
				"type" => "textfield",
				"heading" => __("Twitter", 'edubase'),
				"param_name" => "twitter",
				"value" => '',
				'description'	=> ''
			),

			array(
				"type" => "textfield",
				"heading" => __("Pinterest", 'edubase'),
				"param_name" => "pinterest",
				"value" => '',
				'description'	=> ''
			),

			array(
				"type" => "textfield",
				"heading" => __("Linked In", 'edubase'),
				"param_name" => "linkedin",
				"value" => '',
				'description'	=> ''
			),

			array(
				"type" => "dropdown",
				"heading" => __("Style", 'edubase'),
				"param_name" => "style",
				'value' 	=> array(  __('circle', 'edubase') => 'circle',  __('vertical', 'edubase') => 'vertical',  __('horizontal', 'edubase') => 'horizontal' , __('special', 'edubase')  => 'special' , __('hover', 'edubase') => 'team-hover')
			),

			array(
				"type" => "textfield",
				"heading" => __("Extra class name", 'edubase'),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
			)
	   	)
	));
	
	/******************************
	 * Our Team
	 ******************************/
	vc_map( array(
		"name" => __("(Team) List Style",'edubase'),
		"base" => "wpo_team_list",
		"class" => "",
		"description" => __('Show Info In List Style', 'edubase'),
		"category" => __('Opal Elements', 'edubase'),
	    "params" => array(
	    	array(
				"type" => "textfield",
				"heading" => __("Title", 'edubase'),
				"param_name" => "title",
				"value" => '',
					"admin_label" => true
			),
			array(
				"type" => "attach_image",
				"heading" => __("Photo", 'edubase'),
				"param_name" => "photo",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "textfield",
				"heading" => __("Phone", 'edubase'),
				"param_name" => "phone",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "textarea",
				"heading" => __("information", 'edubase'),
				"param_name" => "information",
				"value" => '',
				'description'	=> __('Allow  put html tags', 'edubase')
			),
			array(
				"type" => "textarea",
				"heading" => __("blockquote", 'edubase'),
				"param_name" => "blockquote",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "textfield",
				"heading" => __("Email", 'edubase'),
				"param_name" => "email",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "textfield",
				"heading" => __("Facebook", 'edubase'),
				"param_name" => "facebook",
				"value" => '',
				'description'	=> ''
			),

			array(
				"type" => "textfield",
				"heading" => __("Twitter", 'edubase'),
				"param_name" => "twitter",
				"value" => '',
				'description'	=> ''
			),

			array(
				"type" => "textfield",
				"heading" => __("Linked In", 'edubase'),
				"param_name" => "linkedin",
				"value" => '',
				'description'	=> ''
			),

			array(
				"type" => "dropdown",
				"heading" => __("Style", 'edubase'),
				"param_name" => "style",
				'value' 	=> array( 'circle' => __('circle', 'edubase'), 'vertical' => __('vertical', 'edubase') , 'horizontal' => __('horizontal', 'edubase') ),
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", 'edubase'),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
			)

	   	)
	));
	
	function parramMegaLayout($settings,$value){
		$dependency = vc_generate_dependencies_attributes($settings);
		ob_start();
		?>
			<div class="layout_images">
				<?php foreach ($settings['layout_images'] as $key => $image) {
					echo '<img src="'.$image.'" data-layout="'.$key.'" class="'.$key.' '.(($key==$value)?'active':'').'">';
				} ?>
			</div>
			<input 	type="hidden"
					name="<?php echo esc_attr($settings['param_name']); ?>"
					class="layout_image_field wpb_vc_param_value wpb-textinput <?php echo esc_attr($settings['param_name']).' '.esc_attr($settings['type']).'_field'; ?>"
					value="<?php echo esc_attr($value); ?>" <?php echo trim($dependency); ?>>
		<?php
		return ob_get_clean();
	}
	 
	/* Heading Text Block
	---------------------------------------------------------- */
	vc_map( array(
		'name'        => __( 'WPO Widget Heading','edubase'),
		'base'        => 'wpo_title_heading',
		"class"       => "",
		"category"    => __('Opal Elements', 'edubase'),
		'description' => __( 'Create title for one Widget', 'edubase' ),
		"params"      => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Widget title', 'edubase' ),
				'param_name' => 'title',
				'value'       => __( 'Title', 'edubase' ),
				'description' => __( 'Enter heading title.', 'edubase' ),
				"admin_label" => true
			),
			array(
			    'type' => 'colorpicker',
			    'heading' => __( 'Title Color', 'edubase' ),
			    'param_name' => 'font_color',
			    'description' => __( 'Select font color', 'edubase' )
			),
			array(
				'type' => 'dropdown',
				'heading' => __( 'Title font size', 'edubase' ),
				'param_name' => 'size',
				'value' => array(
					__( 'Large', 'edubase' ) => 'font-size-lg',
					__( 'Medium', 'edubase' ) => 'font-size-md',
					__( 'Small', 'edubase' ) => 'font-size-sm',
					__( 'Extra small', 'edubase' ) => 'font-size-xs'
				),
				'description' => __( 'Select title font size.', 'edubase' )
			),
			array(
				'type' => 'dropdown',
				'heading' => __( 'Title Align', 'edubase' ),
				'param_name' => 'title_align',
				'value' => array(
					__( 'Align center', 'edubase' ) => 'separator_align_center',
					__( 'Align left', 'edubase' ) => 'separator_align_left',
					__( 'Align right', 'edubase' ) => "separator_align_right"
				),
				'description' => __( 'Select title align.', 'edubase' )
			),
			array(
				"type" => "textarea",
				'heading' => __( 'Description', 'edubase' ),
				"param_name" => "descript",
				"value" => '',
				'description' => __( 'Enter description for title.', 'edubase' )
		    ),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'edubase' ),
				'param_name' => 'el_class',
				'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'edubase' )
			)
		),
	));
	

	/*********************************************************************************************************************
	*  Reassuarence
	*********************************************************************************************************************/
	vc_map( array(
	    "name" => __("WPO Reassuarence",'edubase'),
	    "base" => "wpo_reassuarence",
	    "class" => "",
	    "description"=> __('Counting number with your term', 'edubase'),
	    "category" => __('Opal Elements', 'edubase'),
	    "params" => array(
	    	array(
				"type" => "textfield",
				"heading" => __("Title", 'edubase'),
				"param_name" => "title",
				"value" => '',
				"admin_label" => true
			),

		 

		 	array(
				"type" => "textfield",
				"heading" => __("FontAwsome Icon", 'edubase'),
				"param_name" => "icon",
				"value" => '',
				'description'	=> __( 'This support display icon from FontAwsome, Please click', 'edubase' )
								. '<a href="' . ( is_ssl()  ? 'https' : 'http') . '://fortawesome.github.io/Font-Awesome/" target="_blank"> '
							. __( 'here to see the list', 'edubase' ) . '</a>' . __( 'and use class icons-lg, icons-md, icons-sm to change its size', 'edubase' )
			),

			array(
				"type" => "textfield",
				"heading" => __("Icon Color", 'edubase'),
				"param_name" => "color",
				"value" => 'black'
			),


			array(
				"type" => "attach_image",
				"description" => __("If you upload an image, icon will not show.", 'edubase'),
				"param_name" => "image",
				"value" => '',
				'heading'	=> __('Image', 'edubase' )
			),

		 	array(
				"type" => "textarea",
				"heading" => __("Short Information", 'edubase'),
				"param_name" => "description",
				"value" => '',
				'description'	=> __('Allow  put html tags', 'edubase')
			),


		 	array(
				"type" => "textarea_html",
				"heading" => __("Detail Information", 'edubase'),
				"param_name" => "information",
				"value" => '',
				'description'	=> __('Allow  put html tags', 'edubase')
			),


			array(
				"type" => "dropdown",
				"heading" => __("Style", 'edubase'),
				"param_name" => "style",
				'value' 	=> array( 'circle' => __('circle', 'edubase'), 'vertical' => __('vertical', 'edubase') , 'horizontal' => __('horizontal', 'edubase') ),
			),

			array(
				"type" => "textfield",
				"heading" => __("Extra class name", 'edubase'),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
			)
	   	)
	));
	


	/*********************************************************************************************************************
	 *  Facebook Like Box
	 *********************************************************************************************************************/
	vc_map( array(
		'name'        => __( 'WPO Facebook Like Box','edubase'),
		'base'        => 'wpo_facebook_like_box',
		"class"       => "",
		"category"    => __('Opal Elements', 'edubase'),
		'description' => __( 'Create title for one block', 'edubase' ),
		"params"      => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Widget', 'edubase' ),
				'param_name' => 'title',
				'value'       => __( 'Find us on Facebook', 'edubase' ),
				'description' => __( 'Enter heading title.', 'edubase' ),
				"admin_label" => true
			),
			 
		 
			array(
				"type" => "textfield",
				"heading" => __("Facebook Page URL", 'edubase'),
				"param_name" => "page_url",
				"value" => "#"
			),
			array(
				"type" => "textfield",
				"heading" => __("Width", 'edubase'),
				"param_name" => "width",
				"value" => 268
			),		
			array(
				'type' => 'dropdown',
				'heading' => __( 'Color Scheme', 'edubase' ),
				'param_name' => 'color_scheme',
				'value' => array(
					__( 'Light', 'edubase' ) => 'light',
					__( 'Dark', 'edubase' ) => 'dark'
				),
				'description' => __( 'Select Color Scheme.', 'edubase' )
			),
			array(
                "type" => "checkbox",
                "heading" => __("Show faces", 'edubase'),
                "param_name" => "show_faces",
                "value" => array(
                    'Yes, please' => true
                )
			),
			array(
                "type" => "checkbox",
                "heading" => __("Show stream", 'edubase'),
                "param_name" => "show_stream",
                "value" => array(
                    'Yes, please' => true
                )
			),
			array(
                "type" => "checkbox",
                "heading" => __("Show facebook header", 'edubase'),
                "param_name" => "show_header",
                "value" => array(
                    'Yes, please' => true
                )
			),	
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", 'edubase'),
				"param_name" => "el_class",
				"value" => ''
			),									
		),
	));

	//Element Coming soon
	vc_map( array(
		'name'        => __( 'WPO Coming soon','edubase'),
		'base'        => 'wpo_coming_soon',
		"class"       => "",
		"style" 	  => "",
		"category"    => __('Opal Elements', 'edubase'),
		'description' => __( 'Create Element Coming soon', 'edubase' ),
		"params"      => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Title', 'edubase' ),
				'param_name' => 'title',
				'value'       => __( '', 'edubase' ),
				'description' => __( 'Enter heading title.', 'edubase' ),
				"admin_label" => true
			),
			array(
			    'type' => 'wpo_datepicker',
			    'heading' => __( 'Date coming soon', 'edubase' ),
			    'param_name' => 'date_comingsoon',
			    'description' => __( 'Enter Date Coming soon', 'edubase' )
			),
			array(
				"type" => "textarea",
				'heading' => __( 'Description', 'edubase' ),
				"param_name" => "description",
				"value" => '',
				'description' => __( 'Enter description for title.', 'edubase' )
		    ),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'edubase' ),
				'param_name' => 'el_class',
				'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'edubase' )
			),
			array(
				"type" => "dropdown",
				"heading" => __("Style", 'edubase'),
				"param_name" => "style",
				'value' 	=> array( 
					'style 1' => __('countdown-v1', 'edubase'), 
					'style 2' => __('countdown-v2', 'edubase') ),
			)
		),
	));

	/*********************************************************************************************************************
	 *  Video Box
	*********************************************************************************************************************/
	vc_map( array(
	    "name" => __("Video box",'edubase'),
	    "base" => "wpo_video_box",
	    'icon' => 'icon-wpb-news-15',
	    'description'=>'Show Video box',
	    "class" => "",
	    "category" => __('Opal Elements', 'edubase'),
	    "params" => array(
	    	array(
				"type" => "textfield",
				"heading" => __("Title", 'edubase'),
				"param_name" => "title",
				"value" => '',
				"admin_label" => true
			),
	    	array(
				"type" => "textfield",
				"heading" => __("Data Url", 'edubase'),
				"param_name" => "url",
				"value" => '',
				"admin_label" => true,
				"description" => "example: //player.vimeo.com/video/88558878?color=ffffff&title=0&byline=0&portrait=0",
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Data Width', 'edubase' ),
				'param_name' => 'width',
				'value' => '1920'
		    ),
			array(
				'type' => 'textfield',
				'heading' => __( 'Data Height', 'edubase' ),
				'param_name' => 'height',
				'value' => '1080'
		    ),
			array(
				"type" => "attach_image",
				"heading" => __("Backgroup Image", 'edubase'),
				"param_name" => "imagebg",
				"value" => '',
				'description'	=> ''
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'edubase' ),
				'param_name' => 'el_class',
				'value' => ''
		    )
	   	)
	));

vc_map( array(
		'name'        => __( 'WPO Block Heading','edubase'),
		'base'        => 'wpo_block_heading',
		"class"       => "",
		"category"    => __('Opal Elements', 'edubase'),
		'description' => __( 'Create Block Heading with info + icon', 'edubase' ),
		"params"      => array(
			
			array(
				'type' => 'textfield',
				'heading' => __( 'Block Heading Title', 'edubase' ),
				'param_name' => 'title',
				'value'       => __( '', 'edubase' ),
				'description' => __( 'Enter heading title.', 'edubase' ),
				"admin_label" => true
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Sub Heading Title', 'edubase' ),
				'param_name' => 'subheading',
				'value'       => __( '', 'edubase' ),
				'description' => __( 'Enter Sub heading title.', 'edubase' ),
				"admin_label" => true
			),
			array(
				'type' => 'dropdown',
				'heading' => __( 'Title Alignment', 'edubase' ),
				'param_name' => 'alignment',
				'value' => array(
					__( 'Align left', 'edubase' ) => 'separator_align_left',
					__( 'Align center', 'edubase' ) => 'separator_align_center',
					__( 'Align right', 'edubase' ) => 'separator_align_right'
				)
			),
		 	 array(
                'type' => 'dropdown',
                'heading' => __( 'Heading Style', 'edubase' ),
                'param_name' => 'heading_style',
                'value' => array(
                    __( 'Version 1', 'edubase' ) => 'v1',
                    __( 'Version 2', 'edubase' ) => 'v2',
                    __( 'Version 3', 'edubase' ) => 'v3',
					__( 'Version 4', 'edubase' ) => 'v4',
					__( 'Version 5', 'edubase' ) => 'v5',
					__( 'Version 6', 'edubase' ) => 'v6',
					__( 'Version 7', 'edubase' ) => 'v7',
					__( 'Version 8', 'edubase' ) => 'v8',
					__( 'Version 9', 'edubase' ) => 'v9',
					__( 'Version 10', 'edubase' ) => 'v10',
					__( 'Version 11', 'edubase' ) => 'v11',
					__( 'Version 12', 'edubase' ) => 'v12',
					__( 'Version 13', 'edubase' ) => 'v13',
                )
            ),
			array(
				"type" => "textarea",
				'heading' => __( 'Description', 'edubase' ),
				"param_name" => "descript",
				"value" => '',
				'description' => __( 'Enter description for title.', 'edubase' )
		    ),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'edubase' ),
				'param_name' => 'el_class',
				'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'edubase' )
			)
		),
	));


		/*********************************************************************************************************************
		 *  Our Service
		 *********************************************************************************************************************/
		vc_map( array(
		    "name" => __("Opal Featured Box",'edubase'),
		    "base" => "wpo_featuredbox",
		    "description"=> __('Decreale Service Info', 'edubase'),
		    "class" => "",
		    "category" => __('Opal Elements', 'edubase'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => __("Title", 'edubase'),
					"param_name" => "title",
					"value" => '',
						"admin_label" => true
				),

		    	array(
					"type" => "textfield",
					"heading" => __("Sub Title", 'edubase'),
					"param_name" => "subtitle",
					"value" => '',
						"admin_label" => false
				),
				
		    	array(
					"type" => "colorpicker",
					"heading" => __("Font Text Color", 'edubase'),
					"param_name" => "color",
					"value" => '',
						"admin_label" => false
				),

				array(
					"type" => "dropdown",
					"heading" => __("Style", 'edubase'),
					"param_name" => "style",
					'admin_label' => true,
					'value' 	=> array(
						__('Icon Right', 'edubase') => 'icon-box-left', 
						__('Icon Left', 'edubase') => 'icon-box-right', 
						__('Icon Center', 'edubase') => 'icon-box-center', 
						__('Icon Top - Text Left', 'edubase') => 'icon-box-top text-left', 
						__('Icon Top - Text Right', 'edubase') => 'icon-box-top text-right', 
						__('Icon Right - Text Left', 'edubase') => 'icon-right-text-left',
						__('Background Box Hover', 'edubase') => 'background-box-hover space-padding-0'
					),
				),	
				array(
					'type' => 'dropdown',
					'heading' => __( 'Background Block', 'edubase' ),
					'param_name' => 'background',
					'value' => array(
						__( 'None', 'edubase' ) => '',
						__( 'Success', 'edubase' ) => 'bg-success',
						__( 'Info', 'edubase' ) => 'bg-info',
						__( 'Danger', 'edubase' ) => 'bg-danger',
						__( 'Warning', 'edubase' ) => 'bg-warning',
						__( 'Light', 'edubase' ) => 'bg-default',
					)
				),

			 	array(
					"type" => "textfield",
					"heading" => __("FontAwsome Icon", 'edubase'),
					"param_name" => "icon",
					"value" => '',
					'description'	=> __( 'This support display icon from FontAwsome, Please click', 'edubase' )
									. '<a href="' . ( is_ssl()  ? 'https' : 'http') . '://fortawesome.github.io/Font-Awesome/" target="_blank">'
									. __( 'here to see the list', 'edubase' ) . '</a>' . __( 'and use class icons-lg, icons-md, icons-sm to change its size', 'edubase' )
				),
			 	array(
					'type' => 'dropdown',
					'heading' => __( 'Icon Style', 'edubase' ),
					'param_name' => 'iconstyle',
					"admin_label" => true,
					'value' => edubase_wpo_get_iconstyles()
				),
				array(
					"type" => "attach_image",
					"heading" => __("Photo", 'edubase'),
					"param_name" => "photo",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "textarea_html",
					"heading" => __("Information", 'edubase'),
					"param_name" => "content",
					"value" => '',
					'description'	=> __('Allow  put html tags', 'edubase' )
				),

				array(
					"type" => "textfield",
					"heading" => __("Extra class name", 'edubase'),
					"param_name" => "el_class",
					"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
				)
		   	)
		));

		/*********************************************************************************************************************
		 *  WPO Counter
		 *********************************************************************************************************************/
		vc_map( array(
		    "name" => __("WPO Counter",'edubase'),
		    "base" => "wpo_counter",
		    "class" => "",
		    "description"=> __('Counting number with your term', 'edubase'),
		    "category" => __('Opal Elements', 'edubase'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => __("Title", 'edubase'),
					"param_name" => "title",
					"value" => '',
						"admin_label" => true
				),

				array(
					"type" => "colorpicker",
					"heading" => __("Text Color", 'edubase'),
					"param_name" => "color",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textarea",
					"heading" => __("Description", 'edubase'),
					"param_name" => "description",
					"value" => '',
						"admin_label" => true
				),
				array(
					"type" => "textfield",
					"heading" => __("Number", 'edubase'),
					"param_name" => "number",
					"value" => ''
				),

			 	array(
					"type" => "textfield",
					"heading" => __("FontAwsome Icon", 'edubase'),
					"param_name" => "icon",
					"value" => 'fa-pencil radius-x',
					'description'	=> __( 'This support display icon from FontAwsome, Please click', 'edubase' )
									. '<a href="' . ( is_ssl()  ? 'https' : 'http') . '://fortawesome.github.io/Font-Awesome/" target="_blank">'
									. __( 'here to see the list', 'edubase' ) . '</a>' . __( 'and use class icons-lg, icons-md, icons-sm to change its size', 'edubase' )
				),


				array(
					"type" => "attach_image",
					"description" => __("If you upload an image, icon will not show.", 'edubase'),
					"param_name" => "image",
					"value" => '',
					'heading'	=> __('Image', 'edubase' )
				),

				array(
					"type" => "dropdown",
					"heading" => __("Style", 'edubase'),
					"param_name" => "style",
					'value' 	=> array( 
						__('Default', 'edubase') =>  '',
						__('Style 2', 'edubase') =>'style-2',
						//__('Style 1 light style', 'edubase') =>'style-1 light-style',
						__('Style 2 light style', 'edubase') =>'style-2-light' ,
					)	
				),

				array(	
					"type" => "dropdown",
					"heading" => __("Text Color", 'edubase'),
					"param_name" => "text_color",
					'value' 	=> array( __('None', 'edubase') =>  'text-default', __('Primary', 'edubase') =>'text-primary' , __('Info', 'edubase') => 'text-info',  __('Danger', 'edubase') => 'text-danger',  __('Warning', 'edubase') => 'text-warning'  ),
				),
			
				array(
					"type" => "textfield",
					"heading" => __("Extra class name", 'edubase'),
					"param_name" => "el_class",
					"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
				)
		   	)
		));

/*********************************************************************************************************************
	 *  Info Box
	 *********************************************************************************************************************/
	vc_map( array(
	    "name" => __("WPO Info Box",'edubase'),
	    "base" => "wpo_inforbox",
	    "class" => "",
	    "description"=> __( 'Show header, text in special style', 'edubase'),
	    "category" => __('Opal Elements', 'edubase'),
	    "params" => array(
	    	array(
				"type" => "textfield",
				"heading" => __("Title", 'edubase'),
				"param_name" => "title",
				"value" => '',
					"admin_label" => true
			),
			array(
				"type" => "textfield",
				"heading" => __("Sub Title", 'edubase'),
				"param_name" => "sub_title",
				"value" => '',
					"admin_label" => true
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Title Align', 'edubase' ),
				'param_name' => 'title_align',
				'value' => array(
					__( 'Align center', 'edubase' ) => 'separator_align_center',
					__( 'Align left', 'edubase' ) => 'separator_align_left',
					__( 'Align right', 'edubase' ) => "separator_align_right"
				),
				'description' => __( 'Select title align.', 'edubase' )
			), 
			array(
				'type'       => 'dropdown',
				'heading'    => __( 'Title font size', 'edubase' ),
				'param_name' => 'size',
				'value'      => array(
					__( 'Large', 'edubase' )       => 'font-size-lg',
					__( 'Medium', 'edubase' )      => 'font-size-md',
					__( 'Small', 'edubase' )       => 'font-size-sm',
					__( 'Extra small', 'edubase' ) => 'font-size-xs'
				)
			),
			array(
				'type'       => 'dropdown',
				'heading'    => __( 'Info Box Content Style', 'edubase' ),
				'param_name' => 'inforbox_style',
				'value'      => array(
				__( 'Light', 'edubase' )   => '',
				__( 'Dark', 'edubase' ) => 'inforbox-dark',
				)
			),

			array(
				'type'       => 'dropdown',
				'heading'    => __( 'Info Box Align Mode', 'edubase' ),
				'param_name' => 'inforbox_alight',
				'value'      => array(
				__( 'Left Style', 'edubase' )   => '',
				__( 'Right Style', 'edubase' ) => 'inforbox-align-right',
				)
			),
			array(
				'type'       => 'dropdown',
				'heading'    => __( 'Style display', 'edubase' ),
				'param_name' => 'style_display',
				'value'      => array(
				__( 'Default', 'edubase' )   => '',
				__( 'Style 1', 'edubase' ) => 'style-1',
				)
			),
			array(
				"type" => "textarea",
				"heading" => __("information", 'edubase'),
				"param_name" => "information",
				"value" => '',
				'description'	=> __('Allow  put html tags', 'edubase')
			),
			array(
				"type" => "attach_image",
				"heading" => __("Backgroup Image", 'edubase'),
				"param_name" => "imagebg",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "colorpicker",
				"heading" => __("Background Color", 'edubase'),
				"param_name" => "colorbg",
				"value" => '',
				'description'	=> ''
			),

			array(
				"type" => "textfield",
				"heading" => __("Extra class name", 'edubase'),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
			)
	   	)
	));

vc_map( array(
	    "name" => __("WPO Info Box 2",'edubase'),
	    "base" => "wpo_inforbox2",
	    "class" => "",
	    "description"=> __( 'Show Info In special Block', 'edubase'),
	    "category" => __('Opal Elements', 'edubase'),
	    "params" => array(
	    	array(
				"type" => "textfield",
				"heading" => __("Title", 'edubase'),
				"param_name" => "title",
				"value" => '',
					"admin_label" => true
			),
			 
			array(
				"type" => "textarea",
				"heading" => __("information", 'edubase'),
				"param_name" => "information",
				"value" => '',
				'description'	=> __('Allow  put html tags', 'edubase')
			),
			array(
				"type" => "textfield",
				"heading" => __("FontAwsome Icon", 'edubase'),
				"param_name" => "icon",
				"value" => '',
				'description'	=> __( 'This support display icon from FontAwsome, Please click', 'edubase' )
								. '<a href="' . ( is_ssl()  ? 'https' : 'http') . '://fortawesome.github.io/Font-Awesome/" target="_blank">'
								. __( 'here to see the list', 'edubase' ) . '</a>' . __( 'and use class icons-lg, icons-md, icons-sm to change its size', 'edubase' )
			),
			array(
				'type' => 'dropdown',
				'heading' => __( 'Backgroup Style', 'edubase' ),
				'param_name' => 'bgstyle',
				'prioryty' => 1,
				"description" => __( 'Set background style or customize at below', 'edubase'),
				'value' => array(
					__( 'Default', 'edubase' ) => 'bg-nostyle',
					__( 'Primary', 'edubase' ) => 'bg-primary text-white',
					__( 'Danger', 'edubase' ) => 'bg-danger text-white',
					__( 'Success', 'edubase' ) => 'bg-success text-white',
					__( 'Info', 'edubase' ) => 'bg-info text-white',
					__( 'Warning', 'edubase' ) => 'bg-warning text-white'
				)
			),
		 

			array(
				"type" => "attach_image",
				"heading" => __("Backgroup Image", 'edubase'),
				"param_name" => "imagebg",
				"value" => '',
				'description'	=> ''
			),

			array(
				"type" => "colorpicker",
				"heading" => __("Background Color", 'edubase'),
				"param_name" => "colorbg",
				"value" => '',
				'description'	=> ''
			),

			array(
				"type" => "textfield",
				"heading" => __("Extra class name", 'edubase'),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
			)
	   	)
	));
			
		 
	/*********************************************************************************************************************
	 *  Banner
	 *********************************************************************************************************************/
	vc_map( array(
	    "name" => __("WPO Banner",'edubase'),
	    "base" => "wpo_banner",
	    "class" => "",
	    "description"=> __( 'Show Banner in special style', 'edubase'),
	    "category" => __('Opal Elements', 'edubase'),
	    "params" => array(
	    	array(
				"type" => "textfield",
				"heading" => __("Title", 'edubase'),
				"param_name" => "title",
				"value" => '',
					"admin_label" => true
			),
	    	array(
				"type" => "textfield",
				"heading" => __("Sub title", 'edubase'),
				"param_name" => "subheading",
				"value" => '',
					"admin_label" => true
			),
			array(
				'type'       => 'dropdown',
				'heading'    => __( 'Content Position', 'edubase' ),
				'param_name' => 'content_position',
				'value'      => array(
				__( 'Left', 'edubase' )   => 'content_position_left',
				__( 'Center', 'edubase' ) => 'content_position_center',
				__( 'Right', 'edubase' )  => 'content_position_right'
				)
			),
		 
			array(
				"type" => "textarea",
				"heading" => __("information", 'edubase'),
				"param_name" => "information",
				"value" => '',
				'description'	=> __('Allow  put html tags', 'edubase')
			),
			array(
				"type" => "attach_image",
				"heading" => __("Backgroup Image", 'edubase'),
				"param_name" => "imagebg",
				"value" => '',
				'description'	=> ''
			),
			array(
				"type" => "textfield",
				"heading" => __("Link Button", 'edubase'),
				"param_name" => "link",
				"value" => '',
			),
			array(
				"type" => "textfield",
				"heading" => __("Text Button", 'edubase'),
				"param_name" => "link_text",
				"value" => '',
			),
			array(
				'type' => 'dropdown',
				'heading' => __( 'Button Style Color', 'edubase' ),
				'param_name' => 'color',
				'prioryty' => 1,
				'value' => array(
					__( 'Default', 'edubase' ) => 'btn-default',
					__( 'Primary', 'edubase' ) => 'btn-primary',
					__( 'Danger', 'edubase' ) => 'btn-danger',
					__( 'Success', 'edubase' ) => 'btn-success',
					__( 'Info', 'edubase' ) => 'btn-info',
					__( 'Warning', 'edubase' ) => 'btn-warning',

					__( 'Outline Primary', 'edubase' ) => 'btn-outline btn-primary',
					__( 'Outline Danger', 'edubase' ) => 'btn-outline btn-danger',
					__( 'Outline Success', 'edubase' ) => 'btn-outline btn-success',
					__( 'Outline Info', 'edubase' ) => 'btn-outline btn-info',
					__( 'Outline Warning', 'edubase' ) => 'btn-outline btn-warning',
					__( 'Outline Light', 'edubase' ) => 'btn-outline-light',
					
					__( 'Inverse Primary', 'edubase' ) => 'btn-inverse btn-primary',
					__( 'Inverse Danger', 'edubase' ) => 'btn-inverse btn-danger',
					__( 'Inverse Success', 'edubase' ) => 'btn-inverse btn-success',
					__( 'Inverse Info', 'edubase' ) => 'btn-inverse btn-info',
					__( 'Inverse  Warning', 'edubase' ) => 'btn-inverse btn-warning',
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => __( 'Button Radius Style', 'edubase' ),
				'param_name' => 'btn_style',
				'prioryty' => 1,
				'value' => array(
					__( 'None', 'edubase' ) => '',
					__( 'Radius 50%', 'edubase' ) => 'radius-x',
				 	__( 'Radius 1x', 'edubase' ) => 'radius-1x',
				 	__( 'Radius 2x', 'edubase' ) => 'radius-2x',
				 	__( 'Radius 3x', 'edubase' ) => 'radius-3x',
				 	__( 'Radius 4x', 'edubase' ) => 'radius-4x',
					__( 'Radius 5x', 'edubase' ) => 'radius-5x',
					__( 'Radius 6x', 'edubase' ) => 'radius-6x',
				
				)
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Hover Effect', 'edubase' ),
				'param_name' => 'hover_effect',
				'prioryty' => 1,
				'value' => array(
					__( 'Always Show Content', 'edubase' ) => 'no-effect',
					__( 'Light and Hidden Content', 'edubase' ) => 'light-hide-effect',
				)
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", 'edubase'),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
			)
	   )
	));