<?php 
if( EDUBASE_WPO_PUDDYPRESS_ACTIVED ){
 
		   vc_map( array(
		        "name" => __("PuddyPress Members",'edubase'),
		        "base" => "wpo_puddypress_members",
		        "class" => "",
		        "category" => __('Opal PuddyPress','edubase'),
		        "description"	=> __( 'Display Active, Newest Members with avatar', 'edubase' ),
		        "params" => array(
		            array(
		                "type" => "textfield",
		                "class" => "",
		                "heading" => __('Title', 'edubase'),
		                "param_name" => "title",
		                "admin_label" => true
		            ),
		            array(
		                "type" => "textfield",
		                "heading" => __("Max members to show", 'edubase'),
		                "param_name" => "max_members",
		                "value" => '10',
		                "description" => __("Show Maximun Members", 'edubase')
		            ),
		            array(
		                "type" => "dropdown",
		                "heading" => __("Default Show Members",'edubase'),
		                "param_name" => "member_default",
		                "value" => array(
		                	__( 'Newest', 'edubase' ) => 'newest',
		                	__( 'Aactive', 'edubase' ) => 'active',
		                	__( 'Popular', 'edubase' ) => 'popular',
		                ),
		                "admin_label" => false,
		                "description" => __("Show List of Members As expected.",'edubase')
		            ),
		            array(
		                "type" => "dropdown",
		                "heading" => __("Avatar Size",'edubase'),
		                "param_name" => "avatar_size",
		                "value" => array(
		                	__( 'Large', 'edubase' ) => 'avatar-md',
		                	__( 'Medium', 'edubase' ) => 'avatar-md',
		                	__( 'Small', 'edubase' ) => 'avatar-sm',
		                ),
		                "admin_label" => false,
		                "description" => __("Display Avatar with expected size.",'edubase')
		            ),

		             array(
		                "type" => "dropdown",
		                "heading" => __("Style Display",'edubase'),
		                "param_name" => "style",
		                "value" => array(
		                	__( 'Block', 'edubase' ) => 'block',
		                	__( 'Inline', 'edubase' ) => 'inline',
		                ),
		                "admin_label" => false,
		                "description" => __("Display Title and Avatar Block in expected Style",'edubase')
		            ),
		            array(
		                "type" => "textfield",
		                "heading" => __("Extra class name", 'edubase'),
		                "param_name" => "el_class",
		                "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
		            ),
		        )
		    ));
		 

		    ///

		     vc_map( array(
		        "name" => __("PuddyPress Group",'edubase'),
		        "base" => "wpo_puddypress_groups",
		        "class" => "",
		        "category" => __('Opal PuddyPress','edubase'),
		        "description"	=> __( 'Display Active, Newest Group with avatar', 'edubase' ),
		        "params" => array(
		            array(
		                "type" => "textfield",
		                "class" => "",
		                "heading" => __('Title', 'edubase'),
		                "param_name" => "title",
		                "admin_label" => true
		            ),
		            array(
		                "type" => "textfield",
		                "heading" => __("Max group to show", 'edubase'),
		                "param_name" => "max_group",
		                "value" => '10',
		                "description" => __("Show Maximun Group", 'edubase')
		            ),
		            array(
		                "type" => "dropdown",
		                "heading" => __("Default Show Group",'edubase'),
		                "param_name" => "group_default",
		                "value" => array(
		                	__( 'Newest', 'edubase' ) => 'newest',
		                	__( 'Aactive', 'edubase' ) => 'active',
		                	__( 'Popular', 'edubase' ) => 'popular',
		                ),
		                "admin_label" => false,
		                "description" => __("Show List of Group As expected.",'edubase')
		            ),
		           
		             array(
		                "type" => "dropdown",
		                "heading" => __("Column Display",'edubase'),
		                "param_name" => "column",
		                "value" => array(
		                	__( '2 Columns', 'edubase' ) => '2',
		                	__( '1 Column', 'edubase' ) => '1',
		                ),
		                "admin_label" => false,
		                "description" => __("Display Title and Avatar Block in expected Style",'edubase')
		            ),
		            array(
		                "type" => "textfield",
		                "heading" => __("Extra class name", 'edubase'),
		                "param_name" => "el_class",
		                "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
		            ),
		        )
		    ));


		    $output = array(); 

		    if( is_admin() ){
			    $posts = get_posts (array ('post_type' => 'bps_form', 'orderby' => 'ID', 'order' => 'ASC', 'nopaging' => true));  
			    foreach ($posts as $post) {
			    	$output[$post->post_title] = $post->ID;
			    }   
			    wp_reset_postdata();
			}


		     vc_map( array(
		        "name" => __("PuddyPress Profile Search",'edubase'),
		        "base" => "wpo_profile_search",
		        "class" => "",
		        "category" => __('Opal PuddyPress','edubase'),
		        "description"	=> __( 'Display Profiles Search Form', 'edubase' ),
		        "params" => array(
		            array(
		                "type" => "textfield",
		                "class" => "",
		                "heading" => __('Title', 'edubase'),
		                "param_name" => "title",
		                "admin_label" => true
		            ),
		            
		           
		             array(
		                "type" => "dropdown",
		                "heading" => __("Form Display",'edubase'),
		                "param_name" => "form",
		                "value" => $output,
		                "admin_label" => false,
		                "description" => __("Display Title and Avatar Block in expected Style",'edubase')
		            ),

		             array(
		                "type" => "dropdown",
		                "heading" => __("Template",'edubase'),
		                "param_name" => "template",
		                "value" => array(
		                	__( 'Couple Style', 'edubase' ) => 'pbs-couple-form',
		                	__( 'Normal style', 'edubase' ) => 'pbs-normal-form',
		                ),
		                "admin_label" => false,
		                "description" => __("Display Title and Avatar Block in expected Style",'edubase')
		            ),

		            array(
		                "type" => "textfield",
		                "heading" => __("Extra class name", 'edubase'),
		                "param_name" => "el_class",
		                "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
		            ),
		        )
		    ));
}