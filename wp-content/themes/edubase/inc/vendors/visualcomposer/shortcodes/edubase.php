<?php
if( EDUBASE_WPO_IBEDUCATOR_ACTIVED ){
   /*********************************************************************************************************************
    * EDUCATOR GRID
    *********************************************************************************************************************/
   vc_map( array(
       "name" => __("WPO Educator Grid",'edubase'),
       "base" => "wpo_educator_grid",
       'icon' => 'icon-wpb-application-icon-large',
       'description'=>'Display Educator Grid',
       "class" => "",
       "category" => __('Opal Educator', 'edubase'),
       "params" => array(
         array(
            "type" => "textfield",
            "heading" => __("Title", 'edubase'),
            "param_name" => "title",
            "value" => '',
            "admin_label" => true
         ),
         array(
            'type' => 'dropdown',
            'heading' => __( 'Title font size', 'edubase' ),
            'param_name' => 'size',
            'value' => array(
               __( 'Large', 'edubase' ) => 'font-size-lg',
               __( 'Medium', 'edubase' ) => 'font-size-md',
               __( 'Small', 'edubase' ) => 'font-size-sm',
               __( 'Extra small', 'edubase' ) => 'font-size-xs'
            )
         ),
         array(
            'type' => 'dropdown',
            'heading' => __( 'Title Alignment', 'edubase' ),
            'param_name' => 'alignment',
            'value' => array(
               __( 'Align left', 'edubase' ) => 'separator_align_left',
               __( 'Align center', 'edubase' ) => 'separator_align_center',
               __( 'Align right', 'edubase' ) => 'separator_align_right'
            )
         ),
          array(
            "type" => "dropdown",
            'heading' => __( 'Mode', 'edubase' ),
            "param_name" => "mode",
            "value" => array(
               __('Lastest Courses', 'edubase') => 'most_recent',
               __('Randown Courses', 'edubase') => 'random'
            )
          ),
         array(
            "type" => "textfield",
            'heading' => __( 'Number', 'edubase' ),
            "param_name" => "number",
            "value" => ''
          ),
          array(
            "type" => "dropdown",
            'heading' => __( 'Column', 'edubase' ),
            "param_name" => "column",
            "value" => array(
               '2' => '2',
               '3' => '3',
               '4' => '4'
            )
          ),
          array(
            "type" => "textfield",
            "heading" => __("Extra class name", 'edubase'),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
         )
         )
   ));

   /*********************************************************************************************************************
    * EDUCATOR Odds
    *********************************************************************************************************************/
   vc_map( array(
       "name" => __("WPO Educator Grid Odds",'edubase'),
       "base" => "wpo_educator_grid_odds",
       'icon' => 'icon-wpb-application-icon-large',
       'description'=>'Display Educator Grid Odds',
       "class" => "",
       "category" => __('Opal Educator', 'edubase'),
       "params" => array(
         array(
            "type" => "textfield",
            "heading" => __("Title", 'edubase'),
            "param_name" => "title",
            "value" => '',
            "admin_label" => true
         ),
         array(
            'type' => 'dropdown',
            'heading' => __( 'Title font size', 'edubase' ),
            'param_name' => 'size',
            'value' => array(
               __( 'Large', 'edubase' ) => 'font-size-lg',
               __( 'Medium', 'edubase' ) => 'font-size-md',
               __( 'Small', 'edubase' ) => 'font-size-sm',
               __( 'Extra small', 'edubase' ) => 'font-size-xs'
            )
         ),
         array(
            'type' => 'dropdown',
            'heading' => __( 'Title Alignment', 'edubase' ),
            'param_name' => 'alignment',
            'value' => array(
               __( 'Align left', 'edubase' ) => 'separator_align_left',
               __( 'Align center', 'edubase' ) => 'separator_align_center',
               __( 'Align right', 'edubase' ) => 'separator_align_right'
            )
         ),
          array(
            "type" => "dropdown",
            'heading' => __( 'Mode', 'edubase' ),
            "param_name" => "mode",
            "value" => array(
               __('Lastest Courses', 'edubase') => 'most_recent',
               __('Randown Courses', 'edubase') => 'random'
            )
          ),
         array(
            "type" => "textfield",
            'heading' => __( 'Number', 'edubase' ),
            "param_name" => "number",
            "value" => ''
          ),
          array(
            "type" => "textfield",
            "heading" => __("Extra class name", 'edubase'),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
         )
         )
   ));
/*********************************************************************************************************************
   * EDUCATOR Carousel
*********************************************************************************************************************/
   vc_map( array(
       "name" => __("WPO Educator Carousel",'edubase'),
       "base" => "wpo_educator_carousel",
       'icon' => 'icon-wpb-application-icon-large',
       'description'=>'Display Educator Grid Odds',
       "class" => "",
       "category" => __('Opal Educator', 'edubase'),
       "params" => array(
         array(
            "type" => "textfield",
            "heading" => __("Title", 'edubase'),
            "param_name" => "title",
            "value" => '',
            "admin_label" => true
         ),
         array(
            'type' => 'dropdown',
            'heading' => __( 'Title font size', 'edubase' ),
            'param_name' => 'size',
            'value' => array(
               __( 'Large', 'edubase' ) => 'font-size-lg',
               __( 'Medium', 'edubase' ) => 'font-size-md',
               __( 'Small', 'edubase' ) => 'font-size-sm',
               __( 'Extra small', 'edubase' ) => 'font-size-xs'
            )
         ),
         array(
            'type' => 'dropdown',
            'heading' => __( 'Title Alignment', 'edubase' ),
            'param_name' => 'alignment',
            'value' => array(
               __( 'Align left', 'edubase' ) => 'separator_align_left',
               __( 'Align center', 'edubase' ) => 'separator_align_center',
               __( 'Align right', 'edubase' ) => 'separator_align_right'
            )
         ),
          array(
            "type" => "dropdown",
            'heading' => __( 'Mode', 'edubase' ),
            "param_name" => "mode",
            "value" => array(
               __('Lastest Courses', 'edubase') => 'most_recent',
               __('Randown Courses', 'edubase') => 'random'
            )
          ),
         array(
            "type" => "textfield",
            'heading' => __( 'Number', 'edubase' ),
            "param_name" => "number",
            "value" => ''
          ),
         array(
            "type" => "dropdown",
            'heading' => __( 'Column', 'edubase' ),
            "param_name" => "column",
            "value" => array(
               '1' => '1',
               '2' => '2',
               '3' => '3',
               '4' => '4'
            )
          ),
          array(
            "type" => "textfield",
            "heading" => __("Extra class name", 'edubase'),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
         )
         )
   ));

/*********************************************************************************************************************
   * EDUCATOR Search form
*********************************************************************************************************************/
   vc_map( array(
       "name" => __("WPO Educator Searchform",'edubase'),
       "base" => "wpo_educator_searchform",
       'icon' => 'icon-wpb-application-icon-large',
       'description'=>'Display Educator SearchForm',
       "class" => "",
       "category" => __('Opal Educator', 'edubase'),
       "params" => array(
         array(
            "type" => "textfield",
            "heading" => __("Title", 'edubase'),
            "param_name" => "title",
            "value" => '',
            "admin_label" => true
         ),
         array(
            'type' => 'dropdown',
            'heading' => __( 'Title font size', 'edubase' ),
            'param_name' => 'size',
            'value' => array(
               __( 'Large', 'edubase' ) => 'font-size-lg',
               __( 'Medium', 'edubase' ) => 'font-size-md',
               __( 'Small', 'edubase' ) => 'font-size-sm',
               __( 'Extra small', 'edubase' ) => 'font-size-xs'
            )
         ),
         array(
            'type' => 'dropdown',
            'heading' => __( 'Title Alignment', 'edubase' ),
            'param_name' => 'alignment',
            'value' => array(
               __( 'Align left', 'edubase' ) => 'separator_align_left',
               __( 'Align center', 'edubase' ) => 'separator_align_center',
               __( 'Align right', 'edubase' ) => 'separator_align_right'
            )
         ),
          array(
            "type" => "dropdown",
            'heading' => __( 'Style', 'edubase' ),
            "param_name" => "style",
            "value" => array(
               __('Default', 'edubase') => 'default',
               __('Light Style', 'edubase') => 'light-style',
               __('Vertical Style', 'edubase') => 'vertical-style',
               __('Vertical Light Style', 'edubase') => 'vertical-style light'
            )
          ),
          array(
            "type" => "dropdown",
            'heading' => __( 'Display categories', 'edubase' ),
            "param_name" => "cat_display",
            "value" => array(
               __('Enable', 'edubase') => '1',
               __('Disable', 'edubase') => '0'
            )
          ),
          array(
            "type" => "textfield",
            "heading" => __("Extra class name", 'edubase'),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
          )
      )
   ));
}

/*********************************************************************************************************************
   * Teacher
*********************************************************************************************************************/
   $teacher_categories = array('-- Category --'=> '');
  if( is_admin() ){
    global $wpdb;
    $query    = "SELECT a.name,a.slug,a.term_id FROM $wpdb->terms a JOIN  $wpdb->term_taxonomy b ON (a.term_id= b.term_id ) where b.count>0 and b.taxonomy = 'category_teachers' and b.parent = 0";
    $categories = $wpdb->get_results($query);
    foreach ($categories as $category) {
      $teacher_categories[$category->name] = $category->slug;
    }
  } 

   vc_map( array(
       "name" => __("WPO Teacher Grid",'edubase'),
       "base" => "wpo_teacher_grid",
       'icon' => 'icon-wpb-application-icon-large',
       'description'=>'Display Teacher Grid',
       "class" => "",
       "category" => __('Opal Educator', 'edubase'),
       "params" => array(
         array(
            "type" => "textfield",
            "heading" => __("Title", 'edubase'),
            "param_name" => "title",
            "value" => '',
            "admin_label" => true
         ),
         array(
            'type' => 'dropdown',
            'heading' => __( 'Title font size', 'edubase' ),
            'param_name' => 'size',
            'value' => array(
               __( 'Large', 'edubase' ) => 'font-size-lg',
               __( 'Medium', 'edubase' ) => 'font-size-md',
               __( 'Small', 'edubase' ) => 'font-size-sm',
               __( 'Extra small', 'edubase' ) => 'font-size-xs'
            )
         ),
         array(
            'type' => 'dropdown',
            'heading' => __( 'Title Alignment', 'edubase' ),
            'param_name' => 'alignment',
            'value' => array(
               __( 'Align center', 'edubase' ) => 'separator_align_center',
               __( 'Align left', 'edubase' ) => 'separator_align_left',
               __( 'Align right', 'edubase' ) => 'separator_align_right'
            )
         ),
          array(
            "type" => "dropdown",
            'heading' => __( 'Category', 'edubase' ),
            "param_name" => "category",
            "value" => $teacher_categories
          ),
          array(
            "type" => "textfield",
            'heading' => __( 'Number', 'edubase' ),
            "param_name" => "number",
            "value" => '6'
          ),
          array(
            'type' => 'dropdown',
            'heading' => __( 'Show Information', 'edubase' ),
            'param_name' => 'showinfo',
            'value' => array(
               __( 'No', 'edubase' ) => '0',
               __( 'Yes', 'edubase' ) => '1',
   
            )
         ),
          array(
            'type' => 'dropdown',
            'heading' => __( 'Display Style', 'edubase' ),
            'param_name' => 'layout',
             "admin_label" => true,
            'value' => array(
               __( 'Normal', 'edubase' ) => 'normal',
               __( 'Horizontal', 'edubase' ) => 'horizontal',
               __('Horizontal V2', 'edubase') => 'horizontal-v2',
               __('Carousel', 'edubase') => 'carousel'
   
            )
         ),
         array(
            "type" => "dropdown",
            'heading' => __( 'Column', 'edubase' ),
            "param_name" => "column",
            "value" => array(
               '1' => '1',
               '2' => '2',
               '3' => '3',
               '4' => '4',
               '6' => '6'
            )
          ),
          array(
            "type" => "textfield",
            "heading" => __("Extra class name", 'edubase'),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
         )
        )
   ));

//-- Category Courses Filter --


vc_map( array(
       "name" => __("WPO Category Courses Filter",'edubase'),
       "base" => "wpo_category_courses_filter",
       'icon' => 'icon-wpb-application-icon-large',
       'description'=>'Display WPO Category Courses Filter',
       "class" => "",
       "category" => __('Opal Educator', 'edubase'),
       "params" => array(
         array(
            "type" => "textfield",
            "heading" => __("Title", 'edubase'),
            "param_name" => "title",
            "value" => '',
            "admin_label" => true
         ),
         array(
            'type' => 'dropdown',
            'heading' => __( 'Title font size', 'edubase' ),
            'param_name' => 'size',
            'value' => array(
               __( 'Large', 'edubase' ) => 'font-size-lg',
               __( 'Medium', 'edubase' ) => 'font-size-md',
               __( 'Small', 'edubase' ) => 'font-size-sm',
               __( 'Extra small', 'edubase' ) => 'font-size-xs'
            )
         ),
         array(
            'type' => 'dropdown',
            'heading' => __( 'Title Alignment', 'edubase' ),
            'param_name' => 'alignment',
            'value' => array(
               __( 'Align center', 'edubase' ) => 'separator_align_center',
               __( 'Align left', 'edubase' ) => 'separator_align_left',
               __( 'Align right', 'edubase' ) => 'separator_align_right'
            )
         ),
          array(
            "type" => "dropdown",
            'heading' => __( 'Category', 'edubase' ),
            "param_name" => "category",
            "value" => $teacher_categories
          ),

          array(
            "type" => "textfield",
            'heading' => __( 'Number', 'edubase' ),
            "param_name" => "number",
            "value" => '6'
          ),
         array(
            "type" => "dropdown",
            'heading' => __( 'Column', 'edubase' ),
            "param_name" => "column",
            "value" => array(
               '2' => '2',
               '3' => '3',
               '4' => '4',
               '6' => '6'
            )
          ),
          array(
            "type" => "textfield",
            "heading" => __("Extra class name", 'edubase'),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
         )
        )
   ));