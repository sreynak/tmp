<?php 
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */

/**
 * Call To Action
 */	
 
	 
	vc_update_shortcode_param( 'vc_cta_button2', array(
		'type' => 'dropdown',
		'heading' => __( 'Button Style Color', 'edubase' ),
		'param_name' => 'color',
		'prioryty' => 1,
		'value' => array(
			__( 'Default', 'edubase' ) => 'btn-default',
			__( 'Primary', 'edubase' ) => 'btn-primary',
			__( 'Danger', 'edubase' ) => 'btn-danger',
			__( 'Success', 'edubase' ) => 'btn-success',
			__( 'Info', 'edubase' ) => 'btn-info',
			__( 'Warning', 'edubase' ) => 'btn-warning',

			__( 'Outline Primary', 'edubase' ) => 'btn-outline btn-primary',
			__( 'Outline Danger', 'edubase' ) => 'btn-outline btn-danger',
			__( 'Outline Success', 'edubase' ) => 'btn-outline btn-success',
			__( 'Outline Info', 'edubase' ) => 'btn-outline btn-info',
			__( 'Outline Warning', 'edubase' ) => 'btn-outline btn-warning',

			__( 'Inverse Primary', 'edubase' ) => 'btn-inverse btn-primary',
			__( 'Inverse Danger', 'edubase' ) => 'btn-inverse btn-danger',
			__( 'Inverse Success', 'edubase' ) => 'btn-inverse btn-success',
			__( 'Inverse Info', 'edubase' ) => 'btn-inverse btn-info',
			__( 'Inverse  Warning', 'edubase' ) => 'btn-inverse btn-warning',
		)
	));

	vc_update_shortcode_param('vc_cta_button2', array(
	   'type' => 'dropdown',
	   'heading' => __( 'Heading Style', 'edubase' ),
	   'param_name' => 'heading_style',
	   'prioryty' => 2,
	   'value' => array(
	   	__('Default', 'edubase')	=> 'default',
	      __( 'Version 1', 'edubase' ) => 'v1',
	      __( 'Version 2', 'edubase' ) => 'v2',
	      __( 'Version 3', 'edubase' ) => 'v3',
			__( 'Version 4', 'edubase' ) => 'v4',
			__( 'Version 5', 'edubase' ) => 'v5',
			__( 'Version 6', 'edubase' ) => 'v6',
			__( 'Version 7', 'edubase' ) => 'v7',
			__( 'Version 8', 'edubase' ) => 'v8',
			__( 'Version 9', 'edubase' ) => 'v9',
			__( 'Version 10', 'edubase' ) => 'v10',
			__( 'Version 11', 'edubase' ) => 'v11',
			__( 'Version 12', 'edubase' ) => 'v12',
			__( 'Version 13', 'edubase' ) => 'v13',
			__( 'Version 14', 'edubase' ) => 'v14',
			__( 'Version 15', 'edubase' ) => 'v15',
	    )
	));

	vc_update_shortcode_param( 'vc_cta_button2', array(
		'type' => 'dropdown',
		'heading' => __( 'CTA Style', 'edubase' ),
		'param_name' => 'style',
		'prioryty' => 1,
		'value' => array(
			__( 'Version 1', 'edubase' ) => '1',
			__( 'Version 2', 'edubase' ) => '2',
			__( 'Version 3', 'edubase' ) => '4',
			__( 'Version Light Style 1', 'edubase' ) => '1 light-style ',
			__( 'Version Light Style 2', 'edubase' ) => '3 light-style ',
			__( 'Version Light Style 4', 'edubase' ) => '4 light-style '
		
		)
	));

	vc_update_shortcode_param( 'vc_cta_button2', array(
		'type' => 'dropdown',
		'heading' => __( 'Button Radius Style', 'edubase' ),
		'param_name' => 'btn_style',
		'prioryty' => 1,
		'value' => array(
			__( 'None', 'edubase' ) => '',
			__( 'Radius 50%', 'edubase' ) => '',
		 	__( 'Radius 1x', 'edubase' ) => 'radius-1x',
		 	__( 'Radius 2x', 'edubase' ) => 'radius-2x',
		 	__( 'Radius 3x', 'edubase' ) => 'radius-3x',
		 	__( 'Radius 4x', 'edubase' ) => 'radius-4x',
			__( 'Radius 5x', 'edubase' ) => 'radius-5x',
			__( 'Radius 6x', 'edubase' ) => 'radius-6x',
		
		)
	));

	//Accordions
	vc_update_shortcode_param( 'vc_accordion', array(
		'type' => 'dropdown',
		'heading' => __( 'Style', 'edubase' ),
		'param_name' => 'style',
		'prioryty' => 1,
		'value' => array(
			__( 'Style 1', 'edubase' ) => 'style-1',
			__( 'Style 2', 'edubase' ) => 'style-2',
		)
	));

	//Accordions
	vc_update_shortcode_param( 'vc_toggle', array(
		'type' => 'dropdown',
		'heading' => __( 'Skin', 'edubase' ),
		'param_name' => 'skin',
		'prioryty' => 1,
		'value' => array(
			__( 'Skin 1', 'edubase' ) => '',
			__( 'Style 2', 'edubase' ) => 'style-2',
		)
	));

	//Tabs
	vc_update_shortcode_param( 'vc_tabs', array(
		'type' => 'dropdown',
		'heading' => __( 'Style', 'edubase' ),
		'param_name' => 'style',
		'prioryty' => 1,
		'value' => array(
			__( 'Default', 'edubase' ) => '',
			__( 'Style 1', 'edubase' ) => 'style-1',
			__( 'Style 2', 'edubase' ) => 'style-2',
			__( 'Style 3', 'edubase' ) => 'style-3',
			__( 'Style 4', 'edubase' ) => 'style-4',
			__( 'Style 5', 'edubase' ) => 'style-5'
		)
	));

	//Tab
	vc_update_shortcode_param( 'vc_tab', array(
		'type' => 'textfield',
		'heading' => __( 'Icon', 'edubase' ),
		'param_name' => 'icon',
		'prioryty' => 1,
		'value' => ''
	));		

	vc_update_shortcode_param( 'vc_tab', array(
		'type' => 'textarea',
		'heading' => __( 'Description', 'edubase' ),
		'param_name' => 'description',
		'prioryty' => 1,
		'value' => ''
	));

	//Add animation column

	$cssAnimation = array(
		__("No", "js_composer") => '',
		__("Top to bottom", "js_composer") => "top-to-bottom",
		__("Bottom to top", "js_composer") => "bottom-to-top",
		__("Left to right", "js_composer") => "left-to-right",
		__("Right to left", "js_composer") => "right-to-left",
		__("Appear from center", "js_composer") => "appear",
		'bounce' => 'bounce',
		'flash' => 'flash',
		'pulse' => 'pulse',
		'rubberBand' => 'rubberBand',
		'shake' => 'shake',
		'swing' => 'swing',
		'tada' => 'tada',
		'wobble' => 'wobble',
		'bounceIn' => 'bounceIn',
		'fadeIn' => 'fadeIn',
		'fadeInDown' => 'fadeInDown',
		'fadeInDownBig' => 'fadeInDownBig',
		'fadeInLeft' => 'fadeInLeft',
		'fadeInLeftBig' => 'fadeInLeftBig',
		'fadeInRight' => 'fadeInRight',
		'fadeInRightBig' => 'fadeInRightBig',
		'fadeInUp' => 'fadeInUp',
		'fadeInUpBig' => 'fadeInUpBig',
		'flip' => 'flip',
		'flipInX' => 'flipInX',
		'flipInY' => 'flipInY',
		'lightSpeedIn' => 'lightSpeedIn',
		'rotateInrotateIn' => 'rotateIn',
		'rotateInDownLeft' => 'rotateInDownLeft',
		'rotateInDownRight' => 'rotateInDownRight',
		'rotateInUpLeft' => 'rotateInUpLeft',
		'rotateInUpRight' => 'rotateInUpRight',
		'slideInDown' => 'slideInDown',
		'slideInLeft' => 'slideInLeft',
		'slideInRight' => 'slideInRight',
		'rollIn' => 'rollIn'
	);
	$add_css_animation = array(
		"type" => "dropdown",
		"heading" => __("CSS Animation", 'edubase'),
		"param_name" => "css_animation",
		"admin_label" => true,
		"value" => $cssAnimation,
		"description" => __("Select animation type if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.", 'edubase')
	);
	vc_add_param('vc_column',$add_css_animation);
	vc_add_param('vc_column_inner',$add_css_animation);
			

	vc_update_shortcode_param( 'vc_gmaps', array(
			'type' => 'dropdown',
			'heading' => __( 'Display Style', 'edubase' ),
			'param_name' => 'display_style',
			 'value' => array(
			 		'Default' => '',
			 		'Popup' => 'popup'
			)
	));
	vc_update_shortcode_param( 'vc_gmaps', array(
			'type' => 'attach_image',
			'heading' => __( 'Image (use display style popup)', 'edubase' ),
			'param_name' => 'image',
			 'value' => array(
			 		'Default' => '',
			 		'Popup' => 'popup'
			) 		
	));