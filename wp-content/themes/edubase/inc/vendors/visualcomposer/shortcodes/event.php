<?php
if( EDUBASE_WPO_EVENT_ACTIVED ){
		/*********************************************************************************************************************
		 * Event Frontend
		 *********************************************************************************************************************/
		vc_map( array(
		    "name" => __("WPO Event Frontend (Grid)",'edubase'),
		    "base" => "wpo_event_frontend",
		    'icon' => 'icon-wpb-application-icon-large',
		    'description'=>'Display Event Frontend (Grid)',
		    "class" => "",
		    "category" => __('Opal Event', 'edubase'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => __("Title", 'edubase'),
					"param_name" => "title",
					"value" => '',
					"admin_label" => true
				),
		    	array(
					'type' => 'dropdown',
					'heading' => __( 'Title font size', 'edubase' ),
					'param_name' => 'size',
					'value' => array(
						__( 'Large', 'edubase' ) => 'font-size-lg',
						__( 'Medium', 'edubase' ) => 'font-size-md',
						__( 'Small', 'edubase' ) => 'font-size-sm',
						__( 'Extra small', 'edubase' ) => 'font-size-xs'
					)
				),
				array(
					'type' => 'dropdown',
					'heading' => __( 'Title Alignment', 'edubase' ),
					'param_name' => 'alignment',
					'value' => array(
						__( 'Align left', 'edubase' ) => 'separator_align_left',
						__( 'Align center', 'edubase' ) => 'separator_align_center',
						__( 'Align right', 'edubase' ) => 'separator_align_right'
					)
				),
				array(
					"type" => "dropdown",
					'heading' => __( 'Style display', 'edubase' ),
					"param_name" => "style",
					"value" => array(
						__('Style 1', 'edubase') => 'style-1',
						__('Style 2', 'edubase') => 'style-2'
					)
			    ),
				 array(
					"type" => "dropdown",
					'heading' => __( 'Mode', 'edubase' ),
					"param_name" => "mode",
					"value" => array(
						__('Featured Events', 'edubase') => 'featured',
						__('Lastest Events', 'edubase') => 'most_recent',
						__('Randown Events', 'edubase') => 'random'
					)
			    ),
				array(
					"type" => "textfield",
					'heading' => __( 'Number', 'edubase' ),
					"param_name" => "number",
					"value" => ''
			    ),
			    array(
					"type" => "dropdown",
					'heading' => __( 'Column', 'edubase' ),
					"param_name" => "columns",
					"value" => array(
						'2' => '2',
						'3' => '3',
						'4' => '4'
					)
			    ),
			    array(
					"type" => "textfield",
					"heading" => __("Extra class name", 'edubase'),
					"param_name" => "el_class",
					"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
				)
		   	)
		));


		/*********************************************************************************************************************
		 * Event List
		 *********************************************************************************************************************/
		vc_map( array(
		    "name" => __("WPO Event List",'edubase'),
		    "base" => "wpo_event_list",
		    'icon' => 'icon-wpb-application-icon-large',
		    'description'=>'Display Event List',
		    "class" => "",
		    "category" => __('Opal Event', 'edubase'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => __("Title", 'edubase'),
					"param_name" => "title",
					"value" => '',
					"admin_label" => true
				),
		    	array(
					'type' => 'dropdown',
					'heading' => __( 'Title font size', 'edubase' ),
					'param_name' => 'size',
					'value' => array(
						__( 'Large', 'edubase' ) => 'font-size-lg',
						__( 'Medium', 'edubase' ) => 'font-size-md',
						__( 'Small', 'edubase' ) => 'font-size-sm',
						__( 'Extra small', 'edubase' ) => 'font-size-xs'
					)
				),
				array(
					'type' => 'dropdown',
					'heading' => __( 'Title Alignment', 'edubase' ),
					'param_name' => 'alignment',
					'value' => array(
						__( 'Align left', 'edubase' ) => 'separator_align_left',
						__( 'Align center', 'edubase' ) => 'separator_align_center',
						__( 'Align right', 'edubase' ) => 'separator_align_right'
					)
				),
				 array(
					"type" => "dropdown",
					'heading' => __( 'Mode', 'edubase' ),
					"param_name" => "mode",
					"value" => array(
						__('Featured Events', 'edubase') => 'featured',
						__('Lastest Events', 'edubase') => 'most_recent',
						__('Randown Events', 'edubase') => 'random'
					)
			    ),
				 array(
				 	"type" => "dropdown",
					'heading' => __( 'Order by', 'edubase' ),
					"param_name" => "order",
					"value" => array(
						__('Date', 'edubase') => 'date',
						__('Name', 'edubase') => 'name',
					)
				),
				 array(
				 	"type" => "dropdown",
					'heading' => __( 'Order', 'edubase' ),
					"param_name" => "order",
					"value" => array(
						__('ESC', 'edubase') => 'ESC',
						__('DESC', 'edubase') => 'DESC'
					)
				),
				array(
					"type" => "textfield",
					'heading' => __( 'Number', 'edubase' ),
					"param_name" => "number",
					"value" => ''
			    ),
			    array(
					"type" => "dropdown",
					'heading' => __( 'Style Display', 'edubase' ),
					"param_name" => "style",
					"value" => array(
						__('Style Default', 'edubase') => 'style-default'
					)
			    ),
			    array(
					"type" => "textfield",
					"heading" => __("Extra class name", 'edubase'),
					"param_name" => "el_class",
					"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
				)
		   	)
		));

		/*********************************************************************************************************************
		 * Event List Accordion
		 *********************************************************************************************************************/
		vc_map( array(
		    "name" => __("WPO Event Accordion",'edubase'),
		    "base" => "wpo_event_accordion",
		    'icon' => 'icon-wpb-application-icon-large',
		    'description'=>'Display Event Accordion',
		    "class" => "",
		    "category" => __('Opal Event', 'edubase'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => __("Title", 'edubase'),
					"param_name" => "title",
					"value" => '',
					"admin_label" => true
				),
		    	array(
					'type' => 'dropdown',
					'heading' => __( 'Title font size', 'edubase' ),
					'param_name' => 'size',
					'value' => array(
						__( 'Large', 'edubase' ) => 'font-size-lg',
						__( 'Medium', 'edubase' ) => 'font-size-md',
						__( 'Small', 'edubase' ) => 'font-size-sm',
						__( 'Extra small', 'edubase' ) => 'font-size-xs'
					)
				),
				array(
					'type' => 'dropdown',
					'heading' => __( 'Title Alignment', 'edubase' ),
					'param_name' => 'alignment',
					'value' => array(
						__( 'Align left', 'edubase' ) => 'separator_align_left',
						__( 'Align center', 'edubase' ) => 'separator_align_center',
						__( 'Align right', 'edubase' ) => 'separator_align_right'
					)
				),
				 array(
					"type" => "dropdown",
					'heading' => __( 'Mode', 'edubase' ),
					"param_name" => "mode",
					"value" => array(
						__('Featured Events', 'edubase') => 'featured',
						__('Lastest Events', 'edubase') => 'most_recent',
						__('Randown Events', 'edubase') => 'random'
					)
			    ),
				 array(
				 	"type" => "dropdown",
					'heading' => __( 'Order by', 'edubase' ),
					"param_name" => "order",
					"value" => array(
						__('Date', 'edubase') => 'date',
						__('Name', 'edubase') => 'name',
					)
				),
				 array(
				 	"type" => "dropdown",
					'heading' => __( 'Order', 'edubase' ),
					"param_name" => "order",
					"value" => array(
						__('ESC', 'edubase') => 'ESC',
						__('DESC', 'edubase') => 'DESC'
					)
				),
				array(
					"type" => "textfield",
					'heading' => __( 'Number', 'edubase' ),
					"param_name" => "number",
					"value" => ''
			    ),
			    array(
					"type" => "textfield",
					"heading" => __("Extra class name", 'edubase'),
					"param_name" => "el_class",
					"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
				)
		   	)
		));

		/*********************************************************************************************************************
		 * Event Slide
		 *********************************************************************************************************************/
		vc_map( array(
		    "name" => __("WPO Event Slide",'edubase'),
		    "base" => "wpo_event_slide",
		    'icon' => 'icon-wpb-application-icon-large',
		    'description'=>'Display event slide',
		    "class" => "",
		    "category" => __('Opal Event', 'edubase'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => __("Title", 'edubase'),
					"param_name" => "title",
					"value" => '',
					"admin_label" => true
				),
		    	array(
					'type' => 'dropdown',
					'heading' => __( 'Title font size', 'edubase' ),
					'param_name' => 'size',
					'value' => array(
						__( 'Large', 'edubase' ) => 'font-size-lg',
						__( 'Medium', 'edubase' ) => 'font-size-md',
						__( 'Small', 'edubase' ) => 'font-size-sm',
						__( 'Extra small', 'edubase' ) => 'font-size-xs'
					)
				),
				array(
					'type' => 'dropdown',
					'heading' => __( 'Title Alignment', 'edubase' ),
					'param_name' => 'alignment',
					'value' => array(
						__( 'Align left', 'edubase' ) => 'separator_align_left',
						__( 'Align center', 'edubase' ) => 'separator_align_center',
						__( 'Align right', 'edubase' ) => 'separator_align_right'
					)
				),
				 array(
					"type" => "dropdown",
					'heading' => __( 'Mode', 'edubase' ),
					"param_name" => "mode",
					"value" => array(
						__('Featured Events', 'edubase') => 'featured',
						__('Lastest Events', 'edubase') => 'most_recent',
						__('Randown Events', 'edubase') => 'random'
					)
			    ),				
				array(
					"type" => "textfield",
					'heading' => __( 'Number', 'edubase' ),
					"param_name" => "number",
					"value" => ''
			    ),
			    array(
					"type" => "textfield",
					"heading" => __("Extra class name", 'edubase'),
					"param_name" => "el_class",
					"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
				)
		   )
		));

		/********************************************************************************************************************
		 * Event Frontend
		*********************************************************************************************************************/
		require_once(ABSPATH . 'wp-admin/includes/screen.php');

		
		$posts = array();
		if( is_admin() ){
			$query = get_posts( array('post_type'=> 'tribe_events', 'orderby' => 'id', 'posts_per_page' => -1 ));
			foreach ( $query as $post ) {
				if($post->ID){
			   		$posts[$post->post_title] = $post->ID;
			   }
			}
			wp_reset_postdata();
		}
		vc_map( array(
		    "name" => __("WPO Event Countdown",'edubase'),
		    "base" => "wpo_event_countdown",
		    'icon' => 'icon-wpb-application-icon-large',
		    'description'=>'Display Event Single',
		    "class" => "",
		    "category" => __('Opal Event', 'edubase'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => __("Title", 'edubase'),
					"param_name" => "title",
					"value" => '',
					"admin_label" => true
				),
		    	array(
					'type' => 'dropdown',
					'heading' => __( 'Title font size', 'edubase' ),
					'param_name' => 'size',
					'value' => array(
						__( 'Large', 'edubase' ) => 'font-size-lg',
						__( 'Medium', 'edubase' ) => 'font-size-md',
						__( 'Small', 'edubase' ) => 'font-size-sm',
						__( 'Extra small', 'edubase' ) => 'font-size-xs'
					)
				),
				array(
					'type' => 'dropdown',
					'heading' => __( 'Title Alignment', 'edubase' ),
					'param_name' => 'alignment',
					'value' => array(
						__( 'Align left', 'edubase' ) => 'separator_align_left',
						__( 'Align center', 'edubase' ) => 'separator_align_center',
						__( 'Align right', 'edubase' ) => 'separator_align_right'
					)
				),
				 array(
					"type" => "dropdown",
					'heading' => __( 'Event Single', 'edubase' ),
					"param_name" => "event_id",
					"value" => $posts
			    ),
				
			    array(
					"type" => "dropdown",
					'heading' => __( 'Layout', 'edubase' ),
					"param_name" => "layout",
					"value" => array(
						'Layout 1' => 'layout-1',
					)
			    ), 
			    array(
					"type" => "textfield",
					"heading" => __("Extra class name", 'edubase'),
					"param_name" => "el_class",
					"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
				)
		   	)
		));

		/*********************************************************************************************************************
		 * Event of day
		 *********************************************************************************************************************/
		vc_map( array(
		    "name" => __("WPO Event Of Date",'edubase'),
		    "base" => "wpo_event_of_date",
		    'icon' => 'icon-wpb-application-icon-large',
		    'description'=>'Display Event Of Date',
		    'admin_enqueue_js'	=> array(
		    		get_template_directory_uri(). '/inc/assets/js/event_of_date.backend.js'
		    	),
		    'admin_enqueue_style'	=> array(
		    		get_template_directory_uri(). '/inc/assets/css/colorpicker.css'
		    	),
		    "class" => "",
		    "category" => __('Opal Event', 'edubase'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => __("Title", 'edubase'),
					"param_name" => "title",
					"value" => '',
					"admin_label" => true
				),
		    	array(
					'type' => 'dropdown',
					'heading' => __( 'Title font size', 'edubase' ),
					'param_name' => 'size', 
					'value' => array(
						__( 'Large', 'edubase' ) => 'font-size-lg',
						__( 'Medium', 'edubase' ) => 'font-size-md',
						__( 'Small', 'edubase' ) => 'font-size-sm',
						__( 'Extra small', 'edubase' ) => 'font-size-xs'
					)
				),
				array(
					'type' => 'dropdown',
					'heading' => __( 'Title Alignment', 'edubase' ),
					'param_name' => 'alignment',
					'value' => array(
						__( 'Align left', 'edubase' ) => 'separator_align_left',
						__( 'Align center', 'edubase' ) => 'separator_align_center',
						__( 'Align right', 'edubase' ) => 'separator_align_right'
					)
				),
				array(
					"type" => "textfield",
					'heading' => __( 'Date', 'edubase' ),
					"param_name" => "event_date",
					"value" => ''
			   ),
				array(
					"type" => "dropdown_multiple",
					'heading' => __('Events', 'edubase'),
					'param_name' => "event_ids",
					'value' => array()
				), 
			   array(
					"type" => "textfield",
					"heading" => __("Extra class name", 'edubase'),
					"param_name" => "el_class",
					"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
				),
				)
		));


		vc_add_shortcode_param( 'dropdown_multiple', 'edubase_dropdown_multi_settings_field' );
		function edubase_dropdown_multi_settings_field( $settings, $value ) {
			$output = '';
			$css_option = vc_get_dropdown_option( $settings, $value );
			$output .= '<select multiple name="'
			           . $settings['param_name']
			           . '" class="wpb_vc_param_value wpb-input wpb-select '
			           . $settings['param_name']
			           . ' ' . $settings['type']
			           . ' ' . $css_option
			           . '" data-option="' . $css_option . '" data-value="'.$value.'">';
			if ( is_array( $value ) ) {
				$value = isset( $value['value'] ) ? $value['value'] : array_shift( $value );
			}
			foreach ( $settings['value'] as $index => $data ) {
				if ( is_numeric( $index ) && ( is_string( $data ) || is_numeric( $data ) ) ) {
					$option_label = $data;
					$option_value = $data;
				} elseif ( is_numeric( $index ) && is_array( $data ) ) {
					$option_label = isset( $data['label'] ) ? $data['label'] : array_pop( $data );
					$option_value = isset( $data['value'] ) ? $data['value'] : array_pop( $data );
				} else {
					$option_value = $data;
					$option_label = $index;
				}
				$option_label = $option_label;
				$selected = '';
				if ( $value !== '' && (string) $option_value === (string) $value ) {
					$selected = ' selected="selected"';
				}
				$output .= '<option class="' . $option_value . '" value="' . $option_value . '"' . $selected . '>'
				           . htmlspecialchars( $option_label ) . '</option>';
			}
			$output .= '</select>';

			return $output;
		}

		add_action('wp_ajax_load_event_of_date', '_backend_load_event_of_date');
		function _backend_load_event_of_date(){
			if ( isset( $_POST['event_date'] ) && $_POST['event_date'] ) {
				TribeEventsQuery::init();
				$states[] = 'publish';
				if ( 0 < get_current_user_id() ) {
					$states[] = 'private';
				} 
				$args = array(
					'post_status'  => $states,
					'eventDate'    => $_POST["event_date"],
					'eventDisplay' => 'day'
				);

				TribeEvents::instance()->displaying = 'day';
				$query = TribeEventsQuery::getEvents( $args, true );
				global $wp_query, $post;
				$wp_query = $query;
				add_filter( 'tribe_is_day', '__return_true' );
				$html='';

				 if ( $query->have_posts() ) :
					while ( $query->have_posts() ) : $query->the_post();
						$html .= '<option value="' . get_the_ID() . '"> ' . get_the_title() . '</option>';
					endwhile;
					wp_reset_postdata();
				endif;

		$response = array(
			'html'        => $html,
			'success'     => true,
			'total_count' => $query->found_posts,
			'view'        => 'day',
		);
		header( 'Content-type: application/json' );
		echo json_encode( $response );
		die();
	}
}

	/*********************************************************************************************************************
		* Event Timeline
	*********************************************************************************************************************/
	vc_map( array(
	    "name" => __("WPO Event Timeline",'edubase'),
	    "base" => "wpo_event_timeline",
	    'icon' => 'icon-wpb-application-icon-large',
	    'description'=>'Display Event Timeline',
	    "class" => "",
	    "category" => __('Opal Event', 'edubase'),
	    "params" => array(
	    	array(
				"type" => "textfield",
				"heading" => __("Title", 'edubase'),
				"param_name" => "title",
				"value" => '',
				"admin_label" => true
			),
	    	array(
				'type' => 'dropdown',
				'heading' => __( 'Title font size', 'edubase' ),
				'param_name' => 'size', 
				'value' => array(
					__( 'Large', 'edubase' ) => 'font-size-lg',
					__( 'Medium', 'edubase' ) => 'font-size-md',
					__( 'Small', 'edubase' ) => 'font-size-sm',
					__( 'Extra small', 'edubase' ) => 'font-size-xs'
				)
			),
			array(
				'type' => 'dropdown',
				'heading' => __( 'Title Alignment', 'edubase' ),
				'param_name' => 'alignment',
				'value' => array(
					__( 'Align left', 'edubase' ) => 'separator_align_left',
					__( 'Align center', 'edubase' ) => 'separator_align_center',
					__( 'Align right', 'edubase' ) => 'separator_align_right'
				)
			),
			array(
				"type" => "dropdown",
				'heading' => __( 'Mode', 'edubase' ),
				"param_name" => "mode",
				"value" => array(
					__('Featured Events', 'edubase') => 'featured',
					__('Lastest Events', 'edubase') => 'most_recent',
					__('Randown Events', 'edubase') => 'random'
				)
		    ),
			array(
				"type" => "textfield",
				'heading' => __( 'Number', 'edubase' ),
				"param_name" => "number",
				"value" => ''
		    ),
		   array(
				"type" => "textfield",
				"heading" => __("Extra class name", 'edubase'),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
			),
			)
	));

}