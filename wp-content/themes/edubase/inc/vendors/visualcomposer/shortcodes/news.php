<?php  
	$newssupported = true; 

if( $newssupported ) {
	/**********************************************************************************
	 * Front Page Posts
	 **********************************************************************************/


	/// Front Page 1
	vc_map( array(
		'name' => __( '(News) FrontPage 1', 'edubase' ),
		'base' => 'wpo_frontpageposts',
		'icon' => 'icon-wpb-news-1',
		"category" => __('Opal News', 'edubase'),
		'description' => __( 'Create Post having blog styles', 'edubase' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Widget title', 'js_composer' ),
				'param_name' => 'title',
				'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => __( 'Title Alignment', 'edubase' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				__( 'Align left', 'edubase' )   => 'separator_align_left',
				__( 'Align center', 'edubase' ) => 'separator_align_center',
				__( 'Align right', 'edubase' )  => 'separator_align_right'
				)
			),

			 
			array(
				'type' => 'loop',
				'heading' => __( 'Grids content', 'js_composer' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => __( 'Create WordPress loop, to populate content from your site.', 'js_composer' )
			),
			array(
				"type" => "dropdown",
				"heading" => __("Number Main Posts", 'js_composer'),
				"param_name" => "num_mainpost",
				"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
				"std" => 1
			),

			array(
				'type' => 'textfield',
				'heading' => __( 'Thumbnail size', 'js_composer' ),
				'param_name' => 'grid_thumb_size',
				'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'js_composer' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
			)
		)
	) );
	// front page 2
	vc_map( array(
		'name' => __( '(News) FrontPage 2', 'edubase' ),
		'base' => 'wpo_frontpageposts2',
		'icon' => 'icon-wpb-news-8',
		"category" => __('Opal News', 'edubase'),
		'description' => __( 'Create Post having blog styles', 'edubase' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Widget title', 'js_composer' ),
				'param_name' => 'title',
				'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => __( 'Title Alignment', 'edubase' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				__( 'Align left', 'edubase' )   => 'separator_align_left',
				__( 'Align center', 'edubase' ) => 'separator_align_center',
				__( 'Align right', 'edubase' )  => 'separator_align_right'
				)
			),

			 
			array(
				'type' => 'loop',
				'heading' => __( 'Grids content', 'js_composer' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => __( 'Create WordPress loop, to populate content from your site.', 'js_composer' )
			),
			array(
				"type" => "dropdown",
				"heading" => __("Number Main Posts", 'js_composer'),
				"param_name" => "num_mainpost",
				"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
				"std" => 1
			),

			array(
				'type' => 'textfield',
				'heading' => __( 'Thumbnail size', 'js_composer' ),
				'param_name' => 'grid_thumb_size',
				'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'js_composer' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
			)
		)
	) );
	// front page 3
	vc_map( array(
		'name' => __( '(News) FrontPage 3', 'edubase' ),
		'base' => 'wpo_frontpageposts3',
		'icon' => 'icon-wpb-news-3',
		"category" => __('Opal News', 'edubase'),
		'description' => __( 'Create Post having blog styles', 'edubase' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Widget title', 'js_composer' ),
				'param_name' => 'title',
				'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => __( 'Title Alignment', 'edubase' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				__( 'Align left', 'edubase' )   => 'separator_align_left',
				__( 'Align center', 'edubase' ) => 'separator_align_center',
				__( 'Align right', 'edubase' )  => 'separator_align_right'
				)
			),

		 

			array(
				'type' => 'loop',
				'heading' => __( 'Grids content', 'js_composer' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => __( 'Create WordPress loop, to populate content from your site.', 'js_composer' )
			),
			array(
				"type" => "dropdown",
				"heading" => __("Number Main Posts", 'js_composer'),
				"param_name" => "num_mainpost",
				"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
				"std" => 1
			),

			array(
				'type' => 'textfield',
				'heading' => __( 'Thumbnail size', 'js_composer' ),
				'param_name' => 'grid_thumb_size',
				'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'js_composer' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
			)
		)
	) );
	// front page 2
	vc_map( array(
		'name' => __( '(News) FrontPage 4', 'edubase' ),
		'base' => 'wpo_frontpageposts4',
		'icon' => 'icon-wpb-news-4',
		"category" => __('Opal News', 'edubase'),
		'description' => __( 'Create Post having blog styles', 'edubase' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Widget title', 'js_composer' ),
				'param_name' => 'title',
				'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => __( 'Title Alignment', 'edubase' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				__( 'Align left', 'edubase' )   => 'separator_align_left',
				__( 'Align center', 'edubase' ) => 'separator_align_center',
				__( 'Align right', 'edubase' )  => 'separator_align_right'
				)
			),

		 

			array(
				'type' => 'loop',
				'heading' => __( 'Grids content', 'js_composer' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => __( 'Create WordPress loop, to populate content from your site.', 'js_composer' )
			),
			 

			array(
				'type' => 'textfield',
				'heading' => __( 'Thumbnail size', 'js_composer' ),
				'param_name' => 'grid_thumb_size',
				'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'js_composer' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
			)
		)
	) );
	
		// front page 12
	vc_map( array(
		'name' => __( '(News) FrontPage 12', 'edubase' ),
		'base' => 'wpo_frontpageposts12',
		'icon' => 'icon-wpb-news-12',
		"category" => __('Opal News', 'edubase'),
		'description' => __( 'Create Post having blog styles', 'edubase' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Widget title', 'js_composer' ),
				'param_name' => 'title',
				'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => __( 'Title Alignment', 'edubase' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				__( 'Align left', 'edubase' )   => 'separator_align_left',
				__( 'Align center', 'edubase' ) => 'separator_align_center',
				__( 'Align right', 'edubase' )  => 'separator_align_right'
				)
			),

			 
			array(
				'type' => 'loop',
				'heading' => __( 'Grids content', 'js_composer' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => __( 'Create WordPress loop, to populate content from your site.', 'js_composer' )
			),
			 

			array(
				'type' => 'textfield',
				'heading' => __( 'Thumbnail size', 'js_composer' ),
				'param_name' => 'grid_thumb_size',
				'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'js_composer' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
			)
		)
	) );
// frontpage 13
	vc_map( array(
		'name' => __( '(News) FontPage 13', 'edubase' ),
		'base' => 'wpo_frontpageposts13',
		'icon' => 'icon-wpb-news-13',
		"category" => __('Opal News', 'edubase'),
		'description' => __( 'Create Categories Tab Hovering to show post', 'edubase' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Widget title', 'js_composer' ),
				'param_name' => 'title',
				'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => __( 'Title Alignment', 'edubase' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				__( 'Align left', 'edubase' )   => 'separator_align_left',
				__( 'Align center', 'edubase' ) => 'separator_align_center',
				__( 'Align right', 'edubase' )  => 'separator_align_right'
				)
			),

			 
			array(
				'type' => 'loop',
				'heading' => __( 'Grids content', 'js_composer' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => __( 'Create WordPress loop, to populate content from your site.', 'js_composer' )
			),
			 

			array(
				'type' => 'textfield',
				'heading' => __( 'Thumbnail size', 'js_composer' ),
				'param_name' => 'grid_thumb_size',
				'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'js_composer' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
			)
		)
	) );	

		/**********************************************************************************
	 * FontPage News 14
	 **********************************************************************************/
	vc_map( array(
		'name' => __( '(News) FrontPage 14', 'edubase' ),
		'base' => 'wpo_frontpageposts14',
		'icon' => 'icon-wpb-news-1',
		"category" => __('Opal News', 'edubase'),
		'description' => __( 'Create Post having blog styles', 'edubase' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Widget title', 'js_composer' ),
				'param_name' => 'title',
				'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => __( 'Title Alignment', 'edubase' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				__( 'Align left', 'edubase' )   => 'separator_align_left',
				__( 'Align center', 'edubase' ) => 'separator_align_center',
				__( 'Align right', 'edubase' )  => 'separator_align_right'
				)
			),
 
			array(
				'type' => 'loop',
				'heading' => __( 'Grids content', 'js_composer' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => __( 'Create WordPress loop, to populate content from your site.', 'js_composer' )
			),
			array(
				"type" => "dropdown",
				"heading" => __("Number Main Posts", 'js_composer'),
				"param_name" => "num_mainpost",
				"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
				"std" => 1
			),

			array(
				'type' => 'textfield',
				'heading' => __( 'Thumbnail size', 'js_composer' ),
				'param_name' => 'grid_thumb_size',
				'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'js_composer' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
			)
		)
	) );
	
vc_map( array(
		'name' => __( '(News) Categories Post', 'edubase' ),
		'base' => 'wpo_categoriespost',
		'icon' => 'icon-wpb-news-3',
		"category" => __('Opal News', 'edubase'),
		'description' => __( 'Create Post having blog styles', 'edubase' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Widget title', 'js_composer' ),
				'param_name' => 'title',
				'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => __( 'Title Alignment', 'edubase' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				__( 'Align left', 'edubase' )   => 'separator_align_left',
				__( 'Align center', 'edubase' ) => 'separator_align_center',
				__( 'Align right', 'edubase' )  => 'separator_align_right'
				)
			),

		 

			array(
				'type' => 'loop',
				'heading' => __( 'Grids content', 'js_composer' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => __( 'Create WordPress loop, to populate content from your site.', 'js_composer' )
			),
			 

			array(
				'type' => 'textfield',
				'heading' => __( 'Thumbnail size', 'js_composer' ),
				'param_name' => 'grid_thumb_size',
				'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'js_composer' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
			)
		)
	) );	

// front page 9
	vc_map( array(
		'name' => __( '(News) FrontPage 9', 'edubase' ),
		'base' => 'wpo_frontpageposts9',
		'icon' => 'icon-wpb-news-9',
		"category" => __('Opal News', 'edubase'),
		'description' => __( 'Create Post having blog styles', 'edubase' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Widget title', 'js_composer' ),
				'param_name' => 'title',
				'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => __( 'Title Alignment', 'edubase' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				__( 'Align left', 'edubase' )   => 'separator_align_left',
				__( 'Align center', 'edubase' ) => 'separator_align_center',
				__( 'Align right', 'edubase' )  => 'separator_align_right'
				)
			),

			 
			array(
				'type' => 'loop',
				'heading' => __( 'Grids content', 'js_composer' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => __( 'Create WordPress loop, to populate content from your site.', 'js_composer' )
			),
			array(
				"type" => "dropdown",
				"heading" => __("Grid Columns", 'js_composer'),
				"param_name" => "grid_columns",
				"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
				"std" => 1
			),

			array(
				'type' => 'textfield',
				'heading' => __( 'Thumbnail size', 'js_composer' ),
				'param_name' => 'grid_thumb_size',
				'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'js_composer' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
			)
		)
	) );

	// front page 3
	vc_map( array(
		'name' => __( '(Blog) TimeLine Post', 'edubase' ),
		'base' => 'wpo_timelinepost',
		'icon' => 'icon-wpb-news-10',
		"category" => __('Opal News', 'edubase'),
		'description' => __( 'Create Post having blog styles', 'edubase' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Widget title', 'js_composer' ),
				'param_name' => 'title',
				'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => __( 'Title Alignment', 'edubase' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				__( 'Align left', 'edubase' )   => 'separator_align_left',
				__( 'Align center', 'edubase' ) => 'separator_align_center',
				__( 'Align right', 'edubase' )  => 'separator_align_right'
				)
			),
 

			array(
				'type' => 'loop',
				'heading' => __( 'Grids content', 'js_composer' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => __( 'Create WordPress loop, to populate content from your site.', 'js_composer' )
			),
			 

			array(
				'type' => 'textfield',
				'heading' => __( 'Thumbnail size', 'js_composer' ),
				'param_name' => 'grid_thumb_size',
				'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'js_composer' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
			),
			array(
				'type' => 'dropdown',
				'heading' => __( 'Enable Pagination', 'edubase' ),
				'param_name' => 'pagination',
				'value' => array( 'No'=>'0', 'Yes'=>'1'),
				'std' => '0',
				'admin_label' => true,
				'description' => __( 'Select style display.', 'edubase' )
			)
		)
	) );

	/****/
	vc_map( array(
		'name' => __( '(News) Categories Tab Post', 'edubase' ),
		'base' => 'wpo_categorytabpost',
		'icon' => 'icon-wpb-application-icon-large',
		"category" => __('Opal News', 'edubase'),
		'description' => __( 'Create Post having blog styles', 'edubase' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Widget title', 'js_composer' ),
				'param_name' => 'title',
				'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => __( 'Title Alignment', 'edubase' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				__( 'Align left', 'edubase' )   => 'separator_align_left',
				__( 'Align center', 'edubase' ) => 'separator_align_center',
				__( 'Align right', 'edubase' )  => 'separator_align_right'
				)
			),

			 
			array(
				'type' => 'loop',
				'heading' => __( 'Grids content', 'js_composer' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => __( 'Create WordPress loop, to populate content from your site.', 'js_composer' )
			),

		 

			array(
				"type" => "dropdown",
				"heading" => __("Number Main Posts", 'js_composer'),
				"param_name" => "num_mainpost",
				"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
				"std" => 1
			),

			array(
				'type' => 'textfield',
				'heading' => __( 'Thumbnail size', 'js_composer' ),
				'param_name' => 'grid_thumb_size',
				'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'js_composer' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
			)
		)
	) );
	

	$layout_image = array(
		__('Grid', 'edubase')             => 'grid-1',
		__('Grid - Center', 'edubase')    => 'grid-2',
		__('List', 'edubase')             => 'list-1',
		__('List not image', 'edubase')   => 'list-2',
	);
	vc_map( array(
		'name' => __( '(News) Grid Posts', 'edubase' ),
		'base' => 'wpo_gridposts',
		'icon' => 'icon-wpb-news-2',
		"category" => __('Opal News', 'edubase'),
		'description' => __( 'Post having news,managzine style', 'edubase' ),
	 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Widget title', 'js_composer' ),
				'param_name' => 'title',
				'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => __( 'Title Alignment', 'edubase' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				__( 'Align left', 'edubase' )   => 'separator_align_left',
				__( 'Align center', 'edubase' ) => 'separator_align_center',
				__( 'Align right', 'edubase' )  => 'separator_align_right'
				)
			),

		 
			array(
				'type' => 'loop',
				'heading' => __( 'Grids content', 'js_composer' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => __( 'Create WordPress loop, to populate content from your site.', 'js_composer' )
			),
			array(
				"type" => "dropdown",
				"heading" => __("Layout Type", 'js_composer'),
				"param_name" => "layout",
				"layout_images" => $layout_image,
				"value" => $layout_image,
				"admin_label" => true,
				"description" => __("Select Skin layout.", 'js_composer')
			),
			array(
				"type" => "dropdown",
				"heading" => __("Grid Columns", 'js_composer'),
				"param_name" => "grid_columns",
				"value" => array( 1 , 2 , 3 , 4 , 6),
				"std" => 3
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Thumbnail size', 'js_composer' ),
				'param_name' => 'grid_thumb_size',
				'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'js_composer' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
			)
		)
	) );


	
	/**********************************************************************************
	 * Mega Blogs
	 **********************************************************************************/

	/// Front Page 1
	vc_map( array(
		'name' => __( '(Blog) FrontPage', 'edubase' ),
		'base' => 'wpo_frontpageblog',
		'icon' => 'icon-wpb-news-1',
		"category" => __('Opal News', 'edubase'),
		'description' => __( 'Create Post having blog styles', 'edubase' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Widget title', 'js_composer' ),
				'param_name' => 'title',
				'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' ),
				"admin_label" => true
			),
 			 
			array(
				'type' => 'loop',
				'heading' => __( 'Grids content', 'js_composer' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => __( 'Create WordPress loop, to populate content from your site.', 'js_composer' )
			),
			array(
				"type" => "dropdown",
				"heading" => __("Number Main Posts", 'js_composer'),
				"param_name" => "num_mainpost",
				"value" => array( 1 , 2 , 3 , 4 , 5 , 6),
				"std" => 1
			),

			array(
				'type' => 'textfield',
				'heading' => __( 'Thumbnail size', 'js_composer' ),
				'param_name' => 'grid_thumb_size',
				'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'js_composer' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
			)
		)
	) );


	vc_map( array(
		'name' => __('(Blog) Grids ', 'edubase' ),
		'base' => 'wpo_megablogs',
		'icon' => 'icon-wpb-news-2',
		"category" => __('Opal News', 'edubase'),
		'description' => __( 'Create Post having blog styles', 'edubase' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Widget title', 'js_composer' ),
				'param_name' => 'title',
				'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' ),
				"admin_label" => true
			),

		 

		 
			array(
				'type' => 'textarea',
				'heading' => __( 'Description', 'edubase' ),
				'param_name' => 'descript',
				"value" => ''
			),

			array(
				'type' => 'loop',
				'heading' => __( 'Grids content', 'js_composer' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 10 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => __( 'Create WordPress loop, to populate content from your site.', 'js_composer' )
			),

			array(
				"type" => "dropdown",
				"heading" => __("Layout", 'edubase' ),
				"param_name" => "layout",
				"value" => array( __('Default Style', 'edubase' ) => 'blog' , __('Default Style 2', 'edubase' ) => 'blog-v2'  ,  __('Special Style 1', 'edubase' ) => 'special-1',  __('Special Style 2', 'edubase' ) => 'special-2',  __('Special Style 3', 'edubase' ) => 'special-3' ),
				"std" => 3,
				'admin_label'=> true
			),

			array(
				"type" => "dropdown",
				"heading" => __("Grid Columns", 'js_composer'),
				"param_name" => "grid_columns",
				"value" => array( 1 , 2 , 3 , 4 , 6),
				"std" => 3
			),

			array(
				'type' => 'textfield',
				'heading' => __( 'Thumbnail size', 'js_composer' ),
				'param_name' => 'grid_thumb_size',
				'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'js_composer' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
			)
		)
	) );
 

	/**********************************************************************************
	 * Slideshow Post Widget Gets
	 **********************************************************************************/
		vc_map( array(
			'name' => __( '(News) Slideshow Post', 'edubase' ),
			'base' => 'wpo_slideshowpost',
			'icon' => 'icon-wpb-news-slideshow',
			"category" => __('Opal News', 'edubase'),
			'description' => __( 'Play Posts In slideshow', 'edubase' ),
			 
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' => __( 'Widget title', 'js_composer' ),
					'param_name' => 'title',
					'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' ),
					"admin_label" => true
				),

				array(
					'type' => 'dropdown',
					'heading' => __( 'Title Alignment', 'edubase' ),
					'param_name' => 'alignment',
					'value' => array(
						__( 'Align left', 'edubase' ) => 'separator_align_left',
						__( 'Align center', 'edubase' ) => 'separator_align_center',
						__( 'Align right', 'edubase' ) => 'separator_align_right'
					)
				),

			 

				array(
					'type' => 'textarea',
					'heading' => __( 'Heading Description', 'edubase' ),
					'param_name' => 'descript',
					"value" => ''
				),

				array(
					'type' => 'loop',
					'heading' => __( 'Grids content', 'js_composer' ),
					'param_name' => 'loop',
					'settings' => array(
						'size' => array( 'hidden' => false, 'value' => 10 ),
						'order_by' => array( 'value' => 'date' ),
					),
					'description' => __( 'Create WordPress loop, to populate content from your site.', 'js_composer' )
				),

				array(
					"type" => "dropdown",
					"heading" => __("Layout", 'edubase' ),
					"param_name" => "layout",
					"value" => array( __('Default Style', 'edubase' ) => 'blog'  ,  __('Special Style 1', 'edubase' ) => 'style1' ,  __('Special Style 2', 'edubase' ) => 'style2' ),
					"std" => 3
				),

				array(
					"type" => "dropdown",
					"heading" => __("Grid Columns", 'js_composer'),
					"param_name" => "grid_columns",
					"value" => array( 1 , 2 , 3 , 4 , 6),
					"std" => 3
				),


				array(
					'type' => 'textfield',
					'heading' => __( 'Thumbnail size', 'js_composer' ),
					'param_name' => 'grid_thumb_size',
					'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'js_composer' )
				),
				array(
					'type' => 'textfield',
					'heading' => __( 'Extra class name', 'js_composer' ),
					'param_name' => 'el_class',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
				)
			)
		) );
	
	// List
	vc_map( array(
		'name' => __( '(News) List Post', 'edubase' ),
		'base' => 'wpo_listpost',
		'icon' => 'icon-wpb-news-10',
		"category" => __('Opal News', 'edubase'),
		'description' => __( 'Create Post having blog styles', 'edubase' ),
		 
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => __( 'Widget title', 'js_composer' ),
				'param_name' => 'title',
				'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' ),
				"admin_label" => true
			),

			array(
				'type'                           => 'dropdown',
				'heading'                        => __( 'Title Alignment', 'edubase' ),
				'param_name'                     => 'alignment',
				'value'                          => array(
				__( 'Align left', 'edubase' )   => 'separator_align_left',
				__( 'Align center', 'edubase' ) => 'separator_align_center',
				__( 'Align right', 'edubase' )  => 'separator_align_right'
				)
			),

			 array(
				'type' => 'dropdown',
				'heading' => __( 'Title font size', 'edubase' ),
				'param_name' => 'size',
				'value' => array(
					__( 'Large', 'edubase' ) => 'font-size-lg',
					__( 'Medium', 'edubase' ) => 'font-size-md',
					__( 'Small', 'edubase' ) => 'font-size-sm',
					__( 'Extra small', 'edubase' ) => 'font-size-xs'
				),
				'description' => __( 'Select title font size.', 'edubase' )
			),
			 
			array(
				'type' => 'loop',
				'heading' => __( 'Grids content', 'js_composer' ),
				'param_name' => 'loop',
				'settings' => array(
					'size' => array( 'hidden' => false, 'value' => 4 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'description' => __( 'Create WordPress loop, to populate content from your site.', 'js_composer' )
			),
			 

			array(
				'type' => 'textfield',
				'heading' => __( 'Thumbnail size', 'js_composer' ),
				'param_name' => 'grid_thumb_size',
				'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'js_composer' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Extra class name', 'js_composer' ),
				'param_name' => 'el_class',
				'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
			)
		)
	) );


}
