<?php 
if( EDUBASE_WPO_BBPRESS_ACTIVED ){ 

	     vc_map( array(
	        "name" => __("Recent Topics",'edubase').'(bbpress)',
	        "base" => "wpo_bbpress_topics",
	        "class" => "",
	        "category" => __('Opal PuddyPress','edubase'),
	        "description"	=> __( 'Display Topics from bbpress plugin', 'edubase' ),
	        "params" => array(
	            array(
	                "type" => "textfield",
	                "class" => "",
	                "heading" => __('Title', 'edubase'),
	                "param_name" => "title",
	                "admin_label" => true
	            ),
	            array(
	                "type" => "textfield",
	                "heading" => __("Max Topics to show", 'edubase'),
	                "param_name" => "max_shown",
	                "value" => '5',
	                "description" => __("Show Topics Group", 'edubase')
	            ),

	            array(
	                "type" => "dropdown",
	                "heading" => __("Order By",'edubase'),
	                "param_name" => "order_by",
	                "value" => array(
	                	__( 'Newest', 'edubase' ) => 'newness',
	                	__( 'Newest Topics Topics', 'edubase' ) => 'popular',
	                	__( 'Topics With Recent Replies', 'edubase' ) => 'freshness',
	                ),
	                "admin_label" => false,
	                "description" => __("Show List of Group As expected.",'edubase')
	            ),
	           
	             array(
	                "type" => "dropdown",
	                "heading" => __("Column Display",'edubase'),
	                "param_name" => "column",
	                "value" => array(
	                	__( '2 Columns', 'edubase' ) => '2',
	                	__( '1 Column', 'edubase' ) => '1',
	                ),
	                "admin_label" => false,
	                "description" => __("Display Title and Avatar Block in expected Style",'edubase')
	            ),
	            array(
	                "type" => "textfield",
	                "heading" => __("Extra class name", 'edubase'),
	                "param_name" => "el_class",
	                "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
	            ),
	        )
	    ));
}