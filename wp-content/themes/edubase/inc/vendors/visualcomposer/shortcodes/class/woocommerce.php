<?php
if( EDUBASE_WPO_WOOCOMMERCE_ACTIVED ) {
	if( class_exists('WPBakeryShortCode')){
		class WPBakeryShortCode_Wpo_All_Products extends WPBakeryShortCode {

			public function getListQuery( $show_tab){
				$list_query = array();
				$types = explode(',', $show_tab);
				foreach ($types as $type) {
					$list_query[$type] = $this->getTabTitle($type);
				}
				return $list_query;
			}

			public function getTabTitle($type){
				switch ($type) {
					case 'recent':
						return array('title'=>__('Latest Products','edubase'),'title_tab'=>__('Latest','edubase'));
					case 'featured_product':
						return array('title'=>__('Featured Products','edubase'),'title_tab'=>__('Featured','edubase'));
					case 'top_rate':
						return array('title'=> __('Top Rated Products','edubase'),'title_tab'=>__('Top Rated', 'edubase'));
					case 'best_selling':
						return array('title'=>__('BestSeller Products','edubase'),'title_tab'=>__('BestSeller','edubase'));
					case 'on_sale':
						return array('title'=>__('Special Products','edubase'),'title_tab'=>__('Special','edubase'));
				}
			}
		}
	}

	class WPBakeryShortCode_Wpo_Product_Deals extends WPBakeryShortCode {

	}
	 
	class WPBakeryShortCode_Wpo_Productcategory extends WPBakeryShortCode {

	}

	class WPBakeryShortCode_Wpo_Category_Filter extends WPBakeryShortCode {

	}


	class WPBakeryShortCode_Wpo_Products extends WPBakeryShortCode {

	}


	 class WPBakeryShortCode_Wpo_Category_List extends WPBakeryShortCode {

	}

}