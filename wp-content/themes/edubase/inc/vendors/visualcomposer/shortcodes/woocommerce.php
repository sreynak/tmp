<?php
/**
 * Theme function
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <opalwordpress@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
if( EDUBASE_WPO_WOOCOMMERCE_ACTIVED ) {
		$params 	= array();
	 	$cats 		= array();
		$product_columns_deal = array(1, 2, 3, 4);
		$value 		= array();

		/**
		 * wpo_productcategory
		 */
		

		$product_layout  = array('Grid'=>'grid','List'=>'list','Carousel'=>'carousel');
		$product_type    = array('Best Selling'=>'best_selling','Featured Products'=>'featured_product','Top Rate'=>'top_rate','Recent Products'=>'recent_product','On Sale'=>'on_sale','Recent Review' => 'recent_review' );
		$product_columns = array(6,4, 3, 2, 1);
		$show_tab 		 = array(
			                array('recent', __('Latest Products', 'edubase')),
			                array( 'featured_product', __('Featured Products', 'edubase' )),
			                array('best_selling', __('BestSeller Products', 'edubase' )),
			                array('top_rate', __('TopRated Products', 'edubase' )),
			                array('on_sale', __('Special Products', 'edubase' ))
			            );


		
		if( is_admin() ){
			global $wpdb;
			$sql     = "SELECT a.name,a.slug,a.term_id FROM $wpdb->terms a JOIN  $wpdb->term_taxonomy b ON (a.term_id= b.term_id ) where b.count>0 and b.taxonomy = 'product_cat'";
			$results = $wpdb->get_results($sql);
		
			foreach ($results as $vl) {
				$value[$vl->name] = $vl->slug;
			}
			$query 		= "SELECT a.name,a.slug,a.term_id FROM $wpdb->terms a JOIN  $wpdb->term_taxonomy b ON (a.term_id= b.term_id ) where b.count>0 and b.taxonomy = 'product_cat' and b.parent = 0";
			$categories = $wpdb->get_results($query);
			
			foreach ($categories as $category) {
				$cats[$category->name] = $category->term_id;
			}
		}


	    vc_map( array(
	        "name" => __("WPO Product Deals",'edubase'),
	        "base" => "wpo_product_deals",
	        "class" => "",
	        "category" => __('Opal Woocommece','edubase'),
	        "params" => array(
	            array(
	                "type" => "textfield",
	                "class" => "",
	                "heading" => __('Title', 'edubase'),
	                "param_name" => "title",
	                "admin_label" => true
	            ),
	            array(
	                "type" => "textfield",
	                "heading" => __("Extra class name", 'edubase'),
	                "param_name" => "el_class",
	                "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
	            ),
	            array(
	                "type" => "dropdown",
	                "heading" => __("Columns count",'edubase'),
	                "param_name" => "columns_count",
	                "value" => $product_columns_deal,
	                "admin_label" => true,
	                "description" => __("Select columns count.",'edubase')
	            ),
	            array(
	                "type" => "dropdown",
	                "heading" => __("Layout",'edubase'),
	                "param_name" => "layout",
	                "value" => array(__('Carousel', 'edubase') => 'carousel', __('Grid', 'edubase') =>'grid' ),
	                "admin_label" => true,
	                "description" => __("Select columns count.",'edubase')
	            )
	        )
	    ));
	

		vc_map( array(
		    "name" => __("WPO Product Category",'edubase'),
		    "base" => "wpo_productcategory",
		    "class" => "",
		    "category" =>__("Opal Woocommece",'edubase'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"class" => "",
					"heading" => __('Title', 'edubase'),
					"param_name" => "title",
					"value" =>''
				),
		    	array(
					"type" => "dropdown",
					"class" => "",
					"heading" => __('Category', 'edubase'),
					"param_name" => "category",
					"value" =>$value,
					"admin_label" => true
				),
				array(
					"type" => "dropdown",
					"heading" => __("Style",'edubase'),
					"param_name" => "style",
					"value" => $product_layout
				),
				array(
					"type"        => "attach_image",
					"description" => __("Upload an image for categories", 'edubase'),
					"param_name"  => "image_cat",
					"value"       => '',
					'heading'     => __('Image', 'edubase' )
				),
				array(
					"type" => "textfield",
					"heading" => __("Number of products to show",'edubase'),
					"param_name" => "number",
					"value" => '4'
				),
				array(
					"type" => "dropdown",
					"heading" => __("Columns count",'edubase'),
					"param_name" => "columns_count",
					"value" => $product_columns,
					"admin_label" => true,
					"description" => __("Select columns count.",'edubase')
				),
				array(
					"type" => "textfield",
					"heading" => __("Icon",'edubase'),
					"param_name" => "icon"
				),
				array(
					"type" => "textfield",
					"heading" => __("Extra class name",'edubase'),
					"param_name" => "el_class",
					"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'edubase')
				)
		   	)
		));


		/**
		* wpo_category_filter
		*/

		vc_map( array(
				"name"     => __("WPO Product Categories Filter",'edubase'),
				"base"     => "wpo_category_filter",
				"class"    => "",
				"category" => __('Opal Woocommece', 'edubase'),
				"params"   => array(

				array(
					"type" => "dropdown",
					"class" => "",
					"heading" => __('Category', 'edubase'),
					"param_name" => "term_id",
					"value" =>$cats,
					"admin_label" => true
				),

				array(
					"type"        => "attach_image",
					"description" => __("Upload an image for categories (190px x 190px)", 'edubase'),
					"param_name"  => "image_cat",
					"value"       => '',
					'heading'     => __('Image', 'edubase' )
				),

				array(
					"type"       => "textfield",
					"heading"    => __("Number of categories to show",'edubase'),
					"param_name" => "number",
					"value"      => '5'
				),

				array(
					"type"        => "textfield",
					"heading"     => __("Extra class name",'edubase'),
					"param_name"  => "el_class",
					"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'edubase')
				)
		   	)
		));


		/**
		 * wpo_products
		 */
		vc_map( array(
		    "name" => __("WPO Products",'edubase'),
		    "base" => "wpo_products",
		    "class" => "",
		    "category" => __('Opal Woocommece', 'edubase'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => __("Title",'edubase'),
					"param_name" => "title",
					"admin_label" => true,
					"value" => ''
				),
				 
		    	array(
					"type" => "dropdown",
					"heading" => __("Type",'edubase'),
					"param_name" => "type",
					"value" => $product_type,
					"admin_label" => true,
					"description" => __("Select columns count.",'edubase')
				),
				array(
					"type" => "dropdown",
					"heading" => __("Style",'edubase'),
					"param_name" => "style",
					"value" => $product_layout
				),
				array(
					"type" => "dropdown",
					"heading" => __("Columns count",'edubase'),
					"param_name" => "columns_count",
					"value" => $product_columns,
					"admin_label" => true,
					"description" => __("Select columns count.",'edubase')
				),
				array(
					"type" => "textfield",
					"heading" => __("Number of products to show",'edubase'),
					"param_name" => "number",
					"value" => '4'
				),
				array(
					"type" => "textfield",
					"heading" => __("Extra class name",'edubase'),
					"param_name" => "el_class",
					"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'edubase')
				)
		   	)
		));
	


		vc_map( array(
			"name"     => __("WPO Product Categories List",'edubase'),
			"base"     => "wpo_category_list",
			"class"    => "",
			"category" => __('Opal Woocommece', 'edubase'),
			"params"   => array(
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __('Title', 'edubase'),
				"param_name" => "title",
				"value"      => '',
			),
			array(
				'type' => 'checkbox',
				'heading' => __( 'Show post counts', 'edubase' ),
				'param_name' => 'show_count',
				'description' => __( 'Enables show count total product of category.', 'edubase' ),
				'value' => array( __( 'Yes, please', 'edubase' ) => 'yes' )
			),
			array(
				"type"       => "checkbox",
				"heading"    => __("show children of the current category",'edubase'),
				"param_name" => "show_children",
				'description' => __( 'Enables show children of the current category.', 'edubase' ),
				'value' => array( __( 'Yes, please', 'edubase' ) => 'yes' )
			),
			array(
				"type"       => "checkbox",
				"heading"    => __("Show dropdown children of the current category ",'edubase'),
				"param_name" => "show_dropdown",
				'description' => __( 'Enables show dropdown children of the current category.', 'edubase' ),
				'value' => array( __( 'Yes, please', 'edubase' ) => 'yes' )
			),

			array(
				"type"        => "textfield",
				"heading"     => __("Extra class name",'edubase'),
				"param_name"  => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'edubase')
			)
	   	)
	));
	
	$params =  array(
    	array(
			"type" => "textfield",
			"heading" => __("Title",'edubase'),
			"param_name" => "title",
			"admin_label" => true,
			"value" => ''
		),
		  
		array(
            "type" => "sorted_list",
            "heading" => __("Show Tab", "js_composer"),
            "param_name" => "show_tab",
            "description" => __("Control teasers look. Enable blocks and place them in desired order.", 'edubase'),
            "value" => "recent,featured_product,best_selling",
            "options" => $show_tab
        ),
        array(
			"type" => "dropdown",
			"heading" => __("Style",'edubase'),
			"param_name" => "style",
			"value" => $product_layout
		),
		array(
			"type" => "textfield",
			"heading" => __("Number of products to show",'edubase'),
			"param_name" => "number",
			"value" => '4'
		),
		array(
			"type" => "dropdown",
			"heading" => __("Columns count",'edubase'),
			"param_name" => "columns_count",
			"value" => $product_columns,
			"admin_label" => true,
			"description" => __("Select columns count.",'edubase')
		),
		array(
			"type" => "textfield",
			"heading" => __("Extra class name",'edubase'),
			"param_name" => "el_class",
			"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'edubase')
		)
   	);
 

	/**
	 * wpo_all_products
	 */
	vc_map( array(
	    "name" => __("WPO Products Tabs",'edubase'),
	    "base" => "wpo_all_products",
	    "class" => "",
	    "category" => __('Opal Woocommece', 'edubase'),
	    "params" => $params
	));

}