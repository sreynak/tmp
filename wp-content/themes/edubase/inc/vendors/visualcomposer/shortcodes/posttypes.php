<?php
if(EDUBASE_WPO_PLG_FRAMEWORK_ACTIVED) {
	/**********************************************************************************************************************
	* Testimonials
	 **********************************************************************************************************************/
	if( edubase_wpo_get_posttype_enable('testimonials')){
		vc_map( array(
		    "name" => __("WPO Testimonials",'edubase'),
		    "base" => "wpo_testimonials",
		    'description'=> __('Play Testimonials In Carousel', 'edubase'),
		    "class" => "",
		    "category" => __('Opal Elements', 'edubase'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => __("Title", 'edubase'),
					"param_name" => "title",
					"admin_label" => true,
					"value" => '',
						"admin_label" => true
				),

		    	array(
	                'type' => 'dropdown',
	                'heading' => __( 'Title font size', 'edubase' ),
	                'param_name' => 'size',
	                'value' => array(
	                    __( 'Large', 'edubase' ) => 'font-size-lg',
	                    __( 'Medium', 'edubase' ) => 'font-size-md',
	                    __( 'Small', 'edubase' ) => 'font-size-sm',
	                    __( 'Extra small', 'edubase' ) => 'font-size-xs'
	                )
	            ),

	            array(
	                'type' => 'dropdown',
	                'heading' => __( 'Title Alignment', 'edubase' ),
	                'param_name' => 'alignment',
	                'value' => array(
	                    __( 'Align left', 'edubase' ) => 'separator_align_left',
	                    __( 'Align center', 'edubase' ) => 'separator_align_center',
	                    __( 'Align right', 'edubase' ) => 'separator_align_right'
	                )
	            ),
	            array(
					"type" => "textfield",
					"heading" => __("Number", 'edubase'),
					"param_name" => "number",
					"value" => '6',
				),
				array(
					"type" => "dropdown",
					"heading" => __("Skin", 'edubase'),
					"param_name" => "skin",
					"value" => array('Version Style left'=>'left','Version Style 2'=>'v2','Version Style Slide'=>'slide','Version Style 4'=>'v4','Version Style 5'=>'v5','Version Style 6'=>'v6'),
					"admin_label" => true,
					"description" => __("Select Skin layout.", 'edubase')
				),
				array(
					"type" => "textfield",
					"heading" => __("Extra class name", 'edubase'),
					"param_name" => "el_class",
					"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
				)
		   	)
		));
	}

	/*********************************************************************************************************************
	 *  Portfolio
	 *********************************************************************************************************************/
	if( edubase_wpo_get_posttype_enable('portfolio')){
		vc_map( array(
		    "name" => __("WPO Portfolio",'edubase'),
		    "base" => "wpo_portfolio",
		    'icon' => 'icon-wpb-news-15',
		    'description'=>'Portfolio',
		    "class" => "",
		    "category" => __('Opal Elements', 'edubase'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => __("Title", 'edubase'),
					"param_name" => "title",
					"value" => '',
					"admin_label" => true
				),
		    	 array(
	                "type" => "checkbox",
	                "heading" => __("Item No Padding", 'edubase'),
	                "param_name" => "nopadding",
	                "value" => array(
	                    'Yes, It is Ok' => true
	                ),
	                'std' => true
				),	
		    	 
			 
				array(
					"type" => "textarea",
					'heading' => __( 'Description', 'edubase' ),
					"param_name" => "descript",
					"value" => ''
			    ),

				array(
					'type' => 'dropdown',
					'heading' => __( 'Display Masonry', 'edubase' ),
					'param_name' => 'masonry',
					'value' => array(
						__( 'No', 'edubase' ) => '0',
						__( 'Yes', 'edubase' ) => '1',
					)
				),

				array(
					"type" => "textfield",
					"heading" => __("Number of portfolio to show", 'edubase'),
					"param_name" => "number",
					"value" => '6'
				),

				array(
					'type' => 'dropdown',
					'heading' => __( 'Columns count', 'edubase' ),
					'param_name' => 'columns_count',
					'value' => array( 6, 4, 3, 2, 1 ),
					'std' => 3,
					'admin_label' => true,
					'description' => __( 'Select columns count.', 'edubase' )
				),
				array(
					'type' => 'dropdown',
					'heading' => __( 'Enable Pagination', 'edubase' ),
					'param_name' => 'pagination',
					'value' => array( 'No'=>'0', 'Yes'=>'1'),
					'std' => 'style-1',
					'admin_label' => true,
					'description' => __( 'Select style display.', 'edubase' )
				),
				array(
					"type" => "textfield",
					"heading" => __("Extra class name", 'edubase'),
					"param_name" => "el_class",
					"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
				)
		   	)
		));
	}

	if( edubase_wpo_get_posttype_enable('team')){
		vc_map( array(
		    "name" => __("(Team) Grid Carousel",'edubase'),
		    "base" => "wpo_teamcarousel",
		    "class" => "",
		    "description" => __('Show data from Posttype Team in Carousel and Grid Style', 'edubase'),
		    "category" => __('Opal Elements', 'edubase'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => __("Title", 'edubase'),
					"param_name" => "title",
					"value" => '',
						"admin_label" => true
				),
			 	

			 	array(
					"type" => "textfield",
					"heading" => __("Number Of Items", 'edubase'),
					"param_name" => "number",
					"value" => '4',
						"admin_label" => false
				),
			 	
				array(
					"type" => "dropdown",
					"heading" => __("Show In Columns", 'edubase'),
					"param_name" => "show",
					'description'	=> __('Display Team in N Columns Of Carousel Style','edubase'),
					'value' 	=> array(  __('1 Column', 'edubase') => '1',  __('2 Columns', 'edubase') => '2',  __('3 Columns', 'edubase') => '3' , __('4 Columns', 'edubase')  => '4' , __('5 Columns', 'edubase') => '5')
				),

				array(
					"type" => "textfield",
					"heading" => __("Extra class name", 'edubase'),
					"param_name" => "el_class",
					"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
				)
		   	)
		));
	}
	/*********************************************************************************************************************
	 *  Gallery grid 
	 *********************************************************************************************************************/
	if( edubase_wpo_get_posttype_enable('gallery')){
		vc_map( array(
		    "name" => __("WPO Gallery Grid",'edubase'),
		    "base" => "wpo_gallery_grid",
		    'icon' => 'icon-wpb-application-icon-large',
		    'description'=>'Display Gallery Grid',
		    "class" => "",
		    "category" => __('Opal Elements', 'edubase'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => __("Title", 'edubase'),
					"param_name" => "title",
					"value" => '',
					"admin_label" => true
				),
		    	 
				 
				array(
					"type" => "textfield",
					'heading' => __( 'Number gallery', 'edubase' ),
					"param_name" => "number",
					"value" => '6'
			    ),
			    array(
					"type" => "dropdown",
					'heading' => __( 'Columns', 'edubase' ),
					"param_name" => "column",
					"value" => array('2'=>'2', '3'=>'3', '4'=>'4')
			    ),
			    array(
					"type" => "dropdown",
					'heading' => __( 'Remove Padding', 'edubase' ),
					"param_name" => "padding",
					"value" => array(__('No', 'edubase') => '', __('Yes', 'edubase') => '1')
			    ),
			    array(
					"type" => "textfield",
					'heading' => __( 'Extra class name', 'edubase' ),
					"param_name" => "class",
					"value" => ''
			    )
		   	)
		));

	
		/*********************************************************************************************************************
		 *  Gallery portfolio
		 *********************************************************************************************************************/
		vc_map( array(
		    "name" => __("WPO Gallery Filter",'edubase'),
		    "base" => "wpo_gallery_filter",
		    'icon' => 'icon-wpb-news-15',
		    'description'=>'Show Gallery with gallery post type',
		    "class" => "",
		    "category" => __('Opal Elements', 'edubase'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => __("Title", 'edubase'),
					"param_name" => "title",
					"value" => '',
					"admin_label" => true
				),
		    	 
			 
				array(
					'type' => 'textfield',
					'heading' => __( 'Number gallery', 'edubase' ),
					'param_name' => 'number',
					'value' => '9'
			    ),
			    array(
					"type" => "dropdown",
					'heading' => __( 'Columns', 'edubase' ),
					'param_name' => 'column',
					"value" => array('2' => '2', '3' => '3', '4' => '4'),
			    ),
			    array(
			    	'type' => 'dropdown',
			    	'heading' => __('Show Pagination', 'edubase'),
			    	'param_name' => 'pagination',
			    	'value' => array(__('Yes', 'edubase') => '1', __('No', 'edubase') => '0' )
			    )
		   	)
		));
	}

	if(edubase_wpo_get_posttype_enable('video')){
		vc_map( array(
			    "name" => __("Opal Video Grid V1",'edubase'),
			    "base" => "wpo_video_grid_v1",
			    'icon' => 'icon-wpb-news-15',
			    'description'=>'Show Gallery with gallery post type',
			    "class" => "",
			    "category" => __('Opal Elements', 'edubase'),
			    "params" => array(
			    	array(
						"type" => "textfield",
						"heading" => __("Title", 'edubase'),
						"param_name" => "title",
						"value" => '',
						"admin_label" => true
					),
					array(
						'type' => 'textfield',
						'heading' => __( 'Number gallery', 'edubase' ),
						'param_name' => 'number',
						'value' => '9'
				    ),
				    array(
						'type' => 'textfield',
						'heading' => __( 'Grid Columns', 'edubase' ),
						'param_name' => 'grid_columns',
						'value' => '2'
				    ),
			   	)
			));
	}

	if(edubase_wpo_get_posttype_enable('brand')){

		/*********************************************************************************************************************
	 	*  Brands Carousel
	 	*********************************************************************************************************************/

		vc_map( array(
		    "name" => __("WPO Brands Carousel",'edubase'),
		    "base" => "wpo_brands",
		    'description'=>'Show Brand Logos, Manufacture Logos',
		    "class" => "",
		    "category" => __('Opal Woocommece','edubase'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => __("Title", 'edubase'),
					"param_name" => "title",
					"value" => '',
						"admin_label" => true
				),

				array(
					"type" => "textarea",
					"heading" => __('Description', 'edubase'),
					"param_name" => "descript",
					"value" => ''
				),

				array(
					"type" => "textfield",
					"heading" => __("Number of brands to show", 'edubase'),
					"param_name" => "number",
					"value" => '6'
				),
				array(
					"type" => "textfield",
					"heading" => __("Icon", 'edubase'),
					"param_name" => "icon"
				),
				array(
					"type" => "textfield",
					"heading" => __("Extra class name", 'edubase'),
					"param_name" => "el_class",
					"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'edubase')
				)
		   	)
		));
	}
}