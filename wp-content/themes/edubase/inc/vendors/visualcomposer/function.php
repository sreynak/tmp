<?php
/**
 * Theme function
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <opalwordpress@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
 	

  

/**
 * Create variant objects to modify and proccess actions of only theme.
 */  
 	
 	function edubase_wpo_vc_woocommerce_shortcode_render( $atts, $content='' , $tag='' ){
	    $output = '';
	    if(is_file( EDUBASE_WPO_THEME_TEMPLATE_DIR_PAGEBUILDER.'woocommerce/'. $tag.'.php')){
	      ob_start();
	      require( EDUBASE_WPO_THEME_TEMPLATE_DIR_PAGEBUILDER.'woocommerce/'.$tag.'.php' );
	      $output .= ob_get_clean();
	    }
	    return $output;
	}

 	function edubase_wpo_vc_news_shortcode_render( $atts, $content='' , $tag='' ){
	    $output = '';
	    if(is_file( EDUBASE_WPO_THEME_TEMPLATE_DIR_PAGEBUILDER.'news/'. $tag.'.php')){
	      ob_start();
	      require( EDUBASE_WPO_THEME_TEMPLATE_DIR_PAGEBUILDER.'news/'.$tag.'.php' );
	      $output .= ob_get_clean();
	    }
	    return $output;
	}

 	function edubase_wpo_vc_buddypress_shortcode_render( $atts, $content='' , $tag='' ){
	    $output = '';
	    if(is_file( EDUBASE_WPO_THEME_TEMPLATE_DIR_PAGEBUILDER.'buddypress/'. $tag.'.php')){
	      ob_start();
	      require( EDUBASE_WPO_THEME_TEMPLATE_DIR_PAGEBUILDER.'buddypress/'.$tag.'.php' );
	      $output .= ob_get_clean();
	    }
	    return $output;
	}

	///// Define  list of function processing theme logics.
	function edubase_wpo_vc_shortcode_render( $atts, $content='' , $tag='' ){
	  	$output = '';
	  	if(is_file( EDUBASE_WPO_THEME_TEMPLATE_DIR_PAGEBUILDER. $tag.'.php')){
	  		ob_start();
	  		require( EDUBASE_WPO_THEME_TEMPLATE_DIR_PAGEBUILDER.$tag.'.php' );
	  		$output .= ob_get_clean();
	  	}
	  	return $output;
	}



	function edubase_wpo_vc_elements_render( $atts, $content='' , $tag='' ){
	  $output = '';
	  if(is_file( EDUBASE_WPO_THEME_TEMPLATE_DIR_PAGEBUILDER.'elements/'. $tag.'.php')){
	    ob_start();
	    require( EDUBASE_WPO_THEME_TEMPLATE_DIR_PAGEBUILDER.'elements/'.$tag.'.php' );
	    $output .= ob_get_clean();
	  }
	  return $output;
	}
		
		/** 
		 * Replace pagebuilder columns and rows class by bootstrap classes
		 */
		function edubase_wpo_change_bootstrap_class( $class_string,$tag ){
		 
			if ($tag=='vc_column' || $tag=='vc_column_inner') {
				$class_string = preg_replace('/vc_span(\d{1,2})/', 'col-md-$1', $class_string);
				$class_string = preg_replace('/vc_hidden-(\w)/', 'hidden-$1', $class_string);
				$class_string = preg_replace('/vc_col-(\w)/', 'col-$1', $class_string);
				$class_string = str_replace('wpb_column', '', $class_string);
				$class_string = str_replace('column_container', '', $class_string);
			}
			return $class_string;
		}
		  add_filter( 'vc_shortcodes_css_class', 'edubase_wpo_change_bootstrap_class',10,2);

		/** 
		 * Add vc parameters 
		 */
		function edubase_wpo_add_vc_params(){
			
			/**
			 * add new params for row
			 */
			vc_add_param( 'vc_row', array(
			    "type" => "checkbox",
			    "heading" => __("Parallax", 'edubase'),
			    "param_name" => "parallax",
			    "value" => array(
			        'Yes, please' => true
			    )
			));

			vc_add_param( 'vc_row',   array(
                'type' => 'dropdown',
                'heading' => __( 'Background Styles', 'edubase' ),
                'param_name' => 'bgstyle',
                'description'	=> __('Use Styles Supported In Theme, Select No Use For Customizing on Tab Design Options','edubase'),
                'value' => array(
					__( 'No Use', 'edubase' ) => '',
					__( 'Background Color Primary', 'edubase' ) => 'bg-primary',
					__( 'Background Color Info', 'edubase' ) 	 => 'bg-info',
					__( 'Background Color Danger', 'edubase' )  => 'bg-danger',
					__( 'Background Color Warning', 'edubase' ) => 'bg-warning',
					__( 'Background Color Success', 'edubase' ) => 'bg-success',
					__( 'Background Color Theme', 'edubase' ) 	 => 'bg-theme',
				    __( 'Background Image 1', 'edubase' ) => 'bg-style-v1',
					__( 'Background Image 2', 'edubase' ) => 'bg-style-v2',
					__( 'Background Image 3', 'edubase' ) => 'bg-style-v3',
					__( 'Background Image 4', 'edubase' ) => 'bg-style-v4',
                )
            ) );

		 

			 vc_add_param( 'vc_row', array(
			     "type" => "dropdown",
			     "heading" => __("Is Boxed", 'edubase'),
			     "param_name" => "isfullwidth",
			     "value" => array(
			     				__('Yes, Boxed', 'edubase') => '1',
			     				__('No, Wide', 'edubase') => '0'
			     			)
			));

			vc_add_param( 'vc_row', array(
			    "type" => "textfield",
			    "heading" => __("Icon", 'edubase'),
			    "param_name" => "icon",
			    "value" => '',
				'description'	=> __( 'This support display icon from FontAwsome, Please click', 'edubase' )
								. '<a href="' . ( is_ssl()  ? 'https' : 'http') . '://fortawesome.github.io/Font-Awesome/" target="_blank">'
								. __( 'here to see the list, and use class icons-lg, icons-md, icons-sm to change its size', 'edubase' ) . '</a>'
			));
		}
	 	add_action('init','edubase_wpo_add_vc_params',100);

		/**
		 * auto add footer type in visual composer
		 */
		function edubase_set_visual_composer_post_type(){

			if($options = get_option('wpb_js_content_types')){
				$check = true;
			foreach ($options as $key => $value) {
				if($value=='footer'){  
					$check=false;
				}
			}
			if($check)
				$options[] = 'footer';
			}else{
				$options = array('page','footer');
			}

			update_option( 'wpb_js_content_types',$options );
		}
		add_action('init','edubase_set_visual_composer_post_type',100);

		vc_add_shortcode_param('wpo_datepicker', 'edubase_wpo_datepicker_settings_field', get_template_directory_uri().'/inc/assets/js/datepicker.js');
		function edubase_wpo_datepicker_settings_field( $settings, $value ) {
			
			wp_enqueue_script( 'jquery-ui-datepicker' );
			
			return '<div class="wpo_datetimepicker_block">'             
							.'<input id="wpo_datepicker" name="' . esc_attr( $settings['param_name'] ) . '" class="wpb_vc_param_value wpb-textinput ' .              
							esc_attr( $settings['param_name'] ) . ' ' .              
							esc_attr( $settings['type'] ) . '_field" type="text" value="' . esc_attr( $value ) . '" />
	                </div>';
		}

		/**
		 * auto add footer type in visual composer
		 */
		function edubase_wpo_load_vc_widgets(){ 
			edubase_wpo_includes(  EDUBASE_WPO_THEME_INC_DIR . '/vendors/visualcomposer/shortcodes/class/*.php' );
			edubase_wpo_includes(  EDUBASE_WPO_THEME_INC_DIR . '/vendors/visualcomposer/shortcodes/*.php' );



		}

		add_action('init','edubase_wpo_load_vc_widgets',1);

		function edubase_wpo_get_iconstyles(){
			return  array(
					__( 'Default', 'edubase' ) => '',
					__( 'Outline Default', 'edubase' ) => 'icons-outline icons-default',
					__( 'Outline Primary', 'edubase' ) => 'icons-outline icons-primary',
					__( 'Outline Danger', 'edubase' ) => 'icons-outline icons-danger',
					__( 'Outline Success', 'edubase' ) => 'icons-outline icons-success',
					__( 'Outline Warning', 'edubase' ) => 'icons-outline icons-warning',
					__( 'Outline Info', 'edubase' ) => 'icons-outline icons-info',

					__( 'Inverse Default', 'edubase' )  => 'icons-inverse icons-default',
					__( 'Inverse Primary', 'edubase' )  => 'icons-inverse icons-primary',
					__( 'Inverse Danger', 'edubase' )   => 'icons-inverse icons-danger',
					__( 'Inverse Success', 'edubase' )  => 'icons-inverse icons-success',
					__( 'Inverse Warning', 'edubase' )  => 'icons-inverse icons-warning',
					__( 'Inverse Info', 'edubase' )	 => 'icons-inverse icons-info',

					__( 'Light', 'edubase' )	 => 'icons-light',
					__( 'Dark', 'edubase' )	 => 'icons-darker',

					__( 'Color Default', 'edubase' )	 => 'text-default nostyle',
					__( 'Color Primary', 'edubase' )	 => 'text-primary nostyle',
					__( 'Color Danger', 'edubase' )	 => 'text-danger nostyle',
					__( 'Color Success', 'edubase' )	 => 'text-success nostyle',
					__( 'Color Info', 'edubase' )	 => 'text-info',

			);
		}