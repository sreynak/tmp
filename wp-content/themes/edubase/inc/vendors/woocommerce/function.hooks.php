<?php 
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */

 
	/**
	 * Apply filter to change templates of minibask button following header layout
	 */
	function edubase_wpo_minibasket_template( $template ){
		global $wp_query, $edubase_wpoengine;

		$layout = $edubase_wpoengine->getHeaderLayout($wp_query->get_queried_object_id()) ;

 		if( $layout == 'absolute'){
 			$template = 'mini-cart-button-v2';
 		}	
		return $template; 
	}

	add_filter( 'edubase_wpo_minibasket_template', 'edubase_wpo_minibasket_template' );
	/**
	 * add social share in product detail at bottom
	 */


	function edubase_wpo_woocommerce_product_style_accordion(){
		
	}

	/**
	 * Change style for tab styles
	 */
	function edubase_wpo_woocommerce_single_product_tab_class( $value ){
		return  $value;
	}
	add_filter( 'edubase_wpo_woocommerce_single_product_tab_class', 'edubase_wpo_woocommerce_single_product_tab_class' );


	/**
	 * Change style for accordions styles
	 */
	function edubase_wpo_woocommerce_single_product_accordion_class( $value ){
		return  $value;
	}
	add_filter( 'edubase_wpo_woocommerce_single_product_accordion_class', 'edubase_wpo_woocommerce_single_product_accordion_class' );

	edubase_wpo_woocommerce_product_style_accordion();