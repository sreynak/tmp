<?php 
	$GLOBALS['wpo_teachers_matches'] = array();

	/**
	 * Add meta box fields for course and lesson
	 */
	if( is_admin() ){	
    // Save the Metabox Data

   		function edubase_wpo_save_course_features_meta($post_id, $post) { 
      
		    if( !isset($_POST['post_type']) ){
		       return $post->ID;
		    }
		 
		 		
		      // verify this came from the our screen and with proper authorization,
			// because save_post can be triggered at other times
			if ( $_POST['post_type'] != 'ib_educator_course' ) {
				return $post->ID;
			}

			if ( !isset($_POST['features']) ) {
				return $post->ID;
			}
			$data = array();

			foreach( $_POST['features'] as $item ){
				if( !empty($item) ){
					$data[] = $item;
				}	
			}
			


			// Is the user allowed to edit the post or page?
			if ( !current_user_can( 'edit_post', $post->ID ))
			return $post->ID;

				$course_meta = array();

				$course_meta['_features'] = $data;

				foreach ($course_meta as $key => $value) { // Cycle through the $course_meta array!

				if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
					update_post_meta($post->ID, $key, $value);
				} else { // If the custom field doesn't have a value
					add_post_meta($post->ID, $key, $value);
				}
				if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
			}

	    }

	    add_action('save_post', 'edubase_wpo_save_course_features_meta', 1, 2); // save the custom fields

		function edubase_wpo_add_metabox_postypes(){
			$path = EDUBASE_WPO_THEME_DIR   . '/ibeducator/';
	 
			//Courses setting
			new WPO_MetaBox(array(
				'id'       => 'wpo_postconfig',
				'title'    => __('Courses Options', 'edubase'),
				'types'    => array('ib_educator_course'),
				'priority' => 'high',
				'template' => $path . 'metabox_course.php',
			));
				//Post setting
		 	new WPO_MetaBox(array(
				'id'       => 'wpo_postconfig',
				'title'    => __('Post Configuration' , 'edubase'),
				'types'    => array('ib_educator_lesson'),
				'priority' => 'high',
				'template' =>EDUBASE_WPO_THEME_INC_DIR   . 'metabox/' . 'post.php'
			));
			//Lesson setting
			new WPO_MetaBox(array(
				'id'       => 'wpo_postconfig_other',
				'title'    => __('Lesson Options', 'edubase'),
				'types'    => array('ib_educator_lesson'),
				'priority' => 'high',
				'template' => $path . 'metabox_lesson.php',
			));
		}
		add_action( 'init', 'edubase_wpo_add_metabox_postypes' );
	}

	/**
	 *
	 */
	function edubase_wpo_get_the_course_features(){
		$data = get_post_meta(  get_the_ID(), '_features' );

		if( isset($data[0]) && !empty($data[0]) ){
			return $data[0];
		}
		return array();
	} 	
	
	/**
	 * get teacher id in the course
	 */
	function edubase_wpo_the_teacher_id(){
		return get_post_meta(get_the_id(), '_ib_educator_teacher', true );
	}

	/**
	 * storeed maching teacher by id as global which will be used in next
	 */
 
	function edubase_edubase_wpo_ib_get_teachers_matches( $userId ){
		global $wpo_teachers_matches, $post; 

 		if( !isset($wpo_teachers_matches[$userId]) ){

 			$data =  array('name'=> '', 'link' => '', 'description' => '' );

 		 
 			$user = get_user_by( 'id', $userId );
  		 
			$args = array(
				'post_type' => 'teacher',
				'posts_per_page'=> 1,
				'meta_query' => array(
			        array(
			          'key' => '_relateduser',
			          'value' =>  $userId,
			        )
			     )
			);
			$loop = new WP_Query($args);
			if( $loop->found_posts ){
				while( $loop->have_posts() ){ 
					$loop->the_post();
					$wpo_teachers_matches[$userId] = array('name'=> $user->display_name, 'description'=>edubase_wpo_excerpt(18, '...') , 'link' => get_permalink( get_the_ID() )  );
					break;
				}
				wp_reset_postdata(); 
			}else {
				$wpo_teachers_matches[$userId] = array('name'=> $user->display_name, 'description'=>edubase_wpo_excerpt(18, '...')  , 'link' => get_author_posts_url( $userId )  ); 
			}

		} 
		return $wpo_teachers_matches[$userId];	
	}	

	add_action( 'edubase_edubase_wpo_ib_get_teachers_matches_before', 'edubase_edubase_wpo_ib_get_teachers_matches', 10000 );

	/**
	 * render teacher link 
	 */
	function edubase_wpo_ib_get_teacher(){
		global $post; 

 		$id   = get_post_meta( $post->ID, '_ib_educator_teacher', true );
 		if( $id ){
	 		$link = apply_filters( 'wpo_ibeducator_author_link',  $id );

			return $link;	
		}
		return ;
	}


	/**
	 * get Teacher link by id 
	 */
	add_filter( 'wpo_ibeducator_author_link', 'edubase_wpo_ib_get_teacher_link' );
	function edubase_wpo_ib_get_teacher_link($id){
		global $wpo_teachers_matches; 

		do_action('edubase_edubase_wpo_ib_get_teachers_matches_before', $id );
		
		if( empty($wpo_teachers_matches[$id]['link']) ){
			return '';
		}
		return __('Teacher ', 'edubase') .' <a href="'.$wpo_teachers_matches[$id]['link'].'" title="'.$wpo_teachers_matches[$id]['name'].'">'.$wpo_teachers_matches[$id]['name'].'</a>';			 
	}
 	
 	/**
 	 * Count Students Registered in a course
 	 */
	function edubase_wpo_get_payments_count( $id ) {
		global $wpdb;
		$data = $wpdb->get_results(  $wpdb->prepare("SELECT payment_status, count(1) as num_rows FROM {$this->payments} WHERE  course_id=%s and payment_status='complete' GROUP BY payment_status", $id), OBJECT_K );
		$i = 0;
		foreach( $data as $item ){
			$i += $item->num_rows;
		}
		wp_reset_postdata();
		return $i;
	}

 	/**
 	 * get list of students registered in a courses. 
 	 */
 	function edubase_wpo_get_students_by_course( $id ){  
 		$tables = ib_edu_table_names();

 		global $wpdb;
		$data = $wpdb->get_results( $wpdb->prepare( "SELECT user_id, course_id FROM {$tables['payments']} WHERE  course_id=%s and payment_status='complete'",  $id ), OBJECT_K );
 		
 		$output = array(); 	
 		foreach( $data as $items ){
 			foreach( $items as $item ){
 				$user = get_user_by( 'id', $items->user_id );
 				
 				$output[$items->user_id] = array( 'id' => $items->user_id, 'name' => $user->display_name );
 			}
 		}
 		wp_reset_postdata();
 		return $output;
 	}

 	/**
 	 *
 	 */
 	function edubase_wpo_get_course_lessons_index( $course_id ){
 		
 		$id = get_the_ID();

 		$api = IB_Educator::get_instance();
		$lessons_query = $api->get_lessons($course_id);
		$number = 0;
		if($lessons_query && $lessons_query->have_posts()){
			while ( $lessons_query->have_posts() ) {
				$number++;
				$lessons_query->the_post();
				$student_can_study = ib_edu_student_can_study( get_the_ID() );
		?>
			<div class="lesson<?php if( get_the_ID() == $id ){ ?> active<?php } ?>">
				<span class="number"><?php echo esc_attr($number); ?></span>
				<span class="lesson-icon"><i class="<?php echo edubase_wpo_get_format_icon_class(); ?>"></i></span>
				<a class="title" href="<?php the_permalink() ?>"><?php the_title() ?>
					<?php if($student_can_study){ ?>
		            <i class="zmdi zmdi-check"></i>
		         <?php }else{ ?>
		            <span class="zmdi zmdi-lock-outline"></span>
		         <?php } ?>
				</a>
			</div>
		<?php
			}
			wp_reset_postdata();
		}	 
 	}

 	/**
 	 * Add comment support for course
 	 */
 	function edubase_wpo_ib_add_support_course_comments( $args ){
 		array_push( $args['supports'] , 'comments' );
 		return $args;
 	}
 	add_filter( 'ib_educator_cpt_course', 'edubase_wpo_ib_add_support_course_comments' );

 	/**
 	 * Add comment support for course
 	 */
 	function edubase_wpo_ib_add_support_lesson_formats( $args ){
 		array_push( $args['supports'] , 'post-formats' );
 		return $args;
 	}
 	add_filter( 'ib_educator_cpt_lesson', 'edubase_wpo_ib_add_support_lesson_formats' );


 	function edubase_wpo_ib_update_term_meta( $term_id, $meta_key, $meta_value, $prev_value = '' ) {
		return update_metadata( 'ib_taxonomy_data', $term_id, $meta_key, $meta_value, $prev_value );
	}

	function edubase_wpo_ib_get_term_meta( $term_id, $key, $single = true ) {
		return get_metadata( 'ib_taxonomy_data', $term_id, $key, $single );
	}

	function edubase_wpo_ib_add_term_meta( $term_id, $meta_key, $meta_value, $unique = false ){
		return add_metadata( 'ib_taxonomy_data', $term_id, $meta_key, $meta_value, $unique );
	}
 	/**
 	 * add categories images
 	 */
 	function edubase_wpo_ib_add_form_fields(){  
 	?>
 	<div class="form-field">
		<label><?php _e( 'Thumbnail', 'edubase' ); ?></label>
		<div id="ib_taxonomy_thumbnail" style="float: left; margin-right: 10px;"><img src="<?php echo esc_url( wc_placeholder_img_src() ); ?>" width="60px" height="60px" /></div>
		<div style="line-height: 60px;">
			<input type="hidden" id="ib_taxonomy_thumbnail_id" name="ib_taxonomy_thumbnail_id" />
			<button type="button" class="upload_image_button button"><?php _e( 'Upload/Add image', 'edubase' ); ?></button>
			<button type="button" class="remove_image_button button"><?php _e( 'Remove image', 'edubase' ); ?></button>
		</div>
		<script type="text/javascript">

			// Only show the "remove image" button when needed
			if ( ! jQuery( '#ib_taxonomy_thumbnail_id' ).val() ) {
				jQuery( '.remove_image_button' ).hide();
			}

			// Uploading files
			var file_frame;

			jQuery( document ).on( 'click', '.upload_image_button', function( event ) {

				event.preventDefault();

				// If the media frame already exists, reopen it.
				if ( file_frame ) {
					file_frame.open();
					return;
				}

				// Create the media frame.
				file_frame = wp.media.frames.downloadable_file = wp.media({
					title: '<?php _e( "Choose an image", 'edubase' ); ?>',
					button: {
						text: '<?php _e( "Use image", 'edubase' ); ?>'
					},
					multiple: false
				});

				// When an image is selected, run a callback.
				file_frame.on( 'select', function() {
					var attachment = file_frame.state().get( 'selection' ).first().toJSON();
					var url = attachment.sizes.thumbnail? attachment.sizes.thumbnail.url:attachment.sizes.full.url;
					jQuery( '#ib_taxonomy_thumbnail_id' ).val( attachment.id );
					jQuery( '#ib_taxonomy_thumbnail img' ).attr( 'src',  url );
					jQuery( '.remove_image_button' ).show();
				});

				// Finally, open the modal.
				file_frame.open();
			});

			jQuery( document ).on( 'click', '.remove_image_button', function() {
				jQuery( '#ib_taxonomy_thumbnail img' ).attr( 'src', '<?php echo esc_js( wc_placeholder_img_src() ); ?>' );
				jQuery( '#ib_taxonomy_thumbnail_id' ).val( '' );
				jQuery( '.remove_image_button' ).hide();
				return false;
			});

		</script>
		<div class="clear"></div>
	</div>
 	<?php }

	/**
	 *
	 *
	 */
	function edubase_wpo_ib_edit_form_fields( $term, $taxonomy ) {
		 
		$image 			= '';
		$thumbnail_id 	= absint( edubase_wpo_ib_add_term_meta( $term->term_id, 'thumbnail_id', true ) );
		if ( $thumbnail_id )
			$image = wp_get_attachment_thumb_url( $thumbnail_id );
		else
		{
			$image = "";	
		}
		?>
		<tr class="form-field">
			<th scope="row" valign="top"><label><?php _e( 'Thumbnail', 'edubase' ); ?></label></th>
			<td>
				<div id="ib_taxonomy_thumbnail" style="float: left; margin-right: 10px;"><img src="<?php echo esc_url( $image ); ?>" width="60px" height="60px" /></div>
				<div style="line-height: 60px;">
					<input type="hidden" id="ib_taxonomy_thumbnail_id" name="ib_taxonomy_thumbnail_id" value="<?php echo esc_attr($thumbnail_id); ?>" />
					<button type="button" class="upload_image_button button"><?php _e( 'Upload/Add image', 'edubase' ); ?></button>
					<button type="button" class="remove_image_button button"><?php _e( 'Remove image', 'edubase' ); ?></button>
				</div>
				<script type="text/javascript">

					// Only show the "remove image" button when needed
					if ( '0' === jQuery( '#ib_taxonomy_thumbnail_id' ).val() ) {
						jQuery( '.remove_image_button' ).hide();
					}

					// Uploading files
					var file_frame;

					jQuery( document ).on( 'click', '.upload_image_button', function( event ) {

						event.preventDefault();

						// If the media frame already exists, reopen it.
						if ( file_frame ) {
							file_frame.open();
							return;
						}

						// Create the media frame.
						file_frame = wp.media.frames.downloadable_file = wp.media({
							title: '<?php _e( "Choose an image", 'edubase' ); ?>',
							button: {
								text: '<?php _e( "Use image", 'edubase' ); ?>'
							},
							multiple: false
						});

						// When an image is selected, run a callback.
						file_frame.on( 'select', function() {
							var attachment = file_frame.state().get( 'selection' ).first().toJSON();

		
							var url = attachment.sizes.thumbnail? attachment.sizes.thumbnail.url:attachment.sizes.full.url;

							jQuery( '#ib_taxonomy_thumbnail_id' ).val( attachment.id );
							jQuery( '#ib_taxonomy_thumbnail img' ).attr( 'src', url );
							jQuery( '.remove_image_button' ).show();
						});

						// Finally, open the modal.
						file_frame.open();
					});

					jQuery( document ).on( 'click', '.remove_image_button', function() {
						jQuery( '#ib_taxonomy_thumbnail img' ).attr( 'src', '<?php echo esc_js( "" ); ?>' );
						jQuery( '#ib_taxonomy_thumbnail_id' ).val( '' );
						jQuery( '.remove_image_button' ).hide();
						return false;
					});

				</script>
				<div class="clear"></div>
			</td>
		</tr>
		<?php
	}


 	
 	function edubase_wpo_ib_taxonomy_save_data(  $term_id, $tt_id, $taxonomy ){

 		if ( isset( $_POST['ib_taxonomy_thumbnail_id'] ) && 'ib_educator_category' === $taxonomy ) {
			edubase_wpo_ib_update_term_meta( $term_id, 'thumbnail_id', absint( $_POST['ib_taxonomy_thumbnail_id'] ) );
		}
		
 	}
 	function edubase_wpo_ib_taxonomy_process(){
 		add_action( 'ib_educator_category_add_form_fields', 'edubase_wpo_ib_add_form_fields' , 10 );
 		add_action( 'ib_educator_category_edit_form_fields', 'edubase_wpo_ib_edit_form_fields' ,  10, 2  );
 		add_action( 'created_term',  'edubase_wpo_ib_taxonomy_save_data' , 10, 3 );
		add_action( 'edit_term',  'edubase_wpo_ib_taxonomy_save_data' , 10, 3 );
 	}	
 	add_action('admin_init', 'edubase_wpo_ib_taxonomy_process', 10 );


function edubase_wpo_template_chooser($template)   
{    
 
 $post_type = get_query_var('post_type');   
 if( isset($_GET['s']) && $post_type == 'ib_educator_course' )   
 {
  return locate_template('search-courses.php'); 
 }   
 return $template;   
}
add_filter('template_include', 'edubase_wpo_template_chooser');



if ( !function_exists( 'edubase_wpo_ratings_fix' ) ) {
  function edubase_wpo_ratings_fix($html) {
    $search = plugins_url( '/wp-postratings/images/stars_crystal/' );
    $replace = get_stylesheet_directory_uri() . '/images/ratings/images/stars_crystal/';
    $html = str_replace($search, $replace, $html);
    return $html;
  }
add_filter( 'expand_ratings_template', 'edubase_wpo_ratings_fix', 999, 1 );
}






// Save extra taxonomy fields callback function.
 function edubase_save_extra_taxonomy_fields( $term_id ) {
   if ( isset( $_POST['term_meta'] ) ) {
     $t_id = $term_id;
     $term_meta = get_option( "taxonomy_$t_id" );
     $cat_keys = array_keys( $_POST['term_meta'] );
     foreach ( $cat_keys as $key ) {
       if ( isset ( $_POST['term_meta'][$key] ) ) {
         $term_meta[$key] = $_POST['term_meta'][$key];
       }
     }
     update_option( "taxonomy_$t_id", $term_meta );
   }
   return $term_id;
}

 function edubase_extra_edit_tax_fields($term_obj) {
    $term_id = is_object($term_obj)? $term_obj->term_id:0;
    $default = array(
      'icon'  => '',
      'icon_color' => ''
    );

    if( is_object($term_obj) ){
      $term_meta = get_option( "taxonomy_{$term_id}" );  
      $term_meta = $term_meta? array_merge( $default, $term_meta ):$default;
    }else {
      $term_meta = $default; 
    }
  ?>
    <tr class="form-field">
      <th scope="row" valign="top"><label><?php echo __( 'Icon', 'edubase' ); ?></label></th>
      <td>
        <input name="term_meta[icon]" type="text" value="<?php echo esc_attr($term_meta['icon']) ?>"/>
        <p class="description"><?php echo __( 'This support display icon from material design iconic, Please click <a href="//zavoloklom.github.io/material-design-iconic-font/icons.html">here to see</a> the listand', 'edubase' ); ?></p>
      </td>
    </tr>
    <tr class="form-field">
      <th scope="row" valign="top"><label><?php echo __( 'Icon color', 'edubase' ); ?></label></th>
      <td>
        <input name="term_meta[icon_color]" type="text" value="<?php echo esc_attr($term_meta['icon_color']) ?>"/>
        <p class="description"><?php echo __( 'example: #FF618E', 'edubase' ); ?></p>
      </td>
    </tr>
   <?php
  }

add_action( 'edited_ib_educator_category', 'edubase_save_extra_taxonomy_fields', 10, 2 );
add_action( 'create_ib_educator_category', 'edubase_save_extra_taxonomy_fields', 10, 2 );
add_action( 'ib_educator_category_edit_form_fields', 'edubase_extra_edit_tax_fields', 10, 2 );
add_action( 'ib_educator_category_add_form_fields', 'edubase_extra_edit_tax_fields', 10, 2 );
