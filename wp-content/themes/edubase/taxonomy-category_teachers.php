<?php
/*
*Template Name: Portfolio
*
*/

global $edubase_wpopconfig, $wp_query;
// Get Page Config
$edubase_wpopconfig = $edubase_wpoengine->getPageConfig( $post->ID );
$columns = 2; $i = 0;
?>

<?php get_header( $edubase_wpoengine->getHeaderLayout() );  ?>

<?php if( isset($edubase_wpopconfig['breadcrumb']) && $edubase_wpopconfig['breadcrumb'] ){
    do_action( 'edubase_wpo_layout_breadcrumbs_render' ); 
  } ?>	

<?php do_action( 'edubase_wpo_layout_template_before' ) ; ?>
	<?php while(have_posts()):the_post(); $i++;?>
		<?php if($i%2==1) echo '<div class="row space-60">' ?>
		
		<div class="col-sm-6">
			<div class="teacher-category-wrap">
				<?php get_template_part('templates/content/content','teacher'); ?>
			</div>
		</div>	
		
		<?php if($i%2==0 || $i==$wp_query->found_posts) echo '</div>' ?>	
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
<?php do_action( 'edubase_wpo_layout_template_after' ) ; ?>

<?php get_footer();?>