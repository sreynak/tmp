<?php
/**
 * Theme function
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <opalwordpress@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */

if( !defined('TEXTDOMAIN') ){

	$themename = get_option( 'stylesheet' ); 
	$themename = preg_replace("/\W/", "_", strtolower($themename) );
	define( 'TEXTDOMAIN', $themename );
}

define( 'EDUBASE_WPO_THEME_DIR', get_template_directory() );
define( 'WPO_THEME_DIR', get_template_directory() );


define( 'WPO_THEME_TEMPLATE_DIR', EDUBASE_WPO_THEME_DIR );
define( 'WPO_THEME_INC_DIR', EDUBASE_WPO_THEME_DIR.'/inc/' );
define( 'EDUBASE_WPO_THEME_TEMPLATE_DIR',  EDUBASE_WPO_THEME_DIR . '/templates/' );
define( 'EDUBASE_WPO_THEME_TEMPLATE_DIR_PAGEBUILDER', EDUBASE_WPO_THEME_DIR.'/vc_templates/' );

define( 'EDUBASE_WPO_THEME_INC_DIR', EDUBASE_WPO_THEME_DIR.'/inc/' );
define( 'EDUBASE_WPO_THEME_CSS_DIR', EDUBASE_WPO_THEME_DIR.'/css/' );

define( 'EDUBASE_WPO_THEME_URI', get_template_directory_uri() );
define( 'WPO_FRAMEWORK_ADMIN_STYLE_URI', EDUBASE_WPO_THEME_URI.'/inc/assets/' );
    
define( 'EDUBASE_WPO_THEME_NAME', 'edubase' );
define( 'EDUBASE_WPO_THEME_VERSION', '1.0' );
define( 'WPO_META_MODE_ARRAY', '');

define( 'EDUBASE_WPO_FRAMEWORK_CUSTOMZIME_STYLE_URI', EDUBASE_WPO_THEME_URI.'/css/customize/' );
define( 'EDUBASE_WPO_FRAMEWORK_ADMIN_STYLE_URI', EDUBASE_WPO_THEME_URI.'/inc/assets/' );
define( 'EDUBASE_WPO_FRAMEWORK_ADMIN_IMAGE_URI', EDUBASE_WPO_FRAMEWORK_ADMIN_STYLE_URI.'images/' );
define( 'EDUBASE_WPO_FRAMEWORK_STYLE_URI', EDUBASE_WPO_THEME_URI.'/inc/assets/' ); 
define( 'WPO_FRAMEWORK_ADMIN_IMAGE_URI', EDUBASE_WPO_FRAMEWORK_ADMIN_STYLE_URI.'images/' ); 

/**
 * Define constants storing status enable or disable  installed plugins.
 */
define( 'EDUBASE_WPO_WOOCOMMERCE_ACTIVED', 	   in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );
define( 'EDUBASE_WPO_VISUAL_COMPOSER_ACTIVED', in_array( 'js_composer/js_composer.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ); 
define( 'EDUBASE_WPO_EVENT_ACTIVED', 		   in_array( 'the-events-calendar/the-events-calendar.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );
define( 'EDUBASE_WPO_PLG_FRAMEWORK_ACTIVED',   in_array( 'wpoframework/wpoframework.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );
define( 'EDUBASE_WPO_PUDDYPRESS_ACTIVED', 	   in_array( 'buddypress/bp-loader.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );
define( 'EDUBASE_WPO_BBPRESS_ACTIVED', 	       in_array( 'bbpress/bbpress.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );
define( 'EDUBASE_WPO_IBEDUCATOR_ACTIVED',      in_array('ibeducator/ibeducator.php', apply_filters('active_plugins', get_option('active_plugins') ) ) );

if ( ! isset( $content_width ) ) $content_width = 900;
/* 
 * Localization
 */ 
$lang = EDUBASE_WPO_THEME_DIR . '/languages' ;

/**
 * batch including all files in a path.
 *
 * @param String $path : PATH_DIR/*.php or PATH_DIR with $ifiles not empty
 */
function edubase_wpo_includes( $path, $ifiles=array() ){

    if( !empty($ifiles) ){
         foreach( $ifiles as $key => $file ){
            $file  = $path.'/'.$file; 
            if(is_file($file)){
                require($file);
            }
         }   
    }else {
        $files = glob($path);
        foreach ($files as $key => $file) {
            if(is_file($file)){
                require($file);
            }
        }
    }
}

function edubase_wpo_get_posttype_enable( $post_type){
    $posttypes = array();
    $opts = get_option( 'wpo_themer_posttype' );
    if( !empty($opts) ){
      foreach( $opts as $opt => $key ){
          $posttypes[] = str_replace( 'enable_', '', $opt );
      }  
    }
    return in_array( $post_type, $posttypes );
}

/*
 * Include list of files from Opal Framework.
 */ 
edubase_wpo_includes(  EDUBASE_WPO_THEME_DIR . '/inc/plugins/*.php' );

 
edubase_wpo_includes(  EDUBASE_WPO_THEME_DIR . '/inc/classes/*.php' );

if( is_admin() ) {
   /**
    * Admin Classess Core Frameworks Included
    */ 
    edubase_wpo_includes(  EDUBASE_WPO_THEME_DIR . '/inc/classes/admin/*.php' );
 }


/// include list of functions to process logics of worpdress not support 3rd-plugins.
edubase_wpo_includes(  EDUBASE_WPO_THEME_INC_DIR . '/functions/*.php' );

/// WooCommerce specified functions
if( EDUBASE_WPO_VISUAL_COMPOSER_ACTIVED  ) {
    edubase_wpo_includes(  EDUBASE_WPO_THEME_INC_DIR . '/vendors/visualcomposer/*.php' );
}

/// WooCommerce specified functions
if( EDUBASE_WPO_WOOCOMMERCE_ACTIVED  ) {
    edubase_wpo_includes(  EDUBASE_WPO_THEME_INC_DIR . '/vendors/woocommerce/*.php' );
}


if( EDUBASE_WPO_PUDDYPRESS_ACTIVED  ) {
    edubase_wpo_includes(  EDUBASE_WPO_THEME_INC_DIR . '/vendors/buddypress/*.php' );
}

if( EDUBASE_WPO_IBEDUCATOR_ACTIVED  ) {
     edubase_wpo_includes(  EDUBASE_WPO_THEME_INC_DIR . '/vendors/ibeducator/*.php' );
}

/**
 * Theme Customizer
 */ 
 
edubase_wpo_includes(  EDUBASE_WPO_THEME_INC_DIR . '/customizer/*.php' );
 
/**
 * Init object to start template working
 */ 
$edubase_wpoengine = new Edubase_WPO_TemplateFront();
$protocol = is_ssl() ? 'https:' : 'http:';
$edubase_wpoengine->init();


/**
 * Load Css and Js for Front Page Template
 */
function edubase_wpo_load_frontend_jscss(){

    // add Javascript and CSS

    wp_enqueue_script('edubase-prettyphoto',  EDUBASE_WPO_THEME_URI.'/js/jquery.prettyPhoto.js',array('jquery'),false,true);
    wp_enqueue_script('edubase-owlcaousel',   EDUBASE_WPO_THEME_URI.'/js/owl-carousel/owl.carousel.min.js',array('jquery'),false,true);
    wp_enqueue_script('edubase-main',         EDUBASE_WPO_THEME_URI.'/js/main.js',array('jquery'),false,true);
    wp_enqueue_script('edubase-select-2',     EDUBASE_WPO_THEME_URI.'/js/select2.min.js',array('jquery'),false,true);
    
    wp_enqueue_style('edubase-base-fonticon',    EDUBASE_WPO_THEME_URI.'/css/font-awesome.css' );
    wp_enqueue_style('edubase-prettyPhoto',      EDUBASE_WPO_THEME_URI.'/css/prettyPhoto.css' );
    wp_enqueue_style('edubase-select-2',     EDUBASE_WPO_THEME_URI.'/css/select2.min.css' );

}
add_action( 'wp_enqueue_scripts', 'edubase_wpo_load_frontend_jscss'  );