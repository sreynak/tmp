<div class="widget">
<?php
	if( $relates->have_posts() ): ?>
		<div class="widget-title">
            <h3 class="related-post-title margin-bottom-20 separator_align_left">
                <span><?php esc_html_e( 'Related post', 'clickshop' ); ?></span>
            </h3>
        </div>    
        <div class="related-posts-content">
            <div class="row">
    		<?php
                $class_column = 12/$relate_count;
        		while ( $relates->have_posts() ) : $relates->the_post();
                    ?>
        			<div class="col-sm-<?php echo esc_attr($class_column); ?> col-md-<?php echo esc_attr($class_column); ?> col-lg-<?php echo esc_attr($class_column); ?>">
                        <div class="element-item">
                            <?php if ( has_post_thumbnail()) : ?>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="entry-image image-plus-3 zoom-2">
                                    <?php the_post_thumbnail('full'); ?>
                                </a>
                            <?php endif; ?>
                            <h5 class="entry-title">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h5>
                        </div>
                    </div>
                    <?php
                endwhile; ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
        <?php
    endif;
?>
</div>