<?php global $post; $teacher = Edubase_WPO_Teacher::load( $post ); 
	
	$courses = $teacher->getCourses();
?>

<article id="post-<?php the_ID(); ?>" class="post-teacher">
 
    <div class="row">
      <div class="col-lg-6 col-md-6">
          <div class="teacher-header">
              <?php the_post_thumbnail('thumbnail'); ?>
          </div> 
      </div>
      <div class="col-lg-6 col-md-6">
          <div class="teacher-body">
             <div class="teacher-body-content">
                <h3 class="teacher-name"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                <p class="teacher-position"><?php echo trim($teacher->meta('job')) ?></p>
             </div>  
             <div class="teacher-info text-medium-1">
               <?php echo edubase_wpo_excerpt(18, '...'); ?>
             </div>      
             <div class="bo-social-icons text-left">
                <?php if( $teacher->meta( 'facebook' ) ){ ?>
                <a href="<?php echo esc_url($teacher->meta('facebook')); ?>" class="bo-social-facebook bo-social-white radius-x"><i class="fa fa-facebook"></i></a>
                <?php } ?>
                <?php if( $teacher->meta( 'twitter' ) ){ ?>
                <a href="<?php echo esc_url($teacher->meta('twitter')); ?>" class="bo-social-twitter bo-social-white radius-x"><i class="fa fa-twitter"></i></a>
                <?php } ?>
                <?php if( $teacher->meta( 'linkedin' ) ){ ?>
                <a href="<?php echo esc_url($teacher->meta('linkedin')); ?>" class="bo-social-linkedin bo-social-white radius-x"><i class="fa fa-linkedin"></i></a>
                <?php } ?>
                <?php if( $teacher->meta( 'google' ) ){ ?>
                <a href="<?php echo esc_url($teacher->meta('google')); ?>" class="bo-social-google bo-social-white radius-x"><i class="fa fa-google"></i></a>
                <?php } ?>  
             </div>                          
          </div>                            
      </div>
    </div>

</article>