<?php $thumbsize = isset($thumbsize)? $thumbsize : 'post-thumbnail';?>
<?php
  $post_category = "";
  $categories = get_the_category();
  $separator = ' | ';
  $output = '';
  if($categories){
    foreach($categories as $category) {
      $output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s", 'edubase' ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
    }
  $post_category = trim($output, $separator);
  }      
?>
<article class="post">
    <?php
    if ( has_post_thumbnail() ) {
        ?>
            <figure class="entry-thumb">
                <a href="<?php the_permalink(); ?>" title="" class="entry-image zoom-2">
                    <?php the_post_thumbnail( $thumbsize );?>
                </a>
                <!-- vote    -->
                <?php do_action('wpo_rating') ?>
                 <div class="category-highlight hidden">
                    <?php echo trim($post_category); ?>
                </div>
            </figure>
        <?php
    }
    ?>
    <div class="entry-content">       
        <div class="entry-content-inner clearfix">
            <div class="entry-meta">
                <div class="entry-category hidden">
                    <?php the_category(); ?>
                </div>
                 <div class="entry-create">
                    <span class="entry-date"><?php the_time( 'M d, Y' ); ?></span>
                    <span>/</span>
                    <span class="comment-link"><?php comments_popup_link(__(' 0 comments', 'edubase'), __(' 1 comments', 'edubase'), __(' % comments', 'edubase')); ?></span>
                </div>
            </div>
        </div>
        <?php
            if (get_the_title()) {
            ?>
                <h4 class="entry-title">
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                </h4>
            <?php
        }
        ?>
        <?php
            if (! has_excerpt()) {
                echo "";
            } else {
                ?>
                    <p class="entry-description"><?php echo edubase_wpo_excerpt(22,'...'); ?></p>
                <?php
            }
        ?>
        <div class="readmore"><a class="text-theme text-small fweight-700 text-uppercase" href="<?php the_permalink(); ?>"><?php _e('Read more', 'edubase'); ?></a></div>
    </div>
</article>