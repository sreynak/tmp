<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
$title = $category = $el_class = '';
$size = 'font-size-lg';
$column = 2;
$alignment = 'separator_align_center';
$layout = 'normal';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$arg = array();    
$arg = array( 
    'posts_per_page' => $number, 
    'orderby' => 'date', 
    'order' => 'DESC',
    'post_type' => 'teacher',
    'post_status' => 'publish',
    'taxonomy' => 'category_teachers',
    'term' => $category,
);
$style = 'team-small';
 
$xcolumn = floor( 12/$column );
$class_col = "col-lg-".$xcolumn." col-md-".$xcolumn." col-sm-".$xcolumn." col-xs-12";
$query = new WP_Query( $arg );
$_id = edubase_wpo_makeid();
?>

<div class="widget wpo-teacher-grid <?php echo esc_attr($layout); ?> <?php echo esc_attr($el_class); ?>">
	<?php if( $title ) { ?>
        <h3 class="widget-title visual-title <?php echo esc_attr($size) . ' ' . $alignment; ?>">
           <span><?php echo trim($title); ?></span>
        </h3>
    <?php } ?>

	<div class="widget-content">
		<?php if ( $query->have_posts() ) : ?>
    		<?php if ( $layout=='horizontal' ) : ?>
                <?php 
                    while ( $query->have_posts() ) : $query->the_post();
                        global $post; $teacher = Edubase_WPO_Teacher::load( $post ); 
        				$i = $query->current_post + 1;
        		?>
        		<?php if($i%$column==1) echo '<div class="row space-45">'; ?>
        		
                <div class="team-list">
        			 <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="team-header text-center">
                               <a href="<?php the_permalink() ?>" title="<?php _e( 'History', 'edubase' ); ?>" > <?php the_post_thumbnail(); ?> </a>
                            </div>  
                        </div>
                        <div class="col-lg-6 col-md-6">       
                            <div class="team-body">
                                <div class="team-body-content">
                                    <h3 class="team-name"><a href="<?php the_permalink() ?>" title="<?php _e( 'History', 'edubase' ); ?>" ><?php the_title(); ?></a></h3>
                                    <p><?php echo trim($teacher->meta('job')) ?> / <a href="<?php the_permalink() ?>" title="<?php _e( 'History', 'edubase' ); ?>" ><?php _e( 'History', 'edubase' ); ?></a></p>
                                    <?php if( $showinfo ) : ?>
                                    <div class="teacher-info text-medium-1">
                                       <?php echo edubase_wpo_excerpt(18, '...'); ?>
                                    </div> 
                                    <?php endif; ?>
                                </div>                            
                            </div>  
                        </div>                                    
                    </div>
        		</div>	
        		<?php if($i%$column==0 || $i==($query->found_posts)) echo '</div>'; ?>
        		<?php endwhile; 
        			wp_reset_postdata();
        			
        		?>
            <?php elseif($layout == 'horizontal-v2'): ?>
            <?php 
                while ( $query->have_posts() ) : $query->the_post();
                    global $post; $teacher = Edubase_WPO_Teacher::load( $post ); 
                    $i = $query->current_post + 1;
                ?>
                <?php if($i%$column==1) echo '<div class="row space-30">'; ?>
                <div class="<?php echo esc_attr($class_col); ?>">
                    <?php get_template_part( 'templates/content/content', 'teacher' ) ?> 
                </div>   
                <?php if($i%$column==0 || $i==($query->found_posts)) echo '</div>'; ?>
                <?php endwhile; 
                wp_reset_postdata();  
            ?> 

            <?php elseif($layout == 'carousel'): ?>
                <div id="carousel-<?php echo esc_attr($_id); ?>" class="space-margin-0 text-center owl-carousel-play" data-ride="owlcarousel">
                    <div class="owl-carousel " data-slide="<?php echo esc_attr($column); ?>" data-pagination="false" data-navigation="true">              
                        <?php 
                        while ( $query->have_posts() ) : $query->the_post();
                            global $post; $teacher = Edubase_WPO_Teacher::load( $post ); 
                            $i = $query->current_post + 1;
                        ?>
                         <div class="team-v1 <?php echo esc_attr($style); ?>">
                            <div class="team-header text-center">
                               <a href="<?php the_permalink() ?>" title="<?php _e( 'History', 'edubase' ); ?>" > <?php the_post_thumbnail('thumbnail'); ?> </a>
                            </div>     
                            <div class="team-body">
                                <div class="team-body-content">
                                    <h3 class="team-name"><a href="<?php the_permalink() ?>" title="<?php _e( 'History', 'edubase' ); ?>" ><?php the_title(); ?></a></h3>
                                    <p class="job"><?php echo trim($teacher->meta('job')) ?></p>
                                    <?php if( $showinfo ) : ?>
                                    <div class="teacher-info text-medium-1">
                                       <?php echo edubase_wpo_excerpt(18, '...'); ?>
                                    </div> 
                                    <?php endif; ?>
                                    <div class="read-more"><a href="<?php the_permalink() ?>" title="<?php _e( 'View profile', 'edubase' ); ?>" ><?php _e( 'View profile', 'edubase' ); ?></a></div>
                                </div>                            
                            </div>                                  
                        </div>
                        <?php endwhile; 
                            wp_reset_postdata();
                        ?>
                    </div>   
                    <?php if( $number  > $column) { ?>
                        <div class="owl-control">
                            <a class="left carousel-control carousel-md radius-x" href="#post-slide-<?php the_ID(); ?>" data-slide="prev">
                                    <span class="zmdi zmdi-arrow-back zmd-fw"></span>
                            </a>
                            <a class="right carousel-control carousel-md radius-x" href="#post-slide-<?php the_ID(); ?>" data-slide="next">
                                    <span class="zmdi zmdi-arrow-forward zmd-fw"></span>
                            </a>
                        </div>  
                    <?php } ?> 
                 </div>      
            <?php else : ?>
                      <?php 
                            while ( $query->have_posts() ) : $query->the_post();
                                global $post; $teacher = Edubase_WPO_Teacher::load( $post ); 
                                $i = $query->current_post + 1;
                        ?>
                        <?php if($i%$column==1) echo '<div class="row space-30">'; ?>
                        
                        <div class="<?php echo esc_attr($class_col); ?>">
                             <div class="team-v1 <?php echo esc_attr($style); ?>">
                                <div class="team-header text-center">
                                   <a href="<?php the_permalink() ?>" title="<?php _e( 'History', 'edubase' ); ?>" > <?php the_post_thumbnail('thumbnail'); ?> </a>
                                </div>     
                                <div class="team-body">
                                    <div class="team-body-content">
                                        <h3 class="team-name"><a href="<?php the_permalink() ?>" title="<?php _e( 'History', 'edubase' ); ?>" ><?php the_title(); ?></a></h3>
                                        <p class="team-job"><?php echo trim($teacher->meta('job')) ?></p>
                                        <?php if( $showinfo ) : ?>
                                        <div class="teacher-info text-medium-1">
                                           <?php echo edubase_wpo_excerpt(18, '...'); ?>
                                        </div> 
                                        <?php endif; ?>
                                    </div>                            
                                </div>                                  
                            </div>
                        </div>  
                        <?php if($i%$column==0 || $i==($query->found_posts)) echo '</div>'; ?>
                        <?php endwhile; 
                            wp_reset_postdata();
                        ?>
        <?php endif; ?>

      <?php endif; ?>

	</div>
</div>