<?php
$classes = apply_filters( 'ib_educator_course_classes', array( 'course' ) );
$classes[] = 'course-item';
$categories = '';
$separator = ', ';
$title = get_the_title();
$link = get_the_permalink();
$terms = get_the_terms(get_the_id(), 'ib_educator_category' );
$options = get_post_meta(get_the_id(), 'wpo_postconfig', true );
$post_id = get_the_id();
if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
   foreach ( $terms as $term ) {
     $categories .=  $term->name . $separator;  
   }
}
$categories = trim($categories, $separator);
$educator =  IB_Educator::get_instance();
$lesson = $educator->get_num_lessons(get_the_id());
?>
<article id="course-<?php the_ID(); ?>" class="<?php echo esc_attr( implode( ' ', $classes ) ); ?>"> 
	<?php if ( has_post_thumbnail() ) : ?>
	<div class="course-image">
		<a href="<?php echo esc_url($link) ?>"><?php the_post_thumbnail( 'course-image' ); ?></a>
		<div class="ib-edu-course-price">
			<?php if(ib_edu_get_course_price(get_the_ID()) == 0){ ?>
				 <span class="label-free"><?php _e('Free', 'edubase') ?></span>
			<?php }else{ ?>		
				<span><?php echo ib_edu_format_price( ib_edu_get_course_price( get_the_ID() ) ); ?></span>
			<?php } ?>	
		</div>
	</div>
	<?php endif; ?>
	<div class="course-inner">
		<div class="course-header">
			<h4><a href="<?php echo esc_url($link) ?>"><?php echo esc_html($title); ?></a></h4>
			<div class="author"><span><?php echo edubase_wpo_ib_get_teacher(); ?></span></div>
			<div class="description"><?php echo edubase_wpo_excerpt(14, '...'); ?></div>
		</div>
		<div class="course-meta">
			<div class="left">
				<?php if(isset($options['duration']) && $options['duration']){ ?>
					<span class="duration"><i class="uicon icon-duration"></i> 
						<span class="val"><?php echo esc_html($options['duration']) ?> </span>
					</span>				
				<?php } ?>	
				<span class="lesson"><i class="uicon icon-lesson"></i><?php echo esc_html($lesson); ?> <?php _e(' Lessons', 'edubase');  ?></span>			
			</div>
			<div class="right"> 
			 	<div class="course-rating">
			 		<?php echo do_shortcode('[ratings id="'.$post_id.'" results="true"]'); ?>
			 	</div>	
			</div> 	
		</div>
	</div>	
</article>