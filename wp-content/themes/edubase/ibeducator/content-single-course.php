<?php
global $post;
$author_id = $post->post_author;
$teacher_id = edubase_wpo_the_teacher_id();
$api = IB_Educator::get_instance();
$user_id = get_current_user_id();
$course_id = get_the_ID();
$categories = get_the_term_list( get_the_ID(), 'ib_educator_category', '', __( ', ', 'ibeducator' ) );

$students = edubase_wpo_get_students_by_course( $post->ID );

$options = get_post_meta(get_the_id(), 'wpo_postconfig', true );

$membership_access = Edr_Memberships::get_instance()->membership_can_access( $course_id, $user_id );

if ( $membership_access ) {
	$register_url = ib_edu_get_endpoint_url( 'edu-action', 'join', get_permalink( $course_id ) );
} else {
	$register_url = ib_edu_get_endpoint_url( 'edu-course', $course_id, get_permalink( ib_edu_page_id( 'payment' ) ) );
}

?>

<article id="course-<?php the_ID(); ?>" <?php post_class( 'ib-edu-course-single' ); ?>>
	<div class="course-content entry-content space-top-50">

		<div class="col-md-9">
			<div class="course-thumbnail">
				<?php the_post_thumbnail('full'); ?>
			</div>
			
			<div class="widget course-content entry-content space-top-40">
				<h3 class="widget-title font-size-md separator_align_left"><span><?php _e('About The Courses', 'edubase') ?></span></h3>
				<?php the_content(); ?>
			</div>
			
			<?php  $data = edubase_wpo_get_the_course_features();?>

			<?php if( !empty($data) ) : ?>
				<div class="course-features row widget space-30">
					<div class="col-xs-12">
						<h3 class="widget-title font-size-md separator_align_left"><span><?php _e( 'Key Features', 'edubase' ); ?></span></h3>
						<div class="widget-content">
							<?php $i=0; foreach( $data as $item ) : $i++; ?>
								<?php if($i%2==1) echo '<div class="row space-20">'; ?>
									<div class="col-md-6 col-sm-12 col-xs-12">
										<div class="fitem">
											<span class="icon zmdi zmdi-star-outline"></span> <?php echo esc_html($item); ?> 
										</div>	
									</div>
								<?php if($i%2==0 || $i==count($data)) echo '</div>'; ?>	
							<?php endforeach; ?>
						</div>	
					</div>
				</div>	
			<?php endif; ?>	

			<div class="row">
				<div class="col-sm-12">
					<?php
					$api = IB_Educator::get_instance();
					$lessons_query = $api->get_lessons( $course_id );
					?>
					<?php if ( $lessons_query && $lessons_query->have_posts() ) : ?>
						<section class="widget ib-edu-lessons">
							<h3 class="widget-title font-size-md separator_align_left"><span><?php _e( 'Lessons', 'ibeducator' ); ?></span></h3>
							<div class="less-content <?php if($lessons_query->found_posts > 4) echo 'content-hidden' ?>">
								<?php
								while ( $lessons_query->have_posts() ) {
									$lessons_query->the_post();
									Edr_View::template_part( 'content', 'lesson' );
								}
								wp_reset_postdata();
								?>
							</div>
							<?php if($lessons_query->found_posts > 4){ ?>
								<div class="btn-view-lesson"><a><?php _e('View more lessons', 'edubase') ?></a></div>
							<?php } ?>	
						</section>
					<?php endif; ?>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 space-padding-top-30">
					<?php comments_template(); ?>
				</div>
			</div>
		</div>

		<div class="col-md-3">	
			<div class="course-information">
				<?php
				$access_status = '';
				if ( $user_id ) {
					$access_status = $api->get_access_status( $course_id, $user_id );
				}

				switch ( $access_status ) {
					case 'inprogress':
					echo '<div class="ib-edu-message info">' . __( 'You are registered for this course.', 'ibeducator' ) . '</div>';
					break;
					case 'pending_entry':
					echo '<div class="ib-edu-message info">' . __( 'Your registration for this course is pending.', 'ibeducator' ) . '</div>';
					break;
					case 'pending_payment':
					echo '<div class="ib-edu-message info">' . __( 'Your payment for this course is pending.', 'ibeducator' ) . '</div>';
					break;

							default: //---------------

							if ( 'closed' == ib_edu_registration( $course_id ) ) {
								break;
							}

							$output = apply_filters( 'ib_educator_course_price_widget', null, $membership_access, $course_id, $user_id );
							
							if ( null !== $output ) {
								return $output;
							}

							if ( $membership_access ) {
								$register_url = ib_edu_get_endpoint_url( 'edu-action', 'join', get_permalink( $course_id ) );
								$output .= '<form class="space-20" action="' . esc_url( $register_url ) . '" method="post">';
								$output .= '<input type="hidden" name="_wpnonce" value="' . wp_create_nonce( 'ib_educator_join' ) . '">';
								$output .= '<input type="submit" class="ib-edu-button" value="' . __( 'Join', 'ibeducator' ) . '">';
								$output .= '</form>';
							} else {
								$output .= '<p class="space-30 text-center"><a href="' . esc_url( $register_url ) . '" class="ib-edu-button btn btn btn-success radius-4x">' . __( 'Apply to course now', 'ibeducator' ) . '</a></p>';
							}

							echo trim( $output );
							break;
						}

						// Output error messages.
						$errors = ib_edu_message( 'course_join_errors' );

						if ( $errors ) {
							$messages = $errors->get_error_messages();

							foreach ( $messages as $message ) {
								echo '<div class="ib-edu-message error">' . $message . '</div>';
							}
						}

						?>

						<div class="course-information-inner">
							<?php 
							if($categories){
								echo '<p class="space-5"><span class="category">' . $categories . '</span></p>';
							}
							?>

							<?php do_action( 'ib_educator_before_course_title' ); ?>
							<h1 class="course-title entry-title"><?php the_title(); ?></h1>
							<?php do_action( 'ib_educator_after_course_title' ); ?>
							<div class="course-rating">
								<?php if(function_exists('the_ratings'))  the_ratings(); ?>
							</div>
							<?php
							$output = '<ul class="list">';
							$price = ib_edu_get_course_price( $course_id );
							$price = ( 0 == $price ) ? __( 'Free', 'ibeducator' ) : ib_edu_format_course_price( $price );
							$register_url = ib_edu_get_endpoint_url( 'edu-course', $course_id, get_permalink( ib_edu_page_id( 'payment' ) ) );
							$output .= '<li class="price"><i class="uicon icon-price"></i><span class="lab">' .__('Price: ', 'edubase') . '</span> <span class="val">' . $price . '</span></li>';
							if(isset($options['duration']) && $options['duration']){
								$output .= '<li class="price"><i class="uicon icon-duration"></i><span class="lab">' .__('Duration: ', 'edubase') . '</span> <span class="val">' . $options['duration'] . '</span></li>';
							}
							if(isset($options['is_certificates'])){
								$output .= '<li class="price"><i class="uicon icon-certificates"></i><span class="lab">' .__('Certificates: ', 'edubase') . '</span> <span class="val">' . ( $options['is_certificates'] ? __('Yes', 'edubase') : __('No', 'edubase') ) . '</span></li>';
							}

							$output .= '<li class="lesson"><i class="uicon icon-students"></i><span class="lab">'.__('Students: ', 'edubase') . '</span> <span class="val">' . count($students) .'</span></li>';

							$output .= '<li class="lesson"><i class="uicon icon-lesson"></i><span class="lab">'.__('Lesson: ', 'edubase') . '</span> <span class="val">' .  $api->get_num_lessons(get_the_id()) .'</span></li>';

							$output .= '</ul>';

							echo trim($output);
							?>
						</div>
						<?php if(  $teacher_id ){ ?> 
						<div class="widget course-teacher-information">
							<h3 class="widget-title visual-title font-size-sm separator_align_left">
								<span><?php _e('Teachers', 'edubase') ?></span>
							</h3>
							<div class="widget-content">
								<?php 
								$data = edubase_edubase_wpo_ib_get_teachers_matches( $teacher_id );
								?>

								<?php if( !empty($data) ){  $user = get_user_by( 'id', $teacher_id ); ?> 
								
								<div class="teacher-header">
									<div class="teacher-thumbnail">
										<?php echo get_avatar( $teacher_id, 32 ); ?>
									</div>
									<div class="teacher-info">
										<h3 class="teacher-name"><a href="<?php echo esc_html( $data['link'] ); ?>"><?php echo esc_html( $data['name'] ); ?></a></h3>
										<p class="teacher-position"><a href="<?php echo esc_html( $data['link'] ); ?>"><?php _e( 'History', 'edubase'); ?></a></p>
									</div> 
								</div> 
								
								<div class="teacher-body">
									<div class="clearfix"></div> 
									<div class="teacher-info text-normal">
										<?php echo esc_html( $data['description'] ); ?>
									</div>                     
								</div>  
								<?php } ?> 
							</div>
						</div>	
					</div>	
					<?php } ?>	

					<?php if( count($students) ) : ?>
						<div class="widget course-teacher-information course-students">
							<h3 class="widget-title visual-title font-size-sm separator_align_left">
								<span><?php _e('Lastest Students', 'edubase') ?></span>
							</h3>
							<div class="widget-content">
								<?php $i=0; foreach( $students as $student ): $i++; ?>
									<?php if($i%3==1) echo '<div class="row space-10">'; ?>
									<div class="student-item">
										<?php echo get_avatar( $student['id'], 50 ); ?>
									</div>	
									<?php if($i%3==0 || $i==count($students)) echo '</div>'; ?>
								<?php endforeach; ?>
							</div>	
						</div>	
					<?php endif; ?>
				</div>	
			</div>
		</article>





		

