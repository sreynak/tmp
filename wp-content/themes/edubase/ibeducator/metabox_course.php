<p class="wpo_section is_featured">
  <?php
      $_is_featured = array('id'=>'is_featured', 'title'=>__('Is Featured Course', 'edubase') );
      $mb->getCheckboxElement($_is_featured); 
  ?>
</p>

<p class="wpo_section is_certificates">
  <?php
      $_is_certificates = array(
          'id'=>'is_certificates',
          'title'=>__('Is certificates Course', 'edubase'),
          'data' => array(
            array('id' => '0', 'name' => __('No','edubase')),
            array('id' => '1', 'name' => __('Yes','edubase')),
          ),
          'default' => '1'
          );
      $mb->getSelectElement($_is_certificates); 
  ?>
</p>
 

<p class="wpo_section duration">
  <?php
      $_duration = array('id'=>'duration', 'title'=>__('Duration Time', 'edubase'), 'des'=>'' );
      $mb->addTextElement( $_duration );
  ?>
</p>


<div class="fatures-panel action-panel">

    <div class="action-heading">  
       
        <h3><?php _e( 'Features', 'edubase' ); ?></h3>  <div class="action-button addnew"><?php _e( 'Add Item', 'wpothemer' );?></div> 
    </div>    
    <?php
 
     global $post; 
     $data = get_post_meta( $post->ID, '_features' );

     if( empty($data) ){
        $data = array(0=>array(0=> "") );
     }
     if($data){
        $data = $data[0]; 
    ?> 
    <?php foreach(  $data  as  $key => $item ){

    ?>

    <div class="fatures-item action-item">
        <div class="label"><?php echo trim($key+1); ?></div>
        <div class="inner wpo_section "> 
            <label for="teacher_linkedin"><?php _e( 'Feature', 'wpothemer' );?></label>
            <input type="text" name="features[]"   value="<?php echo  $item; ?>" style="width:70%"/>
           
        </div>
        <div class="action-button remove"><?php _e( 'Remove', 'wpothemer' );?></div>
    </div>

    <?php } ?>
 
    <?php } ?>
     <p><em><?php _e( '','wpothemer' ); ?></em></p>
</div>

<script>

    jQuery(document).ready( function($){
        $('.action-panel .addnew').click( function() {
             var $p = $(this).parent();
            $item = $('.action-item',$p.parent() ).first().clone();
            $p.parent().append( $item );
            $('input , textarea', $item ).val( '' );
            $('.action-item',$p.parent() ).each( function (i) {
                $('.label',this).html(i+1);
            } );
        } );

         $('.action-panel').delegate( '.remove', 'click',  function() {
            var $p = $(this).parent();  
            var $pp = $p.parent();
            if( $( '.action-item', $pp ).length  > 1 ){
                if( confirm("<?php _e('Are you sure to delete this?','wpothemer'); ?>") ){
                    $p.remove();
                    $('.action-item',$pp ).each( function (i) {
                        $('.label',this).html(i+1);
                    } );
                }
            }else {
                $('input , textarea', $p ).val( '' );
            }
        } );
    } );
</script>