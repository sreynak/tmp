<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
global $edubase_wpopconfig, $post;
 
$edubase_wpopconfig  = edubase_wpo_teacher_page_config();

get_header( edubase_wpo_theme_options('headerlayout', '') ); 

$teacher = Edubase_WPO_Teacher::load( $post );
if($teacher->meta( 'relateduser' )){
	$author_id = $teacher->meta( 'relateduser' );
}
//echo $post->post_author;die();
?>
	
<div class="teacher-heading space-padding-tb-50 text-center space-30">
	<div class="container">
		<?php  if(has_post_thumbnail()){ ?>
		   <div class="entry-thumb text-center radius-x">
			 <?php echo get_avatar( $author_id, 200 ); ?>
	      </div> 	
	      <div class="entry-name text-center space-padding-top-20">
	         <h1 class="entry-title text-white text-medium text-uppercase space-margin-0"> <?php the_title(); ?> </h1>
	         <p class="text-white"><?php echo trim($teacher->meta('job')) ?></p>
	      </div>   
	       
	      <?php if(isset($author_id) && $author_id){ ?>
		      <div class="bp-link">
		      	<a class="btn radius-4x btn-inverse-light" href="<?php echo bp_core_get_user_domain( $author_id ) ?>"><?php echo _e('Visit my social page', 'edubase') ?></a>
		      </div>	
		   <?php } ?>   
		<?php } ?>
	</div>	
</div>

<?php do_action( 'edubase_wpo_layout_template_before' ) ; ?>

	<?php while(have_posts()):the_post(); ?>
		<div class="format-teacher">
			<?php global $post; 
			$courses = $teacher->getCourses();
    	?>
	   <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		   <?php if(is_single() ) { ?>
	      <div class="post-container space-padding-top-40">
		      <div class="widget">
			      <h3 class="widget-title visual-title font-size-md separator_align_center">
			      	<span><?php _e('Biography', 'edubase') ?></span>
			      </h3>
		      </div>
        	 	<div class="row">  
					<div class="col-sm-4">
						<div class="teacher-information">
							<div class="widget space-0">
						      <h3 class="widget-title visual-title font-size-sm separator_align_left">
						      	<span><?php _e('Detail', 'edubase') ?></span>
						      </h3>
						      <div class="widget-content">
									<ul class="lists space-0">
					            	<li><span class="lab"><?php _e('Name: ', 'edubase') ?></span> <span class="val"><?php the_title(); ?></span> </li>
					            	<?php if( $teacher->meta( 'specializedin' ) ){ ?>
					            		<li><span class="lab"><?php _e('Specializedin: ', 'edubase') ?></span><span class="val"><?php echo esc_html($teacher->meta( 'specializedin' )); ?></span></li>
					            	<?php } ?>
					            	<?php if( $teacher->meta( 'experience' ) ){ ?>
					            	<li><span class="lab"><?php _e('Experience: ', 'edubase') ?></span><span class="val"><?php echo esc_html($teacher->meta( 'experience' )); ?></span></li>
					            	<?php } ?>
					            	<?php if( $teacher->meta( 'email' ) ){ ?>
					            	<li><span class="lab"><?php _e('Email: ', 'edubase') ?></span><span class="val"><?php echo esc_html($teacher->meta( 'email' )); ?></span></li>
					            	<?php } ?>
					            	<?php if( $teacher->meta( 'link' ) ){ ?>
					            	<li><span class="lab"><?php _e('Link: ', 'edubase') ?></span><span class="val"><a href="<?php echo esc_url($teacher->meta( 'link' )); ?>"><?php echo esc_url($teacher->meta( 'link' )); ?></a></span></li>
					            	<?php } ?>
					            	<?php if( $teacher->meta( 'phone' ) ){ ?>
					            	<li><span class="lab"><?php _e('Phone: ', 'edubase') ?></span><span class="val"><?php echo esc_html($teacher->meta( 'phone' )); ?></span></li>
					            	<?php } ?>
					            	<?php if(function_exists('the_ratings')) { ?>
					            		<li class="rating"><?php the_ratings(); ?></li>
					            	<?php } ?>
					            </ul> 
				            </div>
				           	
								<div class="bo-social-icons text-left">
				           		<?php if( $teacher->meta( 'facebook' ) ){ ?>
				           		<a href="<?php echo esc_url($teacher->meta('facebook')); ?>" class="bo-social-facebook bo-social-white radius-x"><i class="fa fa-facebook"></i></a>
				           		<?php } ?>
				           		<?php if( $teacher->meta( 'twitter' ) ){ ?>
				           		<a href="<?php echo esc_url($teacher->meta('twitter')); ?>" class="bo-social-twitter bo-social-white radius-x"><i class="fa fa-twitter"></i></a>
				           		<?php } ?>
				           		<?php if( $teacher->meta( 'linkedin' ) ){ ?>
				           		<a href="<?php echo esc_url($teacher->meta('linkedin')); ?>" class="bo-social-linkedin bo-social-white radius-x"><i class="fa fa-linkedin"></i></a>
				           		<?php } ?>
				           		<?php if( $teacher->meta( 'google' ) ){ ?>
				           		<a href="<?php echo esc_url($teacher->meta('google')); ?>" class="bo-social-google bo-social-white radius-x"><i class="fa fa-google"></i></a>
				           		<?php } ?>  
					         </div>
					      </div> 
				      </div>   
					</div>

					<div class="col-sm-8">
						<div class="entry-content">
	                <?php
	                    the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'edubase' ) );
	                    wp_link_pages( array(
	                        'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'edubase' ) . '</span>',
	                        'after'       => '</div>',
	                        'link_before' => '<span>',
	                        'link_after'  => '</span>',
	                    ) );
	                ?>
	            	</div><!-- .entry-content -->  
					</div>

				</div>

				<div class="skills-wrapper space-top-40">
					<div class="row">
						<div class="col-sm-6">
								<?php the_post_thumbnail('full');  ?>
						</div>	

						<div class="col-sm-6">
							<div class="skills-inner">
								<?php $skills = get_post_meta( $post->ID, '_skills' ); ?>	
								<?php 
									if( isset($skills[0])  && isset($skills[0]['volume']) ) { 
									$skills = $skills[0];	 
								?>

			 					<div class="teacher-skills widget">
			 						
			 						<h3 class="widget-title visual-title font-size-md separator_align_left">
										<span><?php _e('Skills', 'edubase' ); ?></span>
									</h3>
			 						
			 						<div class="widget-content">
			 							<?php foreach( $skills['volume'] as $key => $value ){ 
			 								if( !empty( $value) ){
			 							?>
				 							<div class="vc_progress_bar">
					 							<div class="vc_single_bar custom">
					 								<small class="vc_label"> <span class="vc_label_units"><?php echo trim($value); ?>%</span><?php  echo trim($skills['label'][$key]) ?></small>
					 								<span data-value="90" data-percentage-value="<?php echo trim($value); ?>" class="vc_bar" style="width: <?php echo trim($value); ?>%;"></span>
					 							</div>	
				 							</div>
			 							<?php }
			 							} ?>
			 						</div>
			 					</div>
			 					<?php } ?>
			 				</div>	
	 					</div>

	 				</div>	
 				</div>	
 				<div class="clearfix"></div>

 					<?php $data = get_post_meta( $post->ID, '_education' ); ?>	
					<?php 
						if( isset($data[0])  && isset($data[0]['time']) ) { 
						$data = $data[0];	 
					?>

 					<div class="teacher-education widget space-top-80">
 						<h3 class="widget-title visual-title font-size-md separator_align_center">
 							<span><?php _e('Education & Training', 'edubase' ); ?></span>
 						</h3>
 						<div class="widget-content">
	 						<ul class="posts-timeline">
	 							<?php $i=0; foreach( $data['time'] as $key => $value ){ 
	 								if( !empty( $value) ){
	 									$i++;
	 							?>
	 							
		 							<li  class="entry-timeline">
		 							 	 <div class="hentry">
		 							 	 		<div class="node"></div>
		 							 	 		<div class="entry-created space-padding-top-10"><span class="radius-2x bg-success"><?php echo trim($value); ?></span></div>
								 	 			 <div class="hentry-box">
								 	 			 	<h4><?php echo trim($data['topic'][$key]) ?></h4>
								 	 			 	<h5><?php echo trim($data['name'][$key]) ?></h5>
				 							 	 	<p>
				 							 	 		<?php echo trim($data['info'][$key]) ?>
				 							 	 	</p>	
				 							 	 </div>		
		 							 	 </div>	
		 							</li>	
		 							
	 							<?php
	 								}
	 							} 
	 							?>
	 						</ul>
	 					</div>	
 					</div>
 					<?php } ?>
 
			   <?php } ?>
			<?php the_tags( '<footer class="entry-meta"><span class="tag-links"><span>'.__('Tags:', 'edubase').' </span>', ', ', '</span></footer>' ); ?>
			    
		</article>   

	</div>
	<?php endwhile; ?>
	
<?php do_action( 'edubase_wpo_layout_template_after' ) ; ?>

   <div class="teacher-courses">
	   <?php  get_template_part( 'ibeducator/related_by_teacher' ); ?>
   </div> 


<?php get_footer(); ?>