<?php
/**
 * List View Single Event
 * This file contains one event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-event.php
 *
 * @package TribeEventsCalendar
 * @since  3.0
 * @author Modern Tribe Inc.
 *
 */

if ( !defined('ABSPATH') ) { die('-1'); }

global $post;
$room = "";
$postconfig = get_post_meta( get_the_ID(),'wpo_postconfig', true );
if( isset( $postconfig['event_room'] ) && !empty( $postconfig['event_room'] )){
   $room = $postconfig['event_room']; 
} 

$featured_image = tribe_event_featured_image( null, 'post-thumbnail' );
$start = strtotime($post->EventStartDate);

$day = date('d', $start);
$month = date('M', $start);

?>
<div class="wpo-event-inner">
	<div class="small-event-header clearfix <?php if(empty($featured_image)) echo 'no-image' ?>">
		<div class="event-top bg-white">
         <div class="event-meta space-padding-tb-20">
            <div class="meta-left">
               <p class="day"><?php echo esc_attr( $day ); ?></p>
               <p class="month"><?php echo esc_attr( $month ); ?></p>
            </div>
            <div class="meta-right">
               <span class="event-time">
                  <i class="fa fa-clock-o"></i>
                  <?php
                      echo tribe_get_start_date(get_the_ID(), false, 'H:i');
                  ?>
                  <span>-</span>
                  <?php
                      if(tribe_get_end_date(get_the_ID(), false, 'dMY') == tribe_get_start_date(get_the_ID(), false, 'dMY')){
                          echo tribe_get_end_date(get_the_ID(), false, 'H:i');
                      }else{
                          echo tribe_get_end_date(get_the_ID(), false, 'M, d H:i');
                      }
                  ?>
               </span>
               <span class="event-address">
                  <?php echo ($room ? '<span class="tribe-events-address"><i class="fa fa-map-marker"></i>' . $room . '</span>' : ''); ?>
               </span>
               <div class="clearfix"></div>
               <h4 class="event-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4> 
            </div>    
         </div>
		</div>
		<div class="clearfix"></div>
		
      <div class="image">
         <?php echo trim( $featured_image ); ?>
         <div class="excerpt"><?php echo edubase_wpo_excerpt(25, '...'); ?></div>
      </div>

		<div class="tribe-events-event-meta-wrapper">
			<?php do_action( 'tribe_events_before_the_meta' ); ?>
				<div class="tribe-events-event-meta">
					<?php
						$start = strtotime($post->EventStartDate);
						$end = strtotime($post->EventEndDate);
						$day = date('d', $start);
						$month = date('M', $start);

						$stime = date(get_option('time_format'), $start);
						$etime = date(get_option('time_format'), $end);
					?>
					
				</div> <!-- .tribe-events-event-meta -->
			<?php do_action( 'tribe_events_after_the_meta' ); ?>
		</div>

	</div>

	<div class="tribe-events-event-details tribe-clearfix <?php if(empty($featured_image)) echo 'no-image' ?>">
		<!-- Event Content -->
		<?php do_action( 'tribe_events_before_the_content' ); ?>
		<div class="tribe-events-list-photo-description tribe-events-content entry-summary description hidden">
			<?php the_excerpt(); ?>
		</div>
		<?php do_action( 'tribe_events_after_the_content' ) ?>

	</div><!-- /.tribe-events-event-details -->
</div>	
