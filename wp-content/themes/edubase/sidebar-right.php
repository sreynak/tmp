<?php  
	global $edubase_wpopconfig;
?>
<?php if($edubase_wpopconfig['right-sidebar']['show']){ 
	$pos = empty($edubase_wpopconfig['right-sidebar']) ?edubase_wpo_theme_options('right-sidebar'): $edubase_wpopconfig['right-sidebar']['widget'];
?>
	<div class="<?php echo esc_attr($edubase_wpopconfig['right-sidebar']['class']); ?>">
		<div class="wpo-sidebar wpo-sidebar-right">
			<div class="sidebar-inner">
				<?php dynamic_sidebar( $pos ); ?>
			</div>
		</div>
	</div>
<?php } ?>