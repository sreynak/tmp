<?php
/*
*Template Name: Gallery
*
*/
global $edubase_wpopconfig;
// Get Page Config
$edubase_wpopconfig = $edubase_wpoengine->getPageConfig( $post->ID );
?>

  <?php get_header( $edubase_wpoengine->getHeaderLayout() );  ?>
  <?php if( isset($edubase_wpopconfig['breadcrumb']) && $edubase_wpopconfig['breadcrumb'] ){
    do_action( 'edubase_wpo_layout_breadcrumbs_render' ); 
  } ?>	

  <?php do_action( 'edubase_wpo_layout_template_before' ) ; ?>
        <div  class="clearfix gallery-page">
            <?php get_template_part('contents-gallery');?>
        </div>
 <?php do_action( 'edubase_wpo_layout_template_after' ) ; ?>

<?php get_footer(); ?>