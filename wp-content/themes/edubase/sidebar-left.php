<?php 
global $edubase_wpopconfig;  
?> 
<?php 
if($edubase_wpopconfig['left-sidebar']['show']){ 
	$pos = empty($edubase_wpopconfig['left-sidebar']) ?edubase_wpo_theme_options('left-sidebar'): $edubase_wpopconfig['left-sidebar']['widget'];
?>
	<div class="<?php echo esc_attr($edubase_wpopconfig['left-sidebar']['class']); ?>">
		<div class="wpo-sidebar wpo-sidebar-left">
			<div class="sidebar-inner">
				<?php dynamic_sidebar( $pos ); ?>
			</div>
 
		</div>
	</div>
<?php } ?>
 