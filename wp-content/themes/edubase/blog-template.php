<?php
/*
*Template Name: Blog
*
*/
global $edubase_wpopconfig;
// Get Page Config
$edubase_wpopconfig = $edubase_wpoengine->getPageConfig();

?>

<?php get_header( $edubase_wpoengine->getHeaderLayout() );  ?>

 <?php 
 	if( isset($edubase_wpopconfig['breadcrumb']) && $edubase_wpopconfig['breadcrumb'] ){
    do_action( 'edubase_wpo_layout_breadcrumbs_render' ); 
  } 
?>  
    
 <?php do_action( 'edubase_wpo_layout_template_before' ) ; ?>

     <div class="post-area blog-page-<?php echo (esc_attr($edubase_wpopconfig['blog_style']) ?  esc_attr($edubase_wpopconfig['blog_style']) : 'default'); ?> <?php echo ($edubase_wpopconfig['blog_style']=='masonry')? 'blog-masonry ': ''; ?>" id="container">
         <?php get_template_part('contents-post');?>
     </div>

 <?php do_action( 'edubase_wpo_layout_template_after' ) ; ?>

<?php get_footer(); ?>