<?php  
global $edubase_wpopconfig;  
$pos = edubase_wpo_theme_options('woocommerce-archive-right-sidebar');
?>

<?php if($edubase_wpopconfig['right-sidebar']['show']){ ?>
	<div class="<?php echo esc_attr($edubase_wpopconfig['right-sidebar']['class']); ?>">
		<div class="wpo-sidebar wpo-sidebar-right">
			<div class="sidebar-inner">
				<?php dynamic_sidebar( $pos ); ?>
			</div>
		</div>
	</div>
<?php } ?>
