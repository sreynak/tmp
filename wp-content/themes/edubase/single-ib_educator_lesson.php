<?php if ( ! defined( 'ABSPATH' ) ) exit; 

global $edubase_wpopconfig;
?>

<?php get_header( edubase_wpo_theme_options('headerlayout', '') );  ?>
    
   <?php
		do_action( 'ib_educator_before_main_loop' );
	?>

	<?php while ( have_posts() ) : the_post(); ?>
	<?php Edr_View::template_part( 'content', 'single-lesson' ); ?>
	<?php endwhile; ?>

	<?php
		do_action( 'ib_educator_after_main_loop' );
	?>
             
<?php get_footer(); ?>

