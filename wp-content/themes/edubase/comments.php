<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <wpopal@gmail.com, support@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */

if ( post_password_required() ){
    return;
}
?>
<div id="comments" class="comments">
    <?php if ( have_comments() ) { ?>
        <header class="header-title">
            <h5 class="comments-title title"><?php comments_number( __('0 Comment', 'edubase'), __('1 Comment', 'edubase'), __('% Comments', 'edubase') ); ?></h5>
        </header><!-- /header -->

        <div class="wpo-commentlists">
    	    <ol class="commentlists">
    	        <?php wp_list_comments('callback=edubase_wpo_theme_comment'); ?>
    	    </ol>
    	    <?php
    	    	// Are there comments to navigate through?
    	    if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
    	    ?>
    	    <footer class="navigation comment-navigation" role="navigation">
    	        <div class="previous"><?php previous_comments_link( __( '&larr; Older Comments', 'edubase' ) ); ?></div>
    	        <div class="next right"><?php next_comments_link( __( 'Newer Comments &rarr;', 'edubase' ) ); ?></div>
    	    </footer><!-- .comment-navigation -->
    	    <?php endif; // Check for comment navigation ?>

    	    <?php if ( ! comments_open() && get_comments_number() ) : ?>
    	        <p class="no-comments"><?php _e( 'Comments are closed.' , 'edubase' ); ?></p>
    	    <?php endif; ?>
        </div>
    <?php } ?> 

	<?php
        $aria_req = ( $req ? " aria-required='true'" : '' );
        $comment_args = array(
            'title_reply'=> '<h4 class="title">'.__('Send a Comment','edubase').'</h4>',

            'comment_field' => '<div class="form-group">
                                    <textarea placeholder="'.__('Your Message', 'edubase').'" rows="5" id="comment" class="form-control"  name="comment"'.$aria_req.'></textarea>
                                </div>',
            'fields' => apply_filters(
            	'comment_form_default_fields',
        		array(
                    'author' => '<div class="row"><div class="form-group col-sm-6 col-xs-12">
                                <input type="text" name="author" placeholder="'.__('Your Name *', 'edubase').'" class="form-control" id="author" value="' . esc_attr( $commenter['comment_author'] ) . '" ' . $aria_req . ' />
                                </div>',
                    'email' => ' <div class="form-group col-sm-6 col-xs-12">
                                <input id="email" name="email" placeholder="'.__('Email *', 'edubase').'" class="form-control" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" ' . $aria_req . ' />
                                </div></div>',
                    'url' => '<div class="form-group">
                                <input id="url" placeholder="'.__('Website', 'edubase').'" name="url" class="form-control" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '"  />
                                </div>',
                                
                )),
            'label_submit' => __('Send', 'edubase'),
			'comment_notes_before' => '<div class="form-group h-info">'.__('Your email address will not be published.','edubase').'</div>',
			'comment_notes_after' => '',
        );
    ?>
	<?php global $post; ?>
	<?php if('open' == $post->comment_status){ ?>
	<div class="commentform row reset-button-default">
    	<div class="col-sm-12">
			<?php edubase_wpo_comment_form($comment_args); ?>
    	</div>
    </div><!-- end commentform -->
	<?php } ?>
</div><!-- end comments -->