<?php 
global $edubase_wpopconfig;  
$pos = esc_attr( edubase_wpo_theme_options('woocommerce-archive-left-sidebar') );
?> 
<?php if($edubase_wpopconfig['left-sidebar']['show']){ ?>
	<div class="<?php echo esc_attr($edubase_wpopconfig['left-sidebar']['class']); ?>">
		<div class="wpo-sidebar wpo-sidebar-left">
			<div class="sidebar-inner">
				<?php dynamic_sidebar( $pos ); ?>
			</div>
 
		</div>
	</div>
<?php } ?>
 