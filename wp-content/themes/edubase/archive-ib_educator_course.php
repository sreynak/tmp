<?php if ( ! defined( 'ABSPATH' ) ) exit; 

global $edubase_wpopconfig;
$edubase_wpopconfig = edubase_wpo_course_page_config();

get_header( edubase_wpo_theme_options('headerlayout', '') );  

$columns = edubase_wpo_theme_options('courses-archive-column');

switch ($columns) {
	case '6': 
		$class_column = 'col-lg-2 col-md-4 col-sm-4';
		break;
	case '4':
		$class_column='col-md-3 col-sm-3';
		break;	
	case '3':
		$class_column='col-md-4 col-sm-4';
		break;
	case '2':
		$class_column='col-md-6 col-sm-6';
		break;
	default:
		$class_column='col-md-6 col-sm-12';
		break;
}
$_count = 0;
?>

<?php 
 	do_action( 'edubase_wpo_layout_breadcrumbs_render' );   
	do_action( 'ib_educator_before_main_loop', 'archive' );
?> 

	<section id="wpo-mainbody" class="wpo-mainbody clearfix">
	    <div class="container">
	      <div class="container-inner">
	      	<?php while ( have_posts() ) : the_post(); ?>
				<?php if($_count%$columns==0): ?>
					<div class="row">
				<?php endif; ?>
					<div class="<?php echo esc_attr($class_column) ?>">
						<?php get_template_part( 'ibeducator/content', 'course' ); ?>
					</div>
				<?php if($_count%$columns==$columns-1 || $_count==$wp_query->post_count -1):?>	
					</div>
				<?php endif; ?>
				<?php $_count++; ?>	
			<?php endwhile; ?>
			</div>
		</div>	
	</section>			

<?php do_action( 'ib_educator_after_main_loop', 'archive' ); ?>

<?php get_footer(); ?>
